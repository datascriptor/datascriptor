/**
 * This file contains a mini-server with HTTPS redirection to run the application in production
 */
const express = require('express');
const fs = require('fs');
const path = require('path');
const http = require('http');
const https = require('https');

// initialize express.
const app = express();

// Initialize variables.
const port = process.env.PORT || 8080;
const securePort = process.env.SECURE_PORT || 8443;

const indexHtmlFile = path.resolve(__dirname, 'dist', 'spa', 'index.html');

const CERT_PATH = process.env.CERT_PATH || path.resolve(__dirname, 'https'); // /etc/letsencrypt/live/scientificdata.isa-explorer.org

// HTTPS CREDENTIALS
const credentials = {
  key: fs.readFileSync(`${CERT_PATH}/datascriptor.org+5-key.pem`),
  cert: fs.readFileSync(`${CERT_PATH}/datascriptor.org+5.pem`)
};

app.use('/js', express.static(
  path.resolve(__dirname, 'dist', 'spa', 'js')
));

app.use('/css', express.static(
  path.resolve(__dirname, 'dist', 'spa', 'css')
));

app.use('/fonts', express.static(
  path.resolve(__dirname, 'dist', 'spa', 'fonts')
));

app.use('/icons', express.static(
  path.resolve(__dirname, 'dist', 'spa', 'icons')
));

// Add a handler to inspect the req.secure flag (see
// http://expressjs.com/api#req.secure). This allows us
// to know whether the request was via http or https.
app.use(function(req, res, next) {
  if (req.secure) {
    // request was via https, so do no special handling
    next();
  } else {
    // request was via http, so redirect to https
    const hostname = req.headers.host.match(':') ? req.headers.host.split(':')[0] : req.headers.host;
    const redirectURL = `https://${hostname}:${securePort}${req.url}`;
    res.redirect(redirectURL);
  }
});

// handle every route with index.html
app.get('*', function(req, res) {
  // the browser must not cache index.html
  res.set('Cache-Control', 'no-cache');
  res.sendFile(indexHtmlFile);
});

// Start the server.
https.createServer(credentials, app).listen(securePort, () => {
  console.log(`Datascriptor listening securely on port ${securePort}`);
});

http.createServer(app).listen(port, () => {
  console.log(`Datascriptor listening on port ${port}`);
});
