const fs = require('fs');
// const path = require('path');
require('dotenv').config({ path: '../../.env' });
console.log(process.env.ORCID_OAUTH_REQUEST_URL);

let https = false;
try {
  if (fs.existsSync('./https/conf.json')) {
    const conf = require('./https/conf.json');
    https = {
      key: fs.readFileSync(conf.key),
      cert: fs.readFileSync(conf.cert),
      ca: fs.readFileSync(conf.ca)
    };
  }
} catch (err) {
  console.log(`Error caught: ${err}`);
}

// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js

module.exports = function(ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/cli-documentation/boot-files
    boot: [
      'i18n',
      'axios'
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: [
      'app.scss'
    ],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v4',
      'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons' // optional, you are not bound to it
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      // iconSet: 'ionicons-v4', // Quasar icon set
      // lang: 'de', // Quasar language pack

      // Possible values for "all":
      // * 'auto' - Auto-import needed Quasar components & directives
      //            (slightly higher compile time; next to minimum bundle size; most convenient)
      // * false  - Manually specify what to import
      //            (fastest compile time; minimum bundle size; most tedious)
      // * true   - Import everything from Quasar
      //            (not treeshaking Quasar; biggest bundle size; convenient)
      importStrategy: 'auto',

      components: [],
      directives: [],

      // Quasar plugins
      plugins: [
        'Dialog',
        'Loading',
        'LocalStorage',
        'Notify'
      ]
    },

    // https://quasar.dev/quasar-cli/cli-documentation/supporting-ie
    // supportIE: false,

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      env: {
        USE_REMOTE: false,
        ORCID_OAUTH_REQUEST_URL: process.env.ORCID_OAUTH_REQUEST_URL
      },
      scopeHoisting: true,
      // vueRouterMode: 'history',
      // showProgress: false,
      // gzip: true,
      // analyze: true,
      // preloadChunks: false,
      // extractCSS: false,

      // https://quasar.dev/quasar-cli/cli-documentation/handling-webpack
      extendWebpack(cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
          options: {
            formatter: require('eslint').CLIEngine.getFormatter('stylish')
          }
        });
      },

      // this is a configuration passed on to the underlying Webpack
      // needed to get proper sourcemaps that can be debugged with vscode
      devtool: 'source-map'
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https,
      port: https ? 8443 : 8080,
      open: true, // opens browser window automatically
      proxy: {
        // proxy all requests starting with /api to the DataScriptor API
        // default  express port 3000
        // default netlify dev port  8888
        '/api': {
          target: 'http://localhost:3000/',
          changeOrigin: true,
          ws: true
        }
      }
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: [],

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: 'GenerateSW',
      workboxOptions: { // only for NON InjectManifest
        skipWaiting: true,
        clientsClaim: true
      },
      manifest: {
        name: 'Datascriptor User Interface',
        short_name: 'Datascriptor-UI',
        description: 'The Datascriptor User Interface, built on Quasar',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    // https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // id: 'org.cordova.quasar.app',
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: 'builder', // or 'packager'

      extendWebpack(cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'org.datascriptor.desktop-app',
        productName: 'Datascriptor Desktop',
        buildVersion: '0.1.0',
        mac: {
          category: 'public.app-category.education',
          // target: 'default',
          identity: process.env.CSC_LINK, // or process.env.CSC_NAME, see https://www.electron.build/code-signing
          // entitlements: build/entitlements.mac.plist,
          type: 'distribution', // or 'development'
          minimumSystemVersion: '10.14.0'
        },
        dmg: {
          // add here DMG-related builder configuration
          // https://www.electron.build/configuration/dmg
          sign: false
        },
        win: {
          // https://www.electron.build/configuration/win
          target: 'nsis',
          // signingHashAlgorithms: ['sha1', 'sha256'],
          requestedExecutionLevel: 'asInvoker'
        },
        linux: {
          // https://www.electron.build/configuration/linux
          target: 'AppImage',
          executableName: 'Datascriptor Desktop',
          synopsis: 'This is the linux desktop application of Datascriptor. Use it to generate Experimental Design and Metadata.',
          // description: '',
          category: 'Science'
          // desktop: '' // The Desktop file entries (name to value)
        },
        deb: {
          // packageCategory: '',  ??
          priority: 'optional'
        }
      }
    }
  };
};
