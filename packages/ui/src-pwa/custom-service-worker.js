/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */

/*
 *   dependencies
 */

import { precacheAndRoute } from 'workbox-precaching';

/*
 *   config
 */

// Precaching
precacheAndRoute(self.__WB_MANIFEST);
