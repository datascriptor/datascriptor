import axios from 'axios';

function computeArms(payload) {
  return axios.post('/api/studyDesign/computeArms', payload);
  // FIXME: extract the body from the response
  // return response.data;
}

function generateSubjectGroups(payload) {
  return axios.post('/api/studyDesign/computeSubjectGroups', payload);
}

export default {
  computeArms,
  generateSubjectGroups
};
