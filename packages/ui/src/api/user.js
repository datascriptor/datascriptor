import axios from 'axios';

export function signIn(email, password) {
  return axios.post('/api/login', { email, password });
}

export function signUp(username, email, password) {
  return axios.post('/api/user', { name: username, email, password });
}

export function generatePwResetLink(email) {
  return axios.post('/api/resetPwdToken', { identifier: email });
}

export function resetPassword(password, token) {
  return axios.patch('/api/user', { password, token });
}

export default {
  signIn,
  signUp,
  generatePwResetLink,
  resetPassword
};
