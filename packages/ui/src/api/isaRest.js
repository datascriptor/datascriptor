import axios from 'axios';

const ISA_REST_BASE_URL = 'http://localhost:5000/';

function generateIsaFromDesign(studyDesignConfig, format) {
  return axios({
    url: 'api/v1/isa-study-from-design',
    method: 'post',
    responseType: 'blob',
    baseURL: ISA_REST_BASE_URL,
    data: {
      studyDesignConfig,
      format
    }
  });
};

export default {
  generateIsaFromDesign
};
