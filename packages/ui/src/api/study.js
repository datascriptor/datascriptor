import axios from 'axios';

export function createStudy(payload, config) {
  return axios.post('/api/study', payload, config);
}

/**
 * @function
 * @name retrieveAllStudies
 * @param {String/null} searchText - the text used to filter the results
 * @param {Number} page - the current page (starting from 1), a positive integer
 * @param {Number} rowsPerPage - the number of rows per page, a positive integer
 * @returns {Object} with properties 'studies' and 'paginationInfo'
 */
export async function retrieveAllStudies({
  searchText = null,
  page = 1,
  rowsPerPage = 10
}) {
  const params = {
    skip: (page - 1) * rowsPerPage,
    limit: rowsPerPage
  };
  if (searchText) {
    params.text = searchText;
  }
  try {
    const res = await axios.get('/api/study', {
      params
    });
    console.log('Study response headers follow');
    console.log(res.headers);
    return {
      studies: res.data,
      paginationInfo: {
        page: Number.parseInt(res.headers['ds-current-page']) || page,
        rowsNumber: Number.parseInt(res.headers['ds-total-count']),
        rowsPerPage: Number.parseInt(res.headers['ds-page-size']) || rowsPerPage
      }
    };
  } catch (err) {
    console.log(err);
    const { response: { error } = {} } = err;
    throw new Error({
      message: 'Error thrown while retrieving studies',
      _rc: error
    });
  }
}

export function retrieveStudyById(studyId) {
  return axios.get(`/api/study/${studyId}`);
}

export function updateStudy(payload, config) {
  console.log(`updateStudy() - Updating record with ID: ${payload._id}`);
  return axios.put(`/api/study/${payload._id}`, payload, config);
}

export function deleteStudy(payload, config) {
  return axios.delete(`/api/study/${payload._id}`, config);
}

export default {
  createStudy,
  retrieveAllStudies,
  retrieveStudyById,
  updateStudy,
  deleteStudy
};
