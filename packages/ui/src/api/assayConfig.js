import axios from 'axios';

function retrieveAll() {
  return axios.get('/api/assayConfig');
  // FIXME: extract the body from the response
  // return response.data;
}

export default {
  retrieveAll
};
