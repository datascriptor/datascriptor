import axios from 'axios';

async function generateScholarlyArticle(payload) {
  const response = await axios.post('/api/scholarlyArticle', payload);
  // FIXME: extract the body from the response
  return response.data;
}

export default {
  generateScholarlyArticle
};
