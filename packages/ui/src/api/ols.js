import axios from 'axios';

const ONTOLOGIES_FOR_SUBJECTS = ['ncbitaxon', 'ncit'];
const ONTOLOGIES_FOR_SAMPLES = ['ncit', 'obi'];
const ONTOLOGIES_FOR_MEASURE_UNITS = ['om']; // possibly also uo

const OLS_BASE_URL = 'https://www.ebi.ac.uk/ols/';
const DEFAULT_QUERY_FIELDS = ['iri', 'ontology_name', 'ontology_prefix', 'short_form', 'synonym', 'description', 'id', 'label', 'is_defining_ontology', 'obo_id', 'type'];
const DEFAULT_FIELD_LIST = ['iri', 'label', 'short_form', 'obo_id', 'ontology_name', 'ontology_prefix', 'description,type'];

/**
 * @function
 * @name getOntologyAnnotationsFromOLS
 * @param {String} val - the value to be searched against the target ontologies
 * @param {Array} targetOntologies - a list of target ontologies in OLS
 * @param {Boolean} autocomplete - if true it will use the 'api/select' endpoint, otherwise 'api/search'
 * @returns Array of matching records (i.e. Ontology Annotations)
 */
async function getOntologyAnnotationsFromOLS(val, targetOntologies, autocomplete = true) {
  // look if val is in the localStorage. If it is, return the localstorage value
  // before the api call.
  const url = autocomplete ? 'api/select' : 'api/search';
  const res = await axios({
    url,
    method: 'get',
    baseURL: OLS_BASE_URL,
    params: {
      q: encodeURIComponent(val),
      ontology: targetOntologies.join(','),
      type: 'class',
      queryFields: DEFAULT_QUERY_FIELDS.concat([
        // 'exact_synonym', 'related_synonym'
      ]).join(','),
      fieldList: DEFAULT_FIELD_LIST.concat([
        // 'exact_synonym', 'related_synonym'
      ]).join(',')
    }
  });
    // some more logic preprocessing the data
  const { response: { docs = [] } = {} } = res.data;
  return docs;
};

export async function getSubjectTypes(val, autocomplete = true) {
  return getOntologyAnnotationsFromOLS(val, ONTOLOGIES_FOR_SUBJECTS, autocomplete);
}

export async function getSampleTypes(val, autocomplete = true) {
  return getOntologyAnnotationsFromOLS(val, ONTOLOGIES_FOR_SAMPLES, autocomplete);
}

export async function getMeasureUnits(val, autocomplete = true) {
  return getOntologyAnnotationsFromOLS(val, ONTOLOGIES_FOR_MEASURE_UNITS, autocomplete);
}

/*
export async function getDurationUnits(val, autocomplete = true) {
    return getOntologyAnnotationsFromOLS(val, ONTOLOGIES_FOR_MEASURE_UNITS, autocomplete);
} */

export default {
  getSubjectTypes,
  getSampleTypes,
  getMeasureUnits
};
