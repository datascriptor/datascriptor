import {
  REMOTE_REQUEST,
  STUDY_SET, FETCH_ERROR_SET,
  STUDY_RESET, FETCH_ERROR_RESET,
  RESEARCH_RESET_ALL
} from 'src/store/mutation-types';
import { retrieveStudyById } from 'src/api/study';

const initialState = {
  isPending: false,
  study: {},
  fetchStudyError: null,
  studyReport: {}
};

const getters = {};

const mutations = {
  [REMOTE_REQUEST](state) {
    state.isPending = true;
  },
  [STUDY_SET](state, study) {
    state.isPending = false;
    state.study = study;
  },
  [FETCH_ERROR_SET](state, error) {
    state.isPending = false;
    state.fetchStudyError = error;
  },
  [STUDY_RESET](state) {
    state.study = {};
  },
  [FETCH_ERROR_RESET](state) {
    state.fetchStudyError = null;
  },
  [RESEARCH_RESET_ALL](state) {
    state = {
      ...initialState
    };
  }
};

const actions = {
  async getStudy({ commit }, studyId) {
    try {
      const res = await retrieveStudyById(studyId);
      commit(STUDY_SET, res.data);
      commit(FETCH_ERROR_RESET);
    } catch (err) {
      commit(STUDY_RESET);
      commit(FETCH_ERROR_SET, err);
    }
  }
};

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state: initialState
};
