import Vue from 'vue';
import Vuex from 'vuex';

import auth from './auth';
import research from './research';
import publication from './publication';
import studyDesignPlanner from './studyDesignPlanner';

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      research,
      publication,
      studyDesignPlanner
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
