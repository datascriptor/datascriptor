// import axios from 'axios';
import { isEmpty } from 'lodash';
import {
  USER_SET, TOKEN_SET
} from 'src/store/mutation-types';
import { LocalStorage } from 'quasar';

export const state = {
  user: null,
  token: null
};

export const getters = {
  isSignedIn: state => !isEmpty(state.user),
  username: state => state.user && state.user.name,
  isAdmin: state => !isEmpty(state.user) && state.user.isAdmin === true,
  config: state => {
    return {
      headers: { Authorization: `Bearer ${state.token}` }
    };
  }
};

export const mutations = {
  [USER_SET](state, user) {
    state.user = user;
  },
  [TOKEN_SET](state, token) {
    state.token = token;
  }
};

export const actions = {
  setCredentials({ state, commit }, payload) {
    const { user, token } = payload;
    commit(USER_SET, user);
    commit(TOKEN_SET, token);
    LocalStorage.set('user', user);
    LocalStorage.set('token', token);
    // axios.defaults.headers.common = { 'Authorization': `Bearer ${token}` };
  },
  autoLogin({ state, commit }) {
    let user = LocalStorage.getItem('user');
    let token = LocalStorage.getItem('token');
    if (user && token) {
      commit(USER_SET, user);
      commit(TOKEN_SET, token);
      // axios.defaults.headers.common = { 'Authorization': `Bearer ${token}` };
    }
    // later on you can handle token expiration in an else
  },
  logout({ state, commit }) {
    LocalStorage.remove('user');
    LocalStorage.remove('token');
    commit(USER_SET, null);
    commit(TOKEN_SET, null);
    // axios.defaults.headers.common = null;
  }
};

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
