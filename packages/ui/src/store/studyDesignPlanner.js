import { isEmpty, zipObject, pick, cloneDeep } from 'lodash';
import sdApi from 'src/api/studyDesign';
import acApi from 'src/api/assayConfig';
import { retrieveStudyById } from 'src/api/study';
// import Vue from 'vue';
import { NUM_REPLICATES } from 'src/utils/constants';
import { filterAnnotationsByOptions } from 'src/utils/funcs';
import { research } from '@datascriptor/core';

import {
  WHOLE_STUDY_DESIGN_PLANNER_SET,
  NAME_SET, DESCRIPTION_SET, SUBJECT_TYPE_SET,
  SUBJECT_SIZE_SET,
  OBSERVATIONAL_FACTOR_ADD, OBSERVATIONAL_FACTOR_REMOVE,
  OBSERVATIONAL_FACTOR_NAME_SET, OBSERVATIONAL_FACTOR_IS_QUANTITATIVE_SET,
  OBSERVATIONAL_FACTOR_VALUES_SET, OBSERVATIONAL_FACTOR_UNIT_SET,
  GENERATED_SUBJECT_GROUPS, SELECTED_SUBJECT_GROUPS_SET,
  DESIGN_TYPE_SET,
  NON_TREATMENT_ELEMENT_SET, NON_TREATMENT_ELEMENT_RESET,
  OBSERVATION_PERIOD_DURATION_SET, OBSERVATION_PERIOD_DURATION_UNIT_SET, OBSERVATION_PERIOD_RESET,
  SCREEN_DURATION_SET, SCREEN_DURATION_UNIT_SET, SCREEN_RESET,
  RUN_IN_DURATION_SET, RUN_IN_DURATION_UNIT_SET, RUN_IN_RESET,
  WASHOUT_DURATION_SET, WASHOUT_DURATION_UNIT_SET, WASHOUT_RESET,
  FOLLOW_UP_DURATION_SET, FOLLOW_UP_DURATION_UNIT_SET, FOLLOW_UP_RESET,
  ADD_TREATMENT, REMOVE_TREATMENT,
  INTERVENTION_TYPE_SET, AGENT_SET, INTENSITY_SET, INTENSITY_UNIT_SET,
  DURATION_SET, DURATION_UNIT_SET,
  FACTORIAL_INTERVENTION_TYPE_SET, FACTORIAL_AGENTS_SET, FACTORIAL_INTENSITES_SET,
  FACTORIAL_INTENSITY_UNIT_SET, FACTORIAL_DURATIONS_SET, FACTORIAL_DURATION_UNIT_SET,
  REMOTE_REQUEST, REMOTE_ERROR_SET, REMOTE_ERROR_RESET,
  GENERATED_STUDY_DESIGN_SET, SELECTED_ARMS_SET, STUDY_DESIGN_SET,
  ADD_SAMPLE_PLAN_ITEM, REMOVE_SAMPLE_PLAN_ITEM,
  SAMPLE_TYPE_SET,
  SAMPLE_TYPE_SELECTED_CELL_TOGGLE, SAMPLE_TYPE_SIZE_SET,
  SAMPLE_TYPE_SIZE_APPLY_TO_ARM, SAMPLE_TYPE_SIZE_APPLY_TO_EPOCH,
  ASSAY_CONFIGS_SET, ASSAY_TYPE_SELECT, ASSAY_TYPE_DESELECT,
  NUM_REPLICATES_SET,
  CREATE_NEW_PARAMETER_VALUE, UPDATE_SELECTED_PARAMETER_VALUES,
  UPDATE_SELECTED_CHARACTERISTIC_VALUES,
  CREATE_NEW_EXTENSION_VALUE, UPDATE_SELECTED_EXTENSION_VALUE,
  ASSAY_TYPE_SELECTED_CELL_TOGGLE, ASSAY_TYPE_SAMPLE_TYPES_SET,
  ASSAY_IN_TAB_SET,
  RESET_ALL
} from 'src/store/mutation-types';

const _observationaFactorTemplate = {
  name: null,
  values: [],
  isQuantitative: false,
  unit: null
};

const _treatmentTemplate = {
  interventionType: null,
  agent: null,
  intensity: null,
  intensityUnit: '',
  duration: null,
  durationUnit: ''
};

const _elementParamsTemplate = {
  interventionType: null,
  agents: [],
  intensities: [],
  intensityUnit: '',
  durations: [],
  durationUnit: ''
};

const DEFAULT_SAMPLE_TYPE_SIZE = 1;

function getDefaultState() {
  return {
    isPending: false,
    remoteError: null, // TODO rename this to error?
    _id: null,
    name: '',
    description: '',
    subjectType: '',
    subjectSize: 10,
    designType: '',
    observationalFactors: [],
    // unselectedSubjectGroups: [],
    // selectedSubjectGroups: [],
    subjectGroups: {
      selected: [],
      unselected: []
    },
    treatmentPlan: {
      observationPeriod: {
        name: 'observation period',
        duration: null,
        durationUnit: ''
      },
      screen: {
        selected: false,
        name: 'screen',
        duration: null,
        durationUnit: ''
      },
      runIn: {
        selected: false,
        name: 'run-in',
        duration: null,
        durationUnit: ''
      },
      washout: {
        selected: false,
        name: 'washout',
        duration: null,
        durationUnit: ''
      },
      followUp: {
        selected: false,
        name: 'follow-up',
        duration: null,
        durationUnit: ''
      },
      treatments: [
        {
          // name: `treatment_${this.number}`,
          ..._treatmentTemplate
        },
        {
          // name: `treatment_${this.number}`,
          ..._treatmentTemplate
        }
      ], // for Crossover Design
      elementParams: {
        ..._elementParamsTemplate
      } // for Factorial Design
    },
    // generatedStudyDesign: {},
    elements: [],
    arms: {
      selected: [],
      unselected: []
    },
    // unselectedArms: [],
    // selectedArms: [],
    samplePlan: [],
    assayConfigs: [],
    assayPlan: [],
    // selectedAssayTypes: [],
    assayInTab: null
  };
}

export const state = getDefaultState();

export const getters = {
  subjectGroups: state => [...state.subjectGroups.selected, ...state.subjectGroups.unselected],
  hasGeneratedSubjectGroups: (state, getters) => {
    return !isEmpty(getters.subjectGroups);
  }, // TODO refactor to hasSubjectGroups
  observationalFactorCount: state => state.observationalFactors.length,
  treatmentIds: state => state.treatmentPlan.treatments.map((treatment, index) => index),
  arms: state => [...state.arms.selected, ...state.arms.unselected].sort((a, b) => a.id - b.id),
  hasGeneratedStudyDesign: (state, getters) => !isEmpty(getters.arms) && !isEmpty(state.elements), // TODO refactor to hasArms or hasStudyArms?
  hasSelectedArms: state => !isEmpty(state.arms.selected),
  armNames: state => {
    const { arms: { selected: selectedArms = [] } } = state;
    return selectedArms.map(arm => arm.name);
  },
  designGridHeaders: state => {
    const { arms: { selected: selectedArms = [] } } = state;
    if (isEmpty(selectedArms)) return null;
    const epochsCount = Math.max(...selectedArms.map(arm => {
      const { epochs = [] } = arm;
      return epochs.length;
    }));
    return [
      'Arm', ...Array.from(Array(epochsCount).keys()).map(key => `Epoch ${key}`)
    ].map(name => {
      return { name, label: name, field: name };
    });
  },
  designGridBody: (state, getters) => {
    const { elements, arms: { selected: selectedArms = [] } } = state;
    if (isEmpty(selectedArms)) return null;
    const res = selectedArms.map(arm => {
      const { name, epochs = [] } = arm;
      const elemsInEpoch = epochs.map(epoch => {
        return epoch.elements.map(elemId => {
          const elem = elements.find(el => el.id === elemId);
          return elem.name.replace('_', ' ');
        }).join(' & ');
      });
      const colummNames = getters.designGridHeaders.map(col => col.name);
      return zipObject(colummNames, [name, ...elemsInEpoch]);
    });
    return res;
  },
  sampleTypeCount: state => state.samplePlan.length,
  assayConfigDetails: state => state.assayConfigs && state.assayConfigs.map(config => pick(config, ['id', 'name', 'technology_type', 'measurement_type'])),
  assayTypeCount: state => state.assayPlan.length,
  sampleItemByIndex: state => index => state.samplePlan[index],
  sampleSelectedCellsByIndex: state => index => state.samplePlan[index].selectedCells,
  sampleTypeTable: state => {
    const { samplePlan = [], arms: { selected: selectedArms = [] } } = state;
    if (isEmpty(selectedArms)) return null;
    const res = selectedArms.reduce((obj, arm) => {
      const { name, epochs = [] } = arm;
      const sampleTypesInEpoch = epochs.map((epoch, epochIx) => {
        const sampleTypes = samplePlan.reduce((arr, samplePlanItem) => {
          if (samplePlanItem.selectedCells[name][epochIx]) {
            return arr.concat(samplePlanItem.sampleType);
          } else return arr;
        }, []);
        return sampleTypes;
      });
      return { ...obj, [name]: sampleTypesInEpoch };
    }, {});
    return res;
  },
  selectedAssayTypes: state => {
    const { assayConfigs, assayPlan } = state;
    const assayPlanNames = assayPlan.map(assayType => assayType.name);
    return assayConfigs.reduce((o, conf) => ({
      ...o, [conf.name]: assayPlanNames.includes(conf.name)
    }), {});
  },
  // stepper function checks for the four steps
  stepOneCompleted: state => !isEmpty(state.subjectGroups.selected),
  stepTwoCompleted: (state, getters) => getters.hasSelectedArms,
  stepThreeCompleted: (state, getters) => {
    return getters.sampleTypeCount >= 1;
    // TODO we should check that at least a cell has been selected
  },
  stepFourCompleted: (state, getters) => {
    return getters.assayTypeCount >= 1;
    // TODO we should check that at least a cell has been selected
    // and that all fields have been populated in each assay form
  },
  studyForSubmission: state => {
    const {
      _id,
      assayConfigs,
      assayInTab,
      remoteError,
      errors,
      name,
      description,
      isPending,
      ...design
    } = state;
    return {
      _id,
      name,
      description,
      design
    };
  }
};

export const mutations = {
  [WHOLE_STUDY_DESIGN_PLANNER_SET](state, payload) {
    state.isPending = false;
    state.remoteError = null;
    state._id = payload._id;
    state.name = payload.name;
    state.description = payload.description;
    Object.assign(state, payload.design);
  },
  [NAME_SET](state, value) {
    state.name = value;
  },
  [DESCRIPTION_SET](state, value) {
    state.description = value;
  },
  [SUBJECT_TYPE_SET](state, value) {
    state.subjectType = value;
  },
  [SUBJECT_SIZE_SET](state, value) {
    state.subjectSize = value;
  },
  [OBSERVATIONAL_FACTOR_ADD](state) {
    state.observationalFactors.push({ ..._observationaFactorTemplate });
  },
  [OBSERVATIONAL_FACTOR_REMOVE](state, index) {
    state.observationalFactors.splice(index, 1);
  },
  [OBSERVATIONAL_FACTOR_NAME_SET](state, { index, name }) {
    state.observationalFactors[index].name = name;
  },
  [OBSERVATIONAL_FACTOR_IS_QUANTITATIVE_SET](state, { index, flag }) {
    state.observationalFactors[index].isQuantitative = flag;
  },
  [OBSERVATIONAL_FACTOR_VALUES_SET](state, { index, values }) {
    state.observationalFactors[index].values = values;
  },
  [OBSERVATIONAL_FACTOR_UNIT_SET](state, { index, unit }) {
    state.observationalFactors[index].unit = unit;
  },
  [GENERATED_SUBJECT_GROUPS](state, subjectGroups) {
    state.subjectGroups.unselected = subjectGroups;
    state.subjectGroups.selected = [];
  },
  [SELECTED_SUBJECT_GROUPS_SET](state, options) {
    const allSubjectGroups = [ ...state.subjectGroups.selected, ...state.subjectGroups.unselected ];
    state.subjectGroups.selected = options.map(
      opt => allSubjectGroups.find(group => group.name === opt.name)
    );
    state.subjectGroups.unselected = allSubjectGroups.filter(group => !state.subjectGroups.selected.some(selectedGroup => selectedGroup.name === group.name));
  },
  [DESIGN_TYPE_SET](state, value) {
    state.designType = value;
  },
  [NON_TREATMENT_ELEMENT_SET](state, elementType) {
    state.treatmentPlan[elementType].selected = true;
  },
  [NON_TREATMENT_ELEMENT_RESET](state, elementType) {
    state.treatmentPlan[elementType].selected = false;
    state.treatmentPlan[elementType].duration = null;
    state.treatmentPlan[elementType].durationUnit = '';
  },
  [OBSERVATION_PERIOD_DURATION_SET](state, value) {
    state.treatmentPlan.observationPeriod.duration = value;
  },
  [OBSERVATION_PERIOD_DURATION_UNIT_SET](state, value) {
    state.treatmentPlan.observationPeriod.durationUnit = value;
  },
  [OBSERVATION_PERIOD_RESET](state) {
    state.treatmentPlan.observationPeriod.duration = null;
    state.treatmentPlan.observationPeriod.durationUnit = '';
  },
  [SCREEN_DURATION_SET](state, value) {
    state.treatmentPlan.screen.duration = value;
  },
  [SCREEN_DURATION_UNIT_SET](state, value) {
    state.treatmentPlan.screen.durationUnit = value;
  },
  [SCREEN_RESET](state) {
    state.treatmentPlan.screen.duration = null;
    state.treatmentPlan.screen.durationUnit = '';
  },
  [RUN_IN_DURATION_SET](state, value) {
    state.treatmentPlan.runIn.duration = value;
  },
  [RUN_IN_DURATION_UNIT_SET](state, value) {
    state.treatmentPlan.runIn.durationUnit = value;
  },
  [RUN_IN_RESET](state) {
    state.treatmentPlan.runIn.duration = null;
    state.treatmentPlan.runIn.durationUnit = '';
  },
  [WASHOUT_DURATION_SET](state, value) {
    state.treatmentPlan.washout.duration = value;
  },
  [WASHOUT_DURATION_UNIT_SET](state, value) {
    state.treatmentPlan.washout.durationUnit = value;
  },
  [WASHOUT_RESET](state) {
    state.treatmentPlan.washout.duration = null;
    state.treatmentPlan.washout.durationUnit = '';
  },
  [FOLLOW_UP_DURATION_SET](state, value) {
    state.treatmentPlan.followUp.duration = value;
  },
  [FOLLOW_UP_DURATION_UNIT_SET](state, value) {
    state.treatmentPlan.followUp.durationUnit = value;
  },
  [FOLLOW_UP_RESET](state) {
    state.treatmentPlan.followUp.duration = null;
    state.treatmentPlan.followUp.durationUnit = '';
  },
  [ADD_TREATMENT](state) {
    state.treatmentPlan.treatments.push({ ..._treatmentTemplate });
  },
  [REMOVE_TREATMENT](state, index) {
    state.treatmentPlan.treatments.splice(index, 1);
  },
  [INTERVENTION_TYPE_SET](state, { index, value }) {
    state.treatmentPlan.treatments[index].interventionType = value;
  },
  [AGENT_SET](state, { index, value }) {
    state.treatmentPlan.treatments[index].agent = value;
  },
  [INTENSITY_SET](state, { index, value }) {
    state.treatmentPlan.treatments[index].intensity = value;
  },
  [INTENSITY_UNIT_SET](state, { index, value }) {
    state.treatmentPlan.treatments[index].intensityUnit = value;
  },
  [DURATION_SET](state, { index, value }) {
    state.treatmentPlan.treatments[index].duration = value;
  },
  [DURATION_UNIT_SET](state, { index, value }) {
    state.treatmentPlan.treatments[index].durationUnit = value;
  },
  [FACTORIAL_INTERVENTION_TYPE_SET](state, value) {
    state.treatmentPlan.elementParams.interventionType = value;
  },
  [FACTORIAL_AGENTS_SET](state, agents) {
    state.treatmentPlan.elementParams.agents = agents;
  },
  [FACTORIAL_INTENSITES_SET](state, intensities) {
    state.treatmentPlan.elementParams.intensities = intensities;
  },
  [FACTORIAL_INTENSITY_UNIT_SET](state, value) {
    state.treatmentPlan.elementParams.intensityUnit = value;
  },
  [FACTORIAL_DURATIONS_SET](state, durations) {
    state.treatmentPlan.elementParams.durations = durations;
  },
  [FACTORIAL_DURATION_UNIT_SET](state, value) {
    state.treatmentPlan.elementParams.durationUnit = value;
  },
  [REMOTE_REQUEST](state) {
    state.isPending = true;
  },
  [REMOTE_ERROR_SET](state, err) {
    state.remoteError = err;
  },
  [REMOTE_ERROR_RESET](state) {
    state.remoteError = null;
  },
  [GENERATED_STUDY_DESIGN_SET](state, generatedStudyDesign) {
    // state.generatedStudyDesign = generatedStudyDesign;
    state.name = generatedStudyDesign.name;
    state.description = generatedStudyDesign.description;
    state.subjectType = generatedStudyDesign.subjectType;
    // state.subjectSize = generatedStudyDesign.subjectSize;
    state.designType = generatedStudyDesign.type;
    state.elements = generatedStudyDesign.elements;
    state.arms.unselected = generatedStudyDesign.arms;
    state.arms.selected = [];
  },
  [SELECTED_ARMS_SET](state, options) {
    // const { generatedStudyDesign: { arms = [] } = {} } = state;
    const allArms = [ ...state.arms.selected, ...state.arms.unselected ];
    state.arms.selected = options.map(opt => allArms.find(arm => arm.name === opt.Arm));
    state.arms.unselected = allArms.filter(arm => !state.arms.selected.some(selectedArm => selectedArm.name === arm.name));
  },
  // this is probably not needed any more
  [STUDY_DESIGN_SET](state, studyDesign) {
    state.studyDesign = studyDesign;
  },
  [ADD_SAMPLE_PLAN_ITEM](state, sampleTypeTemplate) {
    state.samplePlan.push({ ...sampleTypeTemplate });
  },
  [REMOVE_SAMPLE_PLAN_ITEM](state, index) {
    state.samplePlan.splice(index, 1);
  },
  [SAMPLE_TYPE_SET](state, { samplePlanIndex, sampleType }) {
    state.samplePlan[samplePlanIndex].sampleType = sampleType;
  },
  [SAMPLE_TYPE_SELECTED_CELL_TOGGLE](state, { samplePlanIndex, armName, epochIndex, value }) {
    state.samplePlan[samplePlanIndex].selectedCells[armName].splice(epochIndex, 1, value);
    const sampleTypeSize = value ? DEFAULT_SAMPLE_TYPE_SIZE : null;
    state.samplePlan[samplePlanIndex].sampleTypeSizes[armName].splice(epochIndex, 1, sampleTypeSize);
  },
  [SAMPLE_TYPE_SIZE_SET](state, { samplePlanIndex, armName, epochIndex, value }) {
    state.samplePlan[samplePlanIndex].sampleTypeSizes[armName].splice(epochIndex, 1, value);
  },
  [SAMPLE_TYPE_SIZE_APPLY_TO_ARM](state, { samplePlanIndex, armName, value }) {
    const epochsInArm = state.samplePlan[samplePlanIndex].sampleTypeSizes[armName].length;
    state.samplePlan[samplePlanIndex].selectedCells[armName] = Array(epochsInArm).fill(true);
    state.samplePlan[samplePlanIndex].sampleTypeSizes[armName] = Array(epochsInArm).fill(value);
  },
  [SAMPLE_TYPE_SIZE_APPLY_TO_EPOCH](state, { samplePlanIndex, epochIndex, value }) {
    for (const armName of Object.keys(state.samplePlan[samplePlanIndex].sampleTypeSizes)) {
      state.samplePlan[samplePlanIndex].selectedCells[armName].splice(epochIndex, 1, true);
      state.samplePlan[samplePlanIndex].sampleTypeSizes[armName].splice(epochIndex, 1, value);
    }
  },
  [ASSAY_CONFIGS_SET](state, assayConfigs) {
    state.assayConfigs = assayConfigs;
    // state.selectedAssayTypes = assayConfigs.reduce((o, el) => ({ ...o, [el.name]: false }), {});
  },
  [ASSAY_TYPE_SELECT](state, assayTypeName) {
    const selectedAssayType = state.assayConfigs.find(config => config.name === assayTypeName);
    state.assayPlan.push(cloneDeep(selectedAssayType));
  },
  [ASSAY_TYPE_DESELECT](state, assayTypeName) {
    const assayTypeIndex = state.assayPlan.findIndex(assayType => assayType.name === assayTypeName);
    state.assayPlan.splice(assayTypeIndex, 1);
  },
  [NUM_REPLICATES_SET](state, { assayIndex, nodeIndex, value }) {
    // console.log(state.assayConfigs[assayIndex].workflow[nodeIndex][1]['#replicates']);
    state.assayPlan[assayIndex].workflow[nodeIndex][1][NUM_REPLICATES].value = value;
  },
  [UPDATE_SELECTED_PARAMETER_VALUES](state, { assayIndex, nodeIndex, paramName, values }) {
    const parameter = state.assayPlan[assayIndex].workflow[nodeIndex][1][paramName];
    state.assayPlan[assayIndex].workflow[nodeIndex][1][paramName].values = filterAnnotationsByOptions(parameter.options, values);
  },
  [CREATE_NEW_PARAMETER_VALUE](state, { assayIndex, nodeIndex, paramName, newValue }) {
    state.assayPlan[assayIndex].workflow[nodeIndex][1][paramName].options.push(newValue);
    state.assayPlan[assayIndex].workflow[nodeIndex][1][paramName].values.push(newValue);
  },
  [UPDATE_SELECTED_CHARACTERISTIC_VALUES](state, { assayIndex, nodeIndex, values }) {
    const characteristicValue = state.assayPlan[assayIndex].workflow[nodeIndex][1].characteristics_value;
    state.assayPlan[assayIndex].workflow[nodeIndex][1].characteristics_value.values = filterAnnotationsByOptions(characteristicValue.options, values);
  },
  [CREATE_NEW_EXTENSION_VALUE](state, { assayIndex, nodeIndex, newValue }) {
    // TODO create a new file extension value and make it selected
  },
  [UPDATE_SELECTED_EXTENSION_VALUE](state, { assayIndex, nodeIndex, value }) {
    // TODO make selection of the correct file extension value
  },
  [ASSAY_TYPE_SELECTED_CELL_TOGGLE](state, { assayIndex, armName, epochIndex, value }) {
    state.assayPlan[assayIndex].selectedCells[armName].splice(epochIndex, 1, value);
  },
  [ASSAY_TYPE_SAMPLE_TYPES_SET](state, { assayIndex, armName, epochIndex, values }) {
    state.assayPlan[assayIndex].selectedSampleTypes[armName].splice(epochIndex, 1, values);
  },
  [ASSAY_IN_TAB_SET](state, assayName) {
    state.assayInTab = assayName;
  },
  [RESET_ALL](state) {
    Object.assign(state, getDefaultState());
  }
};

export const actions = {
  getStudy: async function({ commit }, studyId) {
    try {
      const { data = {} } = await retrieveStudyById(studyId);
      const { _id, name, description, design } = data;
      commit(WHOLE_STUDY_DESIGN_PLANNER_SET, {
        _id,
        name,
        description,
        design
      });
    } catch (err) {
      commit(RESET_ALL);
      commit(REMOTE_ERROR_SET, err);
    }
  },
  generateSubjectGroups: async function({ state, commit }) {
    const { subjectType, observationalFactors } = state;
    try {
      if (process.env.USE_REMOTE) {
        const { data } = await sdApi.generateSubjectGroups({ subjectType, observationalFactors });
        commit(GENERATED_SUBJECT_GROUPS, data);
      } else {
        const res = research.computeSubjectGroupsFromObservationalFactors(
          subjectType, observationalFactors
        );
        commit(GENERATED_SUBJECT_GROUPS, res);
      }
    } catch (error) {
      commit(REMOTE_ERROR_SET, error);
    }
  },
  generateArms: async function({ state, commit }, payload) {
    try {
      if (process.env.USE_REMOTE) {
        const response = await sdApi.computeArms(payload);
        commit(GENERATED_STUDY_DESIGN_SET, response.data);
      } else {
        const { type, ...kwargs } = payload;
        const design = research.armCollectionBuilder[type](kwargs);
        // Need to call .toJSON() to convert it to Plain Object
        commit(GENERATED_STUDY_DESIGN_SET, design.toJSON());
      }
    } catch (error) {
      commit(REMOTE_ERROR_SET, error);
    }
  },
  addSamplePlanItem: function({ commit, getters }) {
    const { armNames, designGridHeaders } = getters;
    const sampleTypeTemplate = {
      sampleType: null,
      selectedCells: armNames.reduce((o, key) => ({ ...o, [key]: Array(designGridHeaders.length - 1).fill(false) }), {}),
      sampleTypeSizes: armNames.reduce((o, key) => ({ ...o, [key]: Array(designGridHeaders.length - 1).fill(null) }), {})
    };
    commit(ADD_SAMPLE_PLAN_ITEM, sampleTypeTemplate);
  },
  // TODO evaluate whether this one must be removed
  removeSamplePlanItem: function() {},
  retrieveAssayConfigs: async function({ commit, getters }) {
    try {
      const response = await acApi.retrieveAll();
      const { armNames, designGridHeaders } = getters;
      const configs = response.data.map(config => {
        const selectedCells = armNames.reduce((o, key) => ({ ...o, [key]: Array(designGridHeaders.length - 1).fill(false) }), {});
        // const selectedSampleTypes = cloneDeep(getters.sampleTypeTable);
        // const sampleTypesPerCell = null;  we will need something to set sample types
        return {
          ...config,
          selectedCells: cloneDeep(selectedCells),
          selectedSampleTypes: cloneDeep(getters.sampleTypeTable)
        };
      });
      commit(ASSAY_CONFIGS_SET, configs);
    } catch (error) {
      commit(REMOTE_ERROR_SET, error);
    }
  },
  updateSampleTypesInAssayPlanItem: function({ commit, getters }, {
    assayIndex,
    armName,
    epochIndex,
    values
  }) {
    const annotations = getters.sampleTypeTable[armName][epochIndex];
    const selectedSampleTypes = filterAnnotationsByOptions(annotations, values);
    commit(ASSAY_TYPE_SAMPLE_TYPES_SET, { assayIndex, armName, epochIndex, values: selectedSampleTypes });
  }
};

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
