import { SCHOLARLY_ARTICLE_SUCCESS } from './mutation-types';

const state = {
  scholarlyArticle: null
};

const getters = {
  scholarlyArticle: state => state.scholarlyArticle || {}
};

const mutations = {
  [SCHOLARLY_ARTICLE_SUCCESS](state, { scholarlyArticle }) {
    state.scholarlyArticle = scholarlyArticle;
  }
};

const actions = {
  setScholarlyArticle({ commit, state }, payload) {
    commit(SCHOLARLY_ARTICLE_SUCCESS, {
      scholarlyArticle: payload.scholarlyArticle
    });
  }
};

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
};
