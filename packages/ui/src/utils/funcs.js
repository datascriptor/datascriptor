import { isString, isPlainObject } from 'lodash';

/**
 * @function
 * @name processAnnotations
 * @description coverts a string or an Ontology Annotation into an option to be fed to a q-select (or similar select)
 * @param {String/Object} value - if an object, it's an Ontology Annotation containg at least "term" (or "label") and "iri"
 */
export const processAnnotation = value => {
  if (isString(value)) {
    return {
      label: value,
      value: value
    };
  }
  if (isPlainObject(value)) {
    return {
      label: 'term' in value ? value.term : value.label,
      value: value.iri
    };
  }
};

/**
 * @function
 * @name processAnnotations
 * @description coverts a string or an Ontology Annotation into a list of options to be fed to a q-select (or similar select)
 * @param {Array} values - Array of strings or objects. If an object, it's an Ontology Annotation containg at least "term" (or "label") and "iri"
 */
export const processAnnotations = values => values.map(processAnnotation);

export const displayAnnotation = annotation => {
  if (!annotation) return null;
  if (isString(annotation)) {
    return annotation;
  }
  const res = 'term' in annotation ? annotation.term : annotation.label;
  return res;
};

export const filterAnnotationsByOptions = (allAvailableAnnotations, selectedDisplayOptions) => {
  return selectedDisplayOptions.map(displayOption => {
    const foundAnnotation = allAvailableAnnotations.find(annotation => isString(annotation) ? annotation === displayOption.value : annotation.iri === displayOption.value);
    // console.log(`Found Annotation = ${JSON.stringify(foundAnnotation)}`);
    return foundAnnotation;
  });
};
