export const required = value => !!value || 'This field is required';
export const positive = value => value > 0 || 'Please use a positive nnumber';
