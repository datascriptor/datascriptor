
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '', component: () => import('pages/PageHome.vue')
      },
      {
        path: '/login', component: () => import('pages/LogInAndRegistration.vue')
      },
      {
        path: '/resetPassword', component: () => import('pages/PasswordReset.vue')
      },
      {
        path: '/studyDesignPlanner', component: () => import('pages/StudyDesignEditor.vue')
      },
      {
        path: '/study/new', component: () => import('pages/StudyDesignEditor.vue')
      },
      {
        path: '/study/edit/:id', component: () => import('pages/StudyDesignEditor.vue')
      },
      {
        path: '/studies', component: () => import('pages/PageStudies.vue')
      },
      {
        path: '/study/:id', component: () => import('pages/StudyView.vue')
      },
      {
        path: '/articleMetadataView', component: () => import('pages/ScholarlyArticleMetadataView.vue')
      },
      {
        path: '/articleMetadataEdit', component: () => import('pages/ScholarlyArticleMetadataEdit.vue')
      },
      {
        path: '/oauth-redirect', component: () => import('pages/PageOauthRedirect.vue')
      }
    ]
  }
];

// Always leave this as last one
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue')
});

export default routes;
