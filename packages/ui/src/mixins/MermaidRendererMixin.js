import { mermaidAPI } from 'mermaid';

let graphIdCounter = 0;
const MERMAID_CONFIG = {
  theme: 'neutral',
  startOnLoad: false,
  flowchart: {
    width: '100%',
    useMaxWidth: true,
    htmlLabels: true,
    curve: 'cardinal'
  },
  securityLevel: 'loose'
};

export default {

  data() {
    return {
      elementPlanSvg: null,
      config: MERMAID_CONFIG,
      visible: true
    };
  },

  computed: {
    graph: () => `graph LR`
  },

  /**
     * @function
     * @name mounted()
     * Created the SVG from the mermaid text representation
     * TODO complete and test this
     */
  mounted() {
    mermaidAPI.initialize(this.config);
    mermaidAPI.render(
      `mermaid${++graphIdCounter}`,
      this.graph,
      (svg) => {
        console.log(`We're in the callback!!`);
        this.elementPlanSvg = svg;
        // document.body.removeChild(renderDiv);
      }
      // renderDiv
    );
  }

};
