import assayConfigs from '@datascriptor/api/test/fixtures/assay-configs.json';
import { capitalize, cloneDeep } from 'lodash';

const TEST_ID = '1234';

function interceptRestApi() {
  const configs = cloneDeep(assayConfigs);
  cy.intercept('GET', '/api/assayConfig', {
    statusCode: 200,
    body: configs
  });
  // FIXME: POST /study/api is not intercepted!!
  cy.intercept('POST', '/api/study', req => {
    return {
      statusCode: 201,
      body: {
        ...req.body,
        _id: TEST_ID
      } // fixme
    };
  });
}

describe('Opens the StudyDesign Planner Form page', () => {
  beforeEach(() => {
    cy.visit('/#/studyDesignPlanner');
  });

  it('has page header', () => {
    cy.get('h4').should('contain', 'Study Design Planner');
  });
});

describe('Sets up a factorial design (each group receives a single treatment)', () => {
  beforeEach(() => {
    interceptRestApi();
    cy.visit('/#/studyDesignPlanner');
  });

  it('populates the 4-step stepper', () => {
    cy.fixture('factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json').then(studyDesignPlannerJson => {
      const testName = 'whatever';
      const testDescr = 'bao';
      cy.get('input[name=studyName]').type(testName);
      cy.get('textarea[name=studyDescription]').type(testDescr);
      // click on the stuy design selector
      cy.get('[data-cy=select-studyDesignType]')
        .parent()
        .next()
        .children('i.q-select__dropdown-icon')
        .first()
        .click();
      // select the "single-treatment" study design type (the first option)
      cy.get('.q-virtual-scroll__content').children().first().click();
      cy.get('[data-cy=btn-generateSubjectGroups]').should('be.disabled');
      // input subject type
      cy.get('[data-cy=select-subjectType] input').type('Homo sapiens').type('{enter}');
      cy.get('[data-cy=btn-generateSubjectGroups]').should('not.be.disabled');
      // input group size
      cy.get('[data-cy=input-groupSize]').type('{backspace}{backspace}').type(8);
      // add an observational factor (name and values)
      cy.get('[data-cy=btn-addObservationalFactor]').click();
      cy.get('input[aria-label="Factor Name"]')
        .should('be.empty')
        .type('sex');
      cy.get('input[data-cy=select-factorVallues]')
        .type('M')
        .type('{enter}')
        .type('F')
        .type('{enter}');
      // generate subject groups
      cy.get('[data-cy=btn-generateSubjectGroups]').click();
      cy.get('button[data-cy=btn-step1Continue]').should('be.disabled');
      // select subject groups
      cy.get('[data-cy=table-subjectGroups]')
        .find('tr')
        .each($el => {
          $el.find('div.q-checkbox').click();
        });
      // move to Step 2
      cy.get('button[data-cy=btn-step1Continue]')
        .should('not.be.disabled')
        .click();
      cy.get('div.text-h5').should('contain', 'Treatment Plan');
      // Test Intervention type present then select an i. type
      cy.get('[data-cy=select-interventionType]').click().debug();
      cy.get('.q-item.q-item--clickable')
        .find('.q-item__label')
        .eq(4)
        .should('contain', 'drug intervention')
        .click();
      const {
        treatmentPlan: {
          screen,
          followUp,
          elementParams: {
            agents,
            intensities,
            intensityUnit,
            durations,
            durationUnit
          }
        },
        samplePlan: [bloodSamplePlan, salivaSamplePlan],
        assayPlan: [msAssayPlan, genSeqAssayPlan]
      } = studyDesignPlannerJson;
      for (const agent of agents) {
        cy.get('input[data-cy=select-agents]')
          .type(`${agent}{enter}`);
      }
      for (const intensity of intensities) {
        cy.get('input[data-cy=select-intensities]')
          .type(`${intensity}{enter}`);
      }
      cy.get('input[data-cy=input-intensityUnit]')
        .type(intensityUnit);
      for (const duration of durations) {
        cy.get('input[data-cy=select-durations]')
          .type(`${duration}{enter}`);
      }
      cy.get('input[data-cy=input-durationUnit]')
        .type(durationUnit);
      // screen
      cy.get('div[role=checkbox][data-cy=checkbox-hasScreen]').should('exist').click();
      cy.get('input[data-cy=input-screenDuration]').should('exist').type(screen.duration);
      cy.get('input[data-cy=input-screenDurationUnit]').should('exist').type(screen.durationUnit);
      // run-in (not set here)
      cy.get('div[role=checkbox][data-cy=checkbox-hasRunIn]').should('exist');
      cy.get('input[data-cy=input-runInDuration]').should('not.exist');
      cy.get('input[data-cy=input-runInDurationUnit]').should('not.exist');
      // washout (must not exist)
      cy.get('div[role=checkbox][data-cy=checkbox-hasWashout]').should('not.exist');
      // follow-up
      cy.get('div[role=checkbox][data-cy=checkbox-hasFollowUp]').should('exist').click();
      cy.get('input[data-cy=input-followUpDuration]').should('exist').type(followUp.duration);
      cy.get('input[data-cy=input-followUpDurationUnit]').should('exist').type(followUp.durationUnit);
      // generate study arms
      cy.get('div[data-cy=table-studyArmsPicker]').should('not.exist');
      cy.get('button[data-cy=btn-generateStudyArms]').click();
      cy.get('button[data-cy=btn-step2Continue]')
        .should('be.disabled');
      cy.get('div[data-cy=table-studyArmsPicker]')
        .should('exist')
        .find('table.q-table')
        .find('tbody')
        .find('tr')
        .debug()
        .each(($tr, ix) => {
          if (ix % 3 === 0) {
            cy.wrap($tr).find('div[role=checkbox]').click();
          }
        });
      // move to Step 3 - Sampling Plan
      cy.get('button[data-cy=btn-step2Continue]')
        .should('not.be.disabled')
        .click();
      cy.get('div.text-h5').should('contain', 'Sampling Plan');
      cy.get('button[data-cy=btn-addSampleType]').click();
      cy.get('label[data-cy=autocomplete-sampleType] input')
        .type(bloodSamplePlan.sampleType);
      cy.get('div[data-cy=table-samplePlan]')
        .should('exist')
        .find('table.q-table > tbody > tr')
        .first()
        .find('td')
        .each(($td, ix) => {
          if ($td.find('div[role=checkbox]').length) {
            cy.wrap($td).find('div[role=checkbox]').click();
            if (ix % 2 === 0) {
              cy.wrap($td).find('[data-cy=input-nbSamples]').type('{backspace}').type(3);
              cy.wrap($td).find('button[data-cy=btn-applyToEpoch]').click();
            }
          }
        });
      // move to Step 4 - Assay Plan
      cy.get('button[data-cy=btn-step3Continue]')
        .should('not.be.disabled')
        .click();
      cy.get('div.text-h5').should('contain', 'Assay Plan');
      cy.get('div[data-cy=select-assayPlan]')
        .click();
      // now all the available options should have been attached to the DOM
      // let's select a couple of them
      cy.get('div.q-item[data-cy=item-assayType]')
        .find('div.q-item__label')
        .debug();
      cy.get('div.q-item[data-cy=item-assayType]')
        .find('div.q-item__label')
        .contains(capitalize(msAssayPlan.name))
        .debug()
        .click();
      cy.get('div[data-cy=div-assayEditor]').should('exist');
      cy.get('[data-cy="input-extraction_nbReplicates"]')
        .click()
        .type('{backspace}')
        .type(2);
      // set extract type
      cy.get('[data-cy="select-extract_extract type"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('polar fraction')
        .click();
      // set labeled extract type
      cy.get('div[data-cy="select-labeled extract_labeled extract type"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('label_0')
        .click();
      cy.get('input[data-cy="input-mass spectrometry_nbReplicates"]')
        .click()
        .type('{backspace}')
        .type(3);
      // set mass spectrometry instrument
      cy.get('div[data-cy="select-mass spectrometry_instrument"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('Agilent QTQF 6510')
        .click();
      // set mass spectrometry injection mode (FIA and LC)
      cy.get('div[data-cy="select-mass spectrometry_injection_mode"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('FIA')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('LC')
        .click();
      cy.window().trigger('blur');
      // set mass spectrometry acquisition mode (positive)
      cy.get('div[data-cy="select-mass spectrometry_acquisition_mode"]')
        .debug()
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('positive mode')
        .click();
      cy.get('div[data-cy="table-assayPlan_metabolite profiling - mass spectrometry (ontology annotated)"]')
        .find('table.q-table > tbody > tr')
        .each($tr => {
          cy.wrap($tr).find('td').eq(2).find('div[role=checkbox]').click();
        });
      // FIXME: POST /study/api is not intercepted!!
      cy.get('button[role=button][data-cy=btn-createStudy]').click();
    });
  });
});

describe('Sets up a crossover design (each group receives 2 treatments)', () => {
  beforeEach(() => {
    interceptRestApi();
    cy.visit('/#/studyDesignPlanner');
  });

  it('populates the 4-step stepper', () => {
    cy.fixture('crossover-study-design-4-arms-blood-derma-nmr-ms.json').then(studyDesignPlannerJson => {
      cy.get('input[name=studyName]').type(studyDesignPlannerJson.name);
      cy.get('textarea[name=studyDescription]').type(studyDesignPlannerJson.description);
      // click on the stuy design selector
      cy.get('[data-cy=select-studyDesignType]')
        .parent()
        .next()
        .children('i.q-select__dropdown-icon')
        .first()
        .click();
      cy.get('.q-virtual-scroll__content').children().eq(1).click();
      cy.get('[data-cy=btn-generateSubjectGroups]').should('be.disabled');
      // input subject type
      cy.get('[data-cy=select-subjectType] input').type('Homo sapiens').type('{enter}');
      cy.get('[data-cy=btn-generateSubjectGroups]').should('not.be.disabled');
      // input group size
      cy.get('[data-cy=input-groupSize]').type('{backspace}{backspace}').type(12);
      // add an observational factor (name and values)
      cy.get('[data-cy=btn-addObservationalFactor]').click();
      cy.get('input[aria-label="Factor Name"]')
        .should('be.empty')
        .type('sex');
      cy.get('input[data-cy=select-factorVallues]')
        .type('M')
        .type('{enter}')
        .type('F')
        .type('{enter}');
      // generate subject groups
      cy.get('[data-cy=btn-generateSubjectGroups]').click();
      cy.get('button[data-cy=btn-step1Continue]').should('be.disabled');
      // select subject groups
      cy.get('[data-cy=table-subjectGroups]')
        .find('tr')
        .each($el => {
          $el.find('div.q-checkbox').click();
        });
      // move to Step 2
      cy.get('button[data-cy=btn-step1Continue]')
        .should('not.be.disabled')
        .click();

      const {
        treatmentPlan: {
          treatments: [firstTreatment, secondTreatment],
          screen, runIn, washout, followUp
        },
        samplePlan: [bloodSamplePlan, dermaSamplePlan],
        assayPlan: [ msAssayPlan, nmrAssayPlan ]
      } = studyDesignPlannerJson;
      // select intervention typec for first treatment (drug treatment)
      cy.get('[data-cy=select-interventionType_0]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains(firstTreatment.interventionType.label)
        .click();
      cy.get('[data-cy=input-agent_0]').type(firstTreatment.agent);
      cy.get('[data-cy=input-intensity_0]').type(firstTreatment.intensity);
      cy.get('[data-cy=input-intensityUnit_0]').type(firstTreatment.intensityUnit);
      cy.get('[data-cy=input-duration_0]').type(firstTreatment.duration);
      cy.get('[data-cy=input-durationUnit_0]').type(firstTreatment.durationUnit);
      // select intervention type and factor values for second treatment (biological treatment)
      cy.get('[data-cy=select-interventionType_1]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains(secondTreatment.interventionType.label)
        .click();
      cy.get('[data-cy=input-agent_1]').type(secondTreatment.agent);
      cy.get('[data-cy=input-intensity_1]').type(secondTreatment.intensity);
      cy.get('[data-cy=input-intensityUnit_1]').type(secondTreatment.intensityUnit);
      cy.get('[data-cy=input-duration_1]').type(secondTreatment.duration);
      cy.get('[data-cy=input-durationUnit_1]').type(secondTreatment.durationUnit);
      // TODO test the add treatment button
      // screen
      cy.get('div[role=checkbox][data-cy=checkbox-hasScreen]').should('exist').click();
      cy.get('input[data-cy=input-screenDuration]').should('exist').type(screen.duration);
      cy.get('input[data-cy=input-screenDurationUnit]').should('exist').type(screen.durationUnit);
      // run-in
      cy.get('div[role=checkbox][data-cy=checkbox-hasRunIn]').should('exist').click();
      cy.get('input[data-cy=input-runInDuration]').should('exist').type(runIn.duration);
      cy.get('input[data-cy=input-runInDurationUnit]').should('exist').type(runIn.durationUnit);
      // washout
      cy.get('div[role=checkbox][data-cy=checkbox-hasWashout]').should('exist').click();
      cy.get('input[data-cy=input-washoutDuration]').should('exist').type(washout.duration);
      cy.get('input[data-cy=input-washoutDurationUnit]').should('exist').type(washout.durationUnit);
      // follow-up
      cy.get('div[role=checkbox][data-cy=checkbox-hasFollowUp]').should('exist').click();
      cy.get('input[data-cy=input-followUpDuration]').should('exist').type(followUp.duration);
      cy.get('input[data-cy=input-followUpDurationUnit]').should('exist').type(followUp.durationUnit);
      // generate crossover study arms
      cy.get('div[data-cy=table-studyArmsPicker]').should('not.exist');
      cy.get('button[data-cy=btn-generateStudyArms]').click();
      // click all arms in the table
      cy.get('div[data-cy=table-studyArmsPicker]')
        .should('exist')
        .find('table.q-table')
        .find('tbody')
        .find('tr')
        .each($tr => {
          cy.wrap($tr).find('div[role=checkbox]').click();
        });
      // move to Step 3 - Sampling Plan
      cy.get('button[data-cy=btn-step2Continue]')
        .should('not.be.disabled')
        .click();
      cy.get('div.text-h5').should('contain', 'Sampling Plan');
      // set derma sample plan
      cy.get('button[data-cy=btn-addSampleType]').click();
      cy.get('label[data-cy=autocomplete-sampleType] input')
        .type(dermaSamplePlan.sampleType);
      cy.get('div[data-cy=table-samplePlan]')
        .should('exist')
        .find('table.q-table > tbody > tr')
        .first()
        .find('td')
        .each(($td, ix) => {
          if ($td.find('div[role=checkbox]').length) {
            if (ix % 2 === 0) {
              cy.wrap($td).find('div[role=checkbox]').click();
              cy.wrap($td).find('[data-cy=input-nbSamples]').type('{backspace}').type(3);
              cy.wrap($td).find('button[data-cy=btn-applyToEpoch]').click();
            }
          }
        });
      // move to Step 4 - Assay Plan
      cy.get('button[data-cy=btn-step3Continue]')
        .should('not.be.disabled')
        .click();
      cy.get('div.text-h5').should('contain', 'Assay Plan');
      cy.get('div[data-cy=select-assayPlan]')
        .click();
      // now all the available options should have been attached to the DOM
      // let's select a couple of them
      cy.get('div.q-item[data-cy=item-assayType]')
        .find('div.q-item__label')
        .debug();
      cy.get('div.q-item[data-cy=item-assayType]')
        .find('div.q-item__label')
        .contains(capitalize(nmrAssayPlan.name))
        .debug()
        .click();
      cy.get('div[data-cy=div-assayEditor]').should('exist');
      cy.get('[data-cy="input-extraction_nbReplicates"]')
        .click()
        .type('{backspace}')
        .type(2);
      // set extract type
      cy.get('[data-cy="select-extract_extract type"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('supernatant')
        .click();
      // set NMR instrument
      cy.get('div[data-cy="select-nmr_spectroscopy_instrument"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('Bruker Avance II 1 GHz')
        .click();
      // set mass spectrometry injection mode (FIA and LC)
      cy.get('div[data-cy="select-nmr_spectroscopy_pulse_sequence"]')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('TOCSY')
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('watergate')
        .click();
      cy.window().trigger('blur');
      // set mass spectrometry acquisition mode (positive)
      cy.get('div[data-cy="select-nmr_spectroscopy_acquisition_mode"]')
        .debug()
        .click();
      cy.get('.q-item__section.q-item__section--main > .q-item__label')
        .contains('2D 13C-13C NMR')
        .click();
      cy.get('div[data-cy="table-assayPlan_metabolite profiling using NMR spectroscopy"]')
        .find('table.q-table > tbody > tr')
        .each($tr => {
          cy.wrap($tr).find('td').eq(2).find('div[role=checkbox]').click();
        });
      // FIXME: POST /study/api is not intercepted!!
      cy.get('button[role=button][data-cy=btn-createStudy]').click();
    });
  });
});
