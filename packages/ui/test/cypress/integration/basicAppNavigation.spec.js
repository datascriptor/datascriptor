describe('Unauthenticated User Navigation', function() {
  beforeEach(() => {
    cy.fixture('all-studies').as('testStudies');
    cy.fixture('all-studies').then(testStudies => {
      /*
      cy.intercept('GET', '/api/study', {
        statusCode: 200,
        body: testStudies
      }); */
      // FIXME see if I can make it work regardless of the query strings
      cy.intercept('GET', '/api/study?skip=0&limit=5', {
        statusCode: 200,
        body: testStudies
      });
      testStudies.forEach(testStudy => {
        // FIXME does this match overlap with the one above?
        cy.intercept('GET', `/api/study/${testStudy._id}`, {
          statusCode: 200,
          body: testStudy
        });
      });
      cy.visit('/');
    });
  });

  it('has expected elements: title, table of existing studies', () => {
    cy.get('title')
      .should('contain', 'Datascriptor');
    cy.get('[data-cy=studiesTable] .q-table__title').should('have.text', 'Existing Studies');
    cy.get('[data-cy=studiesTable]  table')
      .find('tbody > tr')
      .should('have.length.greaterThan', 0);
    cy.get('[data-cy=studiesTable]  table')
      .find('tbody > tr')
      .each($tr => {
        cy.wrap($tr).find('[data-cy=viewStudyBtn]')
          .should('contain.text', 'View');
        cy.wrap($tr)
          .find('[data-cy=cloneStudyBtn]')
          .should('contain.text', 'Clone');
      });
  });

  it('navigates to a study page and back to home', function() {
    const testId = this.testStudies[0]._id;
    cy.get(`[data-cy=studyBtnGroup-${testId}`)
      .find('[data-cy=viewStudyBtn]')
      .click();
    // if the hash ('#') is removed it will be location('pathname')
    cy.location('hash').should('have.string', `study/${testId}`);
    cy.get('.text-h4.ds-study-header').debug();
    cy.get('.text-h4.ds-study-header')
      .should($el => {
        // need to trim spurious space/newline characters from the text
        expect($el.text().trim()).to.equal(this.testStudies[0].name);
      });
  });
});
