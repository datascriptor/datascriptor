import { cloneDeep } from 'lodash';
import { LocalStorage } from 'quasar';
import { state, getters, mutations, actions } from 'src/store/auth';
import {
  USER_SET, TOKEN_SET
} from 'src/store/mutation-types';

const testUser = {
  name: 'Pippo',
  email: 'pippo@pippo.org',
  isAdmin: true
};
const testToken = 'dgfrhtjhpth.ghirjhoptjhop.jtkjhkj';

let testBaseState, testPopulatedState;

beforeEach(() => {
  testBaseState = cloneDeep(state);
  testPopulatedState = {
    user: cloneDeep(testUser),
    token: testToken
  };
});

describe('studyDesignPlanner', () => {
  describe('getters', () => {
    test('isLoggedIn', () => {
      expect(getters.isSignedIn(testBaseState)).toBe(false);
      expect(getters.isSignedIn(testPopulatedState)).toEqual(true);
    });
    test('username', () => {
      expect(getters.username(testBaseState)).toBe(null);
      expect(getters.username(testPopulatedState)).toEqual(testUser.name);
    });
    test('isAdmin', () => {
      expect(getters.isAdmin(testBaseState)).toEqual(false);
      expect(getters.isAdmin(testPopulatedState)).toEqual(true);
    });
  });

  describe('mutations', () => {
    test('USER_SET', () => {
      mutations[USER_SET](testBaseState, testUser);
      expect(testBaseState.user).toStrictEqual(testUser);
    });

    test('TOKEN_SET', () => {
      mutations[TOKEN_SET](testBaseState, testToken);
      expect(testBaseState.token).toStrictEqual(testToken);
    });
  });

  describe('actions', () => {
    beforeEach(() => {
      LocalStorage.set = jest.fn();
    });

    test('setCredentials', () => {
      const commit = jest.fn();
      actions.setCredentials({ commit }, {
        user: testUser,
        token: testToken
      });
      expect(commit).toHaveBeenCalledWith(USER_SET, testUser);
      expect(commit).toHaveBeenCalledWith(TOKEN_SET, testToken);
      expect(LocalStorage.set).toHaveBeenCalledWith('user', testUser);
      expect(LocalStorage.set).toHaveBeenCalledWith('token', testToken);
    });
  });
});
