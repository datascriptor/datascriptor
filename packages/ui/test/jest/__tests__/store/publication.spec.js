import publication from 'src/store/publication';
import {
    SCHOLARLY_ARTICLE_SUCCESS
} from 'src/store/mutation-types';

let testState;

const testScholarlyArticle = {
    name: 'test Article',
    abstract: 'this article is about something',
    authors: ['Massi Izzo'],
    keywords: []
}

beforeEach(() => {
    testState = {
        scholarlyArticle: null
    };
});

describe('publication', () => {
    describe('getters', () => {
        describe('scholarlyArticle', () => {
            test('gets the \'studyData\' property from the state', () => {
                expect(publication.getters.scholarlyArticle(testState)).toStrictEqual({});
                testState = {
                    scholarlyArticle: testScholarlyArticle
                };
                expect(publication.getters.scholarlyArticle(testState)).toStrictEqual(testState.scholarlyArticle);
            });
        });
    });

    describe('mutations', () => {
        test('SCHOLARLY_ARTICLE_SUCCESS', () => {
            publication.mutations[SCHOLARLY_ARTICLE_SUCCESS](testState, { scholarlyArticle: testScholarlyArticle });
            expect(testState.scholarlyArticle).toStrictEqual(testScholarlyArticle);
        });
    });

    describe('actions', () => {
        let commit, payload;

        beforeEach(() => {
            commit = jest.fn();
            payload = {
                scholarlyArticle: testScholarlyArticle
            };
        });

        describe('setScholarlyArticle', () => {
            test('it commits a SCHOLARLY_ARTICLE_SUCCESS mutation', () => {
                publication.actions.setScholarlyArticle({
                    commit,
                    state: testState
                }, payload);
                expect(commit).toBeCalledWith(SCHOLARLY_ARTICLE_SUCCESS, {
                    scholarlyArticle: payload.scholarlyArticle
                });
            });
        });
    });
});
