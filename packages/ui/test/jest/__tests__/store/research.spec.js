import study from 'src/store/research';
import {
  STUDY_SET
} from 'src/store/mutation-types';

let testState;

beforeEach(() => {
  testState = {
    study: {},
    studyReport: {}
  };
});

describe('study', () => {
  describe('getters', () => {});

  describe('mutations', () => {
    test('STUDY_SET', () => {
      const testStudy = { design: {}, dataset: [] };
      study.mutations[STUDY_SET](testState, testStudy);
      expect(testState.study).toStrictEqual(testStudy);
    });
  });

  describe('actions', () => {});
});
