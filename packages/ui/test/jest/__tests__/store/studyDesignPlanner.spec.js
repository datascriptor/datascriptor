import { state, getters, mutations, actions } from 'src/store/studyDesignPlanner';
import populatedStudyDesignPlannerState from '../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms';
import allStudies from '../../../fixtures/all-studies.json';
import { cloneDeep } from 'lodash';
import axios from 'axios';
import { NUM_REPLICATES } from 'src/utils/constants';
import {
  NAME_SET, DESCRIPTION_SET, SUBJECT_TYPE_SET,
  SUBJECT_SIZE_SET, DESIGN_TYPE_SET,
  OBSERVATIONAL_FACTOR_ADD, OBSERVATIONAL_FACTOR_REMOVE,
  OBSERVATIONAL_FACTOR_NAME_SET, OBSERVATIONAL_FACTOR_IS_QUANTITATIVE_SET,
  OBSERVATIONAL_FACTOR_VALUES_SET, OBSERVATIONAL_FACTOR_UNIT_SET,
  GENERATED_SUBJECT_GROUPS, SELECTED_SUBJECT_GROUPS_SET,
  OBSERVATION_PERIOD_DURATION_SET, OBSERVATION_PERIOD_DURATION_UNIT_SET, OBSERVATION_PERIOD_RESET,
  SCREEN_DURATION_SET, SCREEN_DURATION_UNIT_SET, SCREEN_RESET,
  RUN_IN_DURATION_SET, RUN_IN_DURATION_UNIT_SET, RUN_IN_RESET,
  WASHOUT_DURATION_SET, WASHOUT_DURATION_UNIT_SET, WASHOUT_RESET,
  FOLLOW_UP_DURATION_SET, FOLLOW_UP_DURATION_UNIT_SET, FOLLOW_UP_RESET,
  ADD_TREATMENT, REMOVE_TREATMENT,
  INTERVENTION_TYPE_SET, AGENT_SET, INTENSITY_SET, INTENSITY_UNIT_SET,
  DURATION_SET, DURATION_UNIT_SET,
  FACTORIAL_INTERVENTION_TYPE_SET, FACTORIAL_AGENTS_SET, FACTORIAL_INTENSITES_SET,
  FACTORIAL_INTENSITY_UNIT_SET, FACTORIAL_DURATIONS_SET, FACTORIAL_DURATION_UNIT_SET,
  REMOTE_ERROR_SET, REMOTE_ERROR_RESET,
  GENERATED_STUDY_DESIGN_SET, SELECTED_ARMS_SET, STUDY_DESIGN_SET,
  ADD_SAMPLE_PLAN_ITEM, REMOVE_SAMPLE_PLAN_ITEM,
  SAMPLE_TYPE_SET,
  SAMPLE_TYPE_SELECTED_CELL_TOGGLE, SAMPLE_TYPE_SIZE_SET,
  SAMPLE_TYPE_SIZE_APPLY_TO_ARM, SAMPLE_TYPE_SIZE_APPLY_TO_EPOCH,
  ASSAY_CONFIGS_SET, ASSAY_TYPE_DESELECT, ASSAY_TYPE_SELECT,
  ASSAY_IN_TAB_SET, NUM_REPLICATES_SET,
  UPDATE_SELECTED_PARAMETER_VALUES, CREATE_NEW_PARAMETER_VALUE,
  UPDATE_SELECTED_CHARACTERISTIC_VALUES,
  ASSAY_TYPE_SELECTED_CELL_TOGGLE, ASSAY_TYPE_SAMPLE_TYPES_SET,
  NON_TREATMENT_ELEMENT_SET, NON_TREATMENT_ELEMENT_RESET, REMOTE_REQUEST, RESET_ALL, 
  WHOLE_STUDY_DESIGN_PLANNER_SET
} from 'src/store/mutation-types';

const testAssayConfigs = [
  { id: 0, name: 'Mass Spectrometry' },
  { id: 1, name: 'NMR' },
  { id: 2, name: 'Whole Genome Sequencing' }
];

let testBaseState, testPopulatedState;

jest.mock('axios');

beforeEach(() => {
  testBaseState = cloneDeep(state);
  testPopulatedState = cloneDeep(populatedStudyDesignPlannerState);
});

describe('studyDesignPlanner', () => {
  describe('getters', () => {
    test('subjectGroups - empty state', () => {
      expect(getters.subjectGroups(testBaseState)).toEqual([]);
    });

    test('subjectGroups - populated state', () => {
      expect(getters.subjectGroups(testPopulatedState)).toEqual([
        ...testPopulatedState.subjectGroups.selected, ...testPopulatedState.subjectGroups.unselected
      ]);
    });

    test('hasSubjectGroups - empty state', () => {
      const mockedGetters = {
        subjectGroups: getters.subjectGroups(testBaseState)
      };
      expect(getters.hasGeneratedSubjectGroups(testBaseState, mockedGetters)).toEqual(false);
    });

    test('hasSubjectGroups - populated state', () => {
      const mockedGetters = {
        subjectGroups: getters.subjectGroups(testPopulatedState)
      };
      expect(getters.hasGeneratedSubjectGroups(testPopulatedState, mockedGetters)).toEqual(true);
    });

    test('observationalFactorCount', () => {
      expect(getters.observationalFactorCount(testBaseState)).toEqual(0);
      expect(getters.observationalFactorCount(testPopulatedState)).toEqual(testPopulatedState.observationalFactors.length);
    });

    test('treatmentIds', () => {
      expect(getters.treatmentIds(testBaseState)).toEqual([0, 1]);
    });

    test('arms', () => {
      expect(getters.arms(testBaseState)).toEqual([]);
      const allArms = getters.arms(testPopulatedState);
      expect(allArms.length).toBe(testPopulatedState.arms.selected.length + testPopulatedState.arms.unselected.length);
    });

    test('hasGeneratedStudyDesign - empty state', () => {
      const mockedGetters = {
        arms: getters.arms(testBaseState)
      };
      expect(getters.hasGeneratedStudyDesign(testBaseState, mockedGetters)).toBe(false);
    });

    test('hasGeneratedStudyDesign - populated state', () => {
      const mockedGetters = {
        arms: getters.arms(testPopulatedState)
      };
      expect(getters.hasGeneratedStudyDesign(testPopulatedState, mockedGetters)).toBe(true);
    });

    test('hasSelectedArms', () => {
      expect(getters.hasSelectedArms(testBaseState)).toBe(false);
    });

    test('armNames', () => {
      expect(getters.armNames(testBaseState)).toEqual([]);
    });

    test('designGridHeaders', () => {
      expect(getters.designGridHeaders(testBaseState)).toEqual(null);
      const gridHeaders = getters.designGridHeaders(testPopulatedState);
      expect(Array.isArray(gridHeaders)).toBe(true);
      expect(gridHeaders.length).toBeGreaterThan(0);
    });

    test('designGridBody - empty state', () => {
      expect(getters.designGridBody(testBaseState)).toEqual(null);
    });

    test('designGridBody - populated store', () => {
      const mockedGetters = {
        designGridHeaders: getters.designGridHeaders(testPopulatedState)
      };
      const gridBody = getters.designGridBody(testPopulatedState, mockedGetters);
      expect(Array.isArray(gridBody)).toBe(true);
      expect(gridBody.length).toBeGreaterThan(0);
    });

    test('sampleTypeCount', () => {
      expect(getters.sampleTypeCount(testBaseState)).toEqual(0);
    });

    test('assayConfigDetails', () => {
      const currState = {
        ...testBaseState,
        assayConfigs: [
          { id: 0, name: 'some assay', measurement_type: 'proteomic profile', technology_type: 'mass.spec.', arms: [], elements: [], events: [] },
          { id: 1, name: 'some other assay', measurement_type: 'proteomic profile', technology_type: 'NMR', arms: [], elements: [], events: [] }
        ]
      };
      getters.assayConfigDetails(currState).map((configDetail, ix) => {
        expect(configDetail.id).toBe(currState.assayConfigs[ix].id);
        expect(configDetail.name).toBe(currState.assayConfigs[ix].name);
        expect(configDetail.measurement_type).toBe(currState.assayConfigs[ix].measurement_type);
        expect(configDetail.technology_type).toBe(currState.assayConfigs[ix].technology_type);
        expect(configDetail.arms).toBeUndefined();
        expect(configDetail.elements).toBeUndefined();
        expect(configDetail.events).toBeUndefined();
      });
    });

    test('assayTypeCount', () => {});

    test('sampleItemByIndex', () => {
      const samplePlanIndex = 1;
      expect(getters.sampleItemByIndex(testPopulatedState)(samplePlanIndex)).toStrictEqual(
        testPopulatedState.samplePlan[samplePlanIndex]
      );
    });

    test('sampleSelectedCellsByIndex', () => {
      const samplePlanIndex = 1;
      expect(getters.sampleSelectedCellsByIndex(testPopulatedState)(samplePlanIndex)).toStrictEqual(
        testPopulatedState.samplePlan[samplePlanIndex].selectedCells
      );
    });

    test('sampleTypeTable', () => {
      const actualRes = getters.sampleTypeTable(testPopulatedState);
      expect(actualRes).toBeTruthy();
    });

    // TODO
    test('selectedAssayTypes', () => {});
    test('stepOneCompleted', () => {});
    test('stepTwoCompleted', () => {});
    test('stepThreeCompleted', () => {});
    test('stepFourCompleted', () => {});

    test('studyForSubmission', () => {
      const actualRes = getters.studyForSubmission(testPopulatedState);
      expect(actualRes._id).toBe(testPopulatedState._id);
      expect(actualRes.name).toBe(testPopulatedState.name);
      expect(actualRes.description).toBe(testPopulatedState.description);
      expect(actualRes).toHaveProperty('design');
    });
  });

  describe('mutations', () => {
    test('WHOLE_STUDY_DESIGN_PLANNER_SET', () => {
      const testStudy = allStudies[0];
      mutations[WHOLE_STUDY_DESIGN_PLANNER_SET](testBaseState, testStudy);
      expect(testBaseState.isPending).toBe(false);
      expect(testBaseState.remoteError).toBeNull();
      expect(testBaseState.name).toBe(testStudy.name);
      expect(testBaseState.description).toBe(testStudy.description);
      for (const key of Object.keys(testStudy.design)) {
        expect(testBaseState[key]).toStrictEqual(testStudy.design[key]);
      }
    });

    test('NAME_SET', () => {
      const testName = 'some name';
      mutations[NAME_SET](testBaseState, testName);
      expect(testBaseState.name).toBe(testName);
    });

    test('DESCRIPTION_SET', () => {
      const testDescription = 'some description';
      mutations[DESCRIPTION_SET](testBaseState, testDescription);
      expect(testBaseState.description).toBe(testDescription);
    });

    test('SUBJECT_TYPE_SET', () => {
      const testSubjectType = 'Homo Sapiens';
      mutations[SUBJECT_TYPE_SET](testBaseState, testSubjectType);
      expect(testBaseState.subjectType).toBe(testSubjectType);
    });

    test('SUBJECT_SIZE_SET', () => {
      const testSubjectSize = 8;
      mutations[SUBJECT_SIZE_SET](testBaseState, testSubjectSize);
      expect(testBaseState.subjectSize).toBe(testSubjectSize);
    });

    test('OBSERVATIONAL_FACTOR_ADD', () => {
      expect(testBaseState.observationalFactors).toHaveLength(0);
      mutations[OBSERVATIONAL_FACTOR_ADD](testBaseState);
      expect(testBaseState.observationalFactors).toHaveLength(1);
    });

    test('OBSERVATIONAL_FACTOR_REMOVE', () => {
      const observationalFactorCount = testPopulatedState.observationalFactors.length;
      mutations[OBSERVATIONAL_FACTOR_REMOVE](testPopulatedState);
      expect(testPopulatedState.observationalFactors).toHaveLength(observationalFactorCount - 1);
    });

    test('OBSERVATIONAL_FACTOR_NAME_SET', () => {
      const testFactorName = 'weight group';
      const testIndex = 0;
      mutations[OBSERVATIONAL_FACTOR_NAME_SET](testPopulatedState, {
        index: testIndex,
        name: testFactorName
      });
      expect(testPopulatedState.observationalFactors[testIndex].name).toBe(testFactorName);
    });

    test('OBSERVATIONAL_FACTOR_IS_QUANTITATIVE_SET', () => {
      const testFlag = true;
      const testIndex = 0;
      mutations[OBSERVATIONAL_FACTOR_IS_QUANTITATIVE_SET](testPopulatedState, {
        index: testIndex,
        flag: testFlag
      });
      expect(testPopulatedState.observationalFactors[testIndex].isQuantitative).toBe(testFlag);
    });

    test('OBSERVATIONAL_FACTOR_VALUES_SET', () => {
      const testFactorValues = ['category A', 'category B', 'category C'];
      const testIndex = 0;
      mutations[OBSERVATIONAL_FACTOR_VALUES_SET](testPopulatedState, {
        index: testIndex,
        values: testFactorValues
      });
      expect(testPopulatedState.observationalFactors[testIndex].values).toBe(testFactorValues);
    });

    test('OBSERVATIONAL_FACTOR_UNIT_SET', () => {
      const testFactorUnit = 'kg';
      const testIndex = 0;
      mutations[OBSERVATIONAL_FACTOR_UNIT_SET](testPopulatedState, {
        index: testIndex,
        unit: testFactorUnit
      });
      expect(testPopulatedState.observationalFactors[testIndex].unit).toBe(testFactorUnit);
    });

    test('GENERATED_SUBJECT_GROUPS', () => {
      const testGeneratedSubjectGroup = cloneDeep(testPopulatedState.subjectGroups.selected);
      mutations[GENERATED_SUBJECT_GROUPS](testBaseState, testGeneratedSubjectGroup);
      expect(testBaseState.subjectGroups.unselected).toEqual(testGeneratedSubjectGroup);
      expect(testBaseState.subjectGroups.selected).toEqual([]);
    });

    test('SELECTED_SUBJECT_GROUPS_SET', () => {
      const options = [{ name: 'SubjectGroup_1' }, { name: 'SubjectGroup_2' }];
      expect(testPopulatedState.subjectGroups.selected).toHaveLength(4);
      expect(testPopulatedState.subjectGroups.unselected).toHaveLength(0);
      mutations[SELECTED_SUBJECT_GROUPS_SET](testPopulatedState, options);
      expect(testPopulatedState.subjectGroups.selected).toHaveLength(2);
      expect(testPopulatedState.subjectGroups.unselected).toHaveLength(2);
    });

    test('DESIGN_TYPE_SET', () => {
      const testDesignType = 'crossoverDesign';
      mutations[DESIGN_TYPE_SET](testBaseState, testDesignType);
      expect(testBaseState.designType).toBe(testDesignType);
    });

    test('NON_TREATMENT_ELEMENT_SET', () => {
      const testElementType = 'screen';
      mutations[NON_TREATMENT_ELEMENT_SET](testBaseState, testElementType);
      expect(testBaseState.treatmentPlan[testElementType].selected).toBe(true);
    });

    test('NON_TREATMENT_ELEMENT_RESET', () => {
      const testElementType = 'screen';
      mutations[NON_TREATMENT_ELEMENT_RESET](testPopulatedState, testElementType);
      expect(testPopulatedState.treatmentPlan[testElementType].selected).toBe(false);
      expect(testPopulatedState.treatmentPlan[testElementType].duration).toBe(null);
      expect(testPopulatedState.treatmentPlan[testElementType].durationUnit).toBe('');
    });

    test('SCREEN_DURATION_SET', () => {
      const screenDuration = 10.3;
      mutations[SCREEN_DURATION_SET](testBaseState, screenDuration);
      expect(testBaseState.treatmentPlan.screen.duration).toBe(screenDuration);
    });

    test('SCREEN_DURATION_UNIT_SET', () => {
      const screenDurationUnit = 'Days';
      mutations[SCREEN_DURATION_UNIT_SET](testBaseState, screenDurationUnit);
      expect(testBaseState.treatmentPlan.screen.durationUnit).toBe(screenDurationUnit);
    });

    test('OBSERVATION_PERIOD_DURATION_SET', () => {
      const opDuration = 10.3;
      mutations[OBSERVATION_PERIOD_DURATION_SET](testBaseState, opDuration);
      expect(testBaseState.treatmentPlan.observationPeriod.duration).toBe(opDuration);
    });

    test('OBSERVATION_PERIOD_DURATION_UNIT_SET', () => {
      const opDurationUnit = 'Days';
      mutations[OBSERVATION_PERIOD_DURATION_UNIT_SET](testBaseState, opDurationUnit);
      expect(testBaseState.treatmentPlan.observationPeriod.durationUnit).toBe(opDurationUnit);
    });

    test('OBSERVATION_PERIOD_RESET', () => {
      mutations[OBSERVATION_PERIOD_RESET](testBaseState);
      expect(testBaseState.treatmentPlan.observationPeriod.duration).toBe(null);
      expect(testBaseState.treatmentPlan.observationPeriod.durationUnit).toBe('');
    });

    test('RUN_IN_DURATION_SET', () => {
      const testRunInDuration = 10.3;
      mutations[RUN_IN_DURATION_SET](testBaseState, testRunInDuration);
      expect(testBaseState.treatmentPlan.runIn.duration).toBe(testRunInDuration);
    });

    test('RUN_IN_DURATION_UNIT_SET', () => {
      const testRunInDurationUnit = 'Days';
      mutations[RUN_IN_DURATION_UNIT_SET](testBaseState, testRunInDurationUnit);
      expect(testBaseState.treatmentPlan.runIn.durationUnit).toBe(testRunInDurationUnit);
    });

    test('WASHOUT_DURATION_SET', () => {
      const testWashoutDuration = 10.3;
      mutations[WASHOUT_DURATION_SET](testBaseState, testWashoutDuration);
      expect(testBaseState.treatmentPlan.washout.duration).toBe(testWashoutDuration);
    });

    test('WASHOUT_DURATION_UNIT_SET', () => {
      const testWashoutDurationUnit = 'Days';
      mutations[WASHOUT_DURATION_UNIT_SET](testBaseState, testWashoutDurationUnit);
      expect(testBaseState.treatmentPlan.washout.durationUnit).toBe(testWashoutDurationUnit);
    });

    test('FOLLOW_UP_DURATION_SET', () => {
      const testFollowUpDuration = 10.3;
      mutations[FOLLOW_UP_DURATION_SET](testBaseState, testFollowUpDuration);
      expect(testBaseState.treatmentPlan.followUp.duration).toBe(testFollowUpDuration);
    });

    test('FOLLOW_UP_DURATION_UNIT_SET', () => {
      const testFollowUpDurationUnit = 'Days';
      mutations[FOLLOW_UP_DURATION_UNIT_SET](testBaseState, testFollowUpDurationUnit);
      expect(testBaseState.treatmentPlan.followUp.durationUnit).toBe(testFollowUpDurationUnit);
    });

    test('SCREEN_RESET', () => {
      mutations[SCREEN_RESET](testBaseState);
      expect(testBaseState.treatmentPlan.screen.duration).toBeFalsy();
      expect(testBaseState.treatmentPlan.screen.durationUnit).toBeFalsy();
    });

    test('RUN_IN_RESET', () => {
      mutations[RUN_IN_RESET](testBaseState);
      expect(testBaseState.treatmentPlan.runIn.duration).toBeFalsy();
      expect(testBaseState.treatmentPlan.runIn.durationUnit).toBeFalsy();
    });

    test('WASHOUT_RESET', () => {
      mutations[WASHOUT_RESET](testBaseState);
      expect(testBaseState.treatmentPlan.washout.duration).toBeFalsy();
      expect(testBaseState.treatmentPlan.washout.durationUnit).toBeFalsy();
    });

    test('FOLLOW_UP_RESET', () => {
      mutations[FOLLOW_UP_RESET](testBaseState);
      expect(testBaseState.treatmentPlan.followUp.duration).toBeFalsy();
      expect(testBaseState.treatmentPlan.followUp.durationUnit).toBeFalsy();
    });

    test('ADD_TREATMENT', () => {
      const initTreatmentLength = testBaseState.treatmentPlan.treatments.length;
      mutations[ADD_TREATMENT](testBaseState);
      expect(testBaseState.treatmentPlan.treatments).toHaveLength(initTreatmentLength + 1);
      mutations[ADD_TREATMENT](testBaseState);
      expect(testBaseState.treatmentPlan.treatments).toHaveLength(initTreatmentLength + 2);
      mutations[ADD_TREATMENT](testBaseState);
      expect(testBaseState.treatmentPlan.treatments).toHaveLength(initTreatmentLength + 3);
    });

    test('REMOVE_TREATMENT', () => {
      const initTreatmentLength = testBaseState.treatmentPlan.treatments.length;
      mutations[REMOVE_TREATMENT](testBaseState, 0);
      expect(testBaseState.treatmentPlan.treatments).toHaveLength(initTreatmentLength - 1);
      mutations[REMOVE_TREATMENT](testBaseState, 0);
      expect(testBaseState.treatmentPlan.treatments).toHaveLength(initTreatmentLength - 2);
    });

    test('INTERVENTION_TYPE_SET', () => {
      const testType = 'test type';
      const testIndex = 0;
      mutations[INTERVENTION_TYPE_SET](testBaseState, {
        index: testIndex,
        value: testType
      });
      expect(testBaseState.treatmentPlan.treatments[testIndex].interventionType).toBe(testType);
    });

    test('AGENT_SET', () => {
      const testAgent = 'some agent';
      const testIndex = 0;
      mutations[AGENT_SET](testBaseState, {
        index: testIndex,
        value: testAgent
      });
      expect(testBaseState.treatmentPlan.treatments[testIndex].agent).toBe(testAgent);
    });

    test('INTENSITY_SET', () => {
      const testIntensity = 5.3;
      const testIndex = 0;
      mutations[INTENSITY_SET](testBaseState, {
        index: testIndex,
        value: testIntensity
      });
      expect(testBaseState.treatmentPlan.treatments[testIndex].intensity).toBe(testIntensity);
    });

    test('INTENSITY_UNIT_SET', () => {
      const testIntensityUnit = 'Cd';
      const testIndex = 0;
      mutations[INTENSITY_UNIT_SET](testBaseState, {
        index: testIndex,
        value: testIntensityUnit
      });
      expect(testBaseState.treatmentPlan.treatments[testIndex].intensityUnit).toBe(testIntensityUnit);
    });

    test('DURATION_SET', () => {
      const testDuration = 5.3;
      const testIndex = 0;
      mutations[DURATION_SET](testBaseState, {
        index: testIndex,
        value: testDuration
      });
      expect(testBaseState.treatmentPlan.treatments[testIndex].duration).toBe(testDuration);
    });

    test('DURATION_UNIT_SET', () => {
      const testDurationUnit = 'days';
      const testIndex = 0;
      mutations[DURATION_UNIT_SET](testBaseState, {
        index: testIndex,
        value: testDurationUnit
      });
      expect(testBaseState.treatmentPlan.treatments[testIndex].durationUnit).toBe(testDurationUnit);
    });

    test('FACTORIAL_INTERVENTION_TYPE_SET', () => {
      const testType = 'test type';
      mutations[FACTORIAL_INTERVENTION_TYPE_SET](testBaseState, testType);
      expect(testBaseState.treatmentPlan.elementParams.interventionType).toBe(testType);
    });

    test('FACTORIAL_AGENTS_SET', () => {
      const testAgents = ['some agent', 'another agent'];
      mutations[FACTORIAL_AGENTS_SET](testBaseState, testAgents);
      expect(testBaseState.treatmentPlan.elementParams.agents).toBe(testAgents);
    });

    test('FACTORIAL_INTENSITES_SET', () => {
      const testIntensities = [2.3, 3.5, 5];
      mutations[FACTORIAL_INTENSITES_SET](testBaseState, testIntensities);
      expect(testBaseState.treatmentPlan.elementParams.intensities).toBe(testIntensities);
    });

    test('FACTORIAL_INTENSITY_UNIT_SET', () => {
      const testIntensityUnit = 'Cd';
      mutations[FACTORIAL_INTENSITY_UNIT_SET](testBaseState, testIntensityUnit);
      expect(testBaseState.treatmentPlan.elementParams.intensityUnit).toBe(testIntensityUnit);
    });

    test('FACTORIAL_DURATIONS_SET', () => {
      const testDurations = [20, 50];
      mutations[FACTORIAL_DURATIONS_SET](testBaseState, testDurations);
      expect(testBaseState.treatmentPlan.elementParams.durations).toBe(testDurations);
    });

    test('FACTORIAL_DURATION_UNIT_SET', () => {
      const testDurationUnit = 'Cd';
      mutations[FACTORIAL_DURATION_UNIT_SET](testBaseState, testDurationUnit);
      expect(testBaseState.treatmentPlan.elementParams.durationUnit).toBe(testDurationUnit);
    });

    test('REMOTE_REQUEST', () => {
      mutations[REMOTE_REQUEST](testBaseState);
      expect(testBaseState.isPending).toBe(true);
    });

    test('REMOTE_ERROR_SET', () => {
      const testErr = {
        message: 'this is a freaking error!'
      };
      mutations[REMOTE_ERROR_SET](testBaseState, testErr);
      expect(testBaseState.remoteError).toEqual(testErr);
    });

    test('REMOTE_ERROR_RESET', () => {
      mutations[REMOTE_ERROR_RESET](testBaseState);
      expect(testBaseState.remoteError).toBeFalsy();
    });

    test('GENERATED_STUDY_DESIGN_SET', () => {
      const testStudyDesign = {
        name: 'testStudyDesign',
        description: 'this is a study design',
        arms: [],
        type: {
          value: 'crossover'
        }
      };
      mutations[GENERATED_STUDY_DESIGN_SET](testBaseState, testStudyDesign);
      expect(testBaseState.name).toBe(testStudyDesign.name);
      expect(testBaseState.description).toBe(testStudyDesign.description);
      expect(testBaseState.designType).toBe(testStudyDesign.type);
      expect(testBaseState.subjectType).toBe(testStudyDesign.subjectType);
      // expect(testBaseState.subjectSize).toBe(testStudyDesign.subjectSize);
      expect(testBaseState.elements).toBe(testStudyDesign.elements);
      expect(testBaseState.arms.unselected).toEqual(testStudyDesign.arms);
      expect(testBaseState.arms.selected).toEqual([]);
    });

    test('SELECTED_ARMS_SET', () => {
      const options = [];
      const allArms = [ ...testPopulatedState.arms.selected, ...testPopulatedState.arms.unselected ];
      mutations[SELECTED_ARMS_SET](testPopulatedState, options);
      expect(testPopulatedState.arms.selected).toEqual([]);
      expect(testPopulatedState.arms.unselected).toEqual(allArms);
    });

    test('STUDY_DESIGN_SET', () => {
      const testStudyDesign = {
        name: 'testStudyDesign',
        description: 'this is a study design',
        arms: []
      };
      mutations[STUDY_DESIGN_SET](testBaseState, testStudyDesign);
      expect(testBaseState.studyDesign).toEqual(testStudyDesign);
    });

    test('ADD_SAMPLE_PLAN_ITEM and REMOVE_SAMPLE_PLAN_ITEM and SAMPLE_TYPE_SET', () => {
      // TODO fix this supplying a valid template
      const initSamplePlanLength = testBaseState.samplePlan.length;
      mutations[ADD_SAMPLE_PLAN_ITEM](testBaseState);
      expect(testBaseState.samplePlan).toHaveLength(initSamplePlanLength + 1);
      const sampleTypeTemplate = [];
      mutations[ADD_SAMPLE_PLAN_ITEM](testBaseState, sampleTypeTemplate);
      expect(testBaseState.samplePlan).toHaveLength(initSamplePlanLength + 2);
      mutations[REMOVE_SAMPLE_PLAN_ITEM](testBaseState, 1);
      expect(testBaseState.samplePlan).toHaveLength(initSamplePlanLength + 1);
      const testSampleType = 'scum';
      const testSampleIndex = 0;
      mutations[SAMPLE_TYPE_SET](testBaseState, {
        sampleType: testSampleType,
        samplePlanIndex: testSampleIndex
      });
      expect(testBaseState.samplePlan[testSampleIndex].sampleType).toBe(testSampleType);
    });

    test('SAMPLE_TYPE_SELECTED_CELL_TOGGLE and SAMPLE_TYPE_SIZE_SETSAMPLE_TYPE_SIZE_SET', () => {
      const testSamplePlanIndex = 1;
      const testArmName = 'Arm_5';
      const testEpochIndex = 5;
      const testValue = 8;
      expect(
        testPopulatedState.samplePlan[testSamplePlanIndex].selectedCells[testArmName][testEpochIndex]
      ).toBe(true);
      mutations[SAMPLE_TYPE_SIZE_SET](testPopulatedState, {
        samplePlanIndex: testSamplePlanIndex,
        armName: testArmName,
        epochIndex: testEpochIndex,
        value: testValue
      });
      expect(
        testPopulatedState.samplePlan[testSamplePlanIndex].sampleTypeSizes[testArmName][testEpochIndex]
      ).toBe(testValue);
      mutations[SAMPLE_TYPE_SELECTED_CELL_TOGGLE](testPopulatedState, {
        samplePlanIndex: testSamplePlanIndex,
        armName: testArmName,
        epochIndex: testEpochIndex,
        value: false
      });
      expect(
        testPopulatedState.samplePlan[testSamplePlanIndex].selectedCells[testArmName][testEpochIndex]
      ).toBe(false);
      expect(
        testPopulatedState.samplePlan[testSamplePlanIndex].sampleTypeSizes[testArmName][testEpochIndex]
      ).toBe(null);
      mutations[SAMPLE_TYPE_SELECTED_CELL_TOGGLE](testPopulatedState, {
        samplePlanIndex: testSamplePlanIndex,
        armName: testArmName,
        epochIndex: testEpochIndex,
        value: true
      });
      expect(
        testPopulatedState.samplePlan[testSamplePlanIndex].selectedCells[testArmName][testEpochIndex]
      ).toBe(true);
      expect(
        testPopulatedState.samplePlan[testSamplePlanIndex].sampleTypeSizes[testArmName][testEpochIndex]
      ).toBe(1);
    });

    test('SAMPLE_TYPE_SIZE_APPLY_TO_ARM', () => {
      const testSamplePlanIndex = 1;
      const testArmName = 'Arm_5';
      const testValue = 12;
      mutations[SAMPLE_TYPE_SIZE_APPLY_TO_ARM](testPopulatedState, {
        samplePlanIndex: testSamplePlanIndex,
        armName: testArmName,
        value: testValue
      });
      const targetArmSelected = testPopulatedState.samplePlan[testSamplePlanIndex].selectedCells[testArmName];
      for (const element of targetArmSelected) {
        expect(element).toBe(true);
      }
      const targetArmSizes = testPopulatedState.samplePlan[testSamplePlanIndex].sampleTypeSizes[testArmName];
      for (const element of targetArmSizes) {
        expect(element).toBe(testValue);
      }
    });

    test('SAMPLE_TYPE_SIZE_APPLY_TO_EPOCH,', () => {
      const testSamplePlanIndex = 1;
      const testEpochIndex = 0;
      const testValue = 6;
      mutations[SAMPLE_TYPE_SIZE_APPLY_TO_EPOCH](testPopulatedState, {
        samplePlanIndex: testSamplePlanIndex,
        epochIndex: testEpochIndex,
        value: testValue
      });
      // all checkboxes must be selected for the taget epoch
      for (const values of Object.values(testPopulatedState.samplePlan[testSamplePlanIndex].selectedCells)) {
        expect(values[testEpochIndex]).toBe(true);
      }
      // all sizes must be set to testValue for the taget epoch
      for (const values of Object.values(testPopulatedState.samplePlan[testSamplePlanIndex].sampleTypeSizes)) {
        expect(values[testEpochIndex]).toBe(testValue);
      }
    });

    test('ASSAY_CONFIGS_SET', () => {
      mutations[ASSAY_CONFIGS_SET](testBaseState, testAssayConfigs);
      expect(testBaseState.assayConfigs).toStrictEqual(testAssayConfigs);
    });

    test('ASSAY_TYPE_SELECT and ASSAY_TYPE_DESELECT', () => {
      mutations[ASSAY_CONFIGS_SET](testBaseState, testAssayConfigs);
      const targetIndex = 1;
      const targetName = testAssayConfigs[targetIndex].name;
      expect(testBaseState.assayPlan).toHaveLength(0);
      mutations[ASSAY_TYPE_SELECT](testBaseState, targetName);
      expect(testBaseState.assayPlan).toHaveLength(1);
      expect(testBaseState.assayPlan[0].name).toBe(targetName);
      mutations[ASSAY_TYPE_DESELECT](testBaseState, targetName);
      expect(testBaseState.assayPlan).toHaveLength(0);
    });

    test('NUM_REPLICATES_SET', () => {
      const testNumReplicates = 3;
      const assayIndex = 0;
      const nodeIndex = 2;
      mutations[NUM_REPLICATES_SET](testPopulatedState, {
        assayIndex,
        nodeIndex,
        value: testNumReplicates
      });
      expect(
        testPopulatedState.assayPlan[assayIndex].workflow[nodeIndex][1][NUM_REPLICATES].value
      ).toBe(testNumReplicates);
    });

    test('UPDATE_SELECTED_PARAMETER_VALUES', () => {
      const assayIndex = 0;
      const nodeIndex = 4; // mass spectrometry protocol node
      const paramName = 'instrument';
      const testParamValues = [
        { label: 'Agilent QTQF 6510', value: 'Agilent QTQF 6510' },
        { label: 'Agilent QTQF 6520A', value: 'Agilent QTQF 6520A' }
      ];
      mutations[UPDATE_SELECTED_PARAMETER_VALUES](testPopulatedState, {
        assayIndex,
        nodeIndex,
        paramName,
        values: testParamValues
      });
      expect(
        testPopulatedState.assayPlan[assayIndex].workflow[nodeIndex][1][paramName].values
      ).toStrictEqual(testParamValues.map(pV => pV.value));
    });

    test('CREATE_NEW_PARAMETER_VALUE', () => {
      const assayIndex = 0;
      const nodeIndex = 4; // mass spectrometry protocol node
      const paramName = 'instrument';
      const newValue = 'my brand new instrument';
      mutations[CREATE_NEW_PARAMETER_VALUE](testPopulatedState, {
        assayIndex,
        nodeIndex,
        paramName,
        newValue
      });
      expect(
        testPopulatedState.assayPlan[assayIndex].workflow[nodeIndex][1][paramName].values
      ).toContain(newValue);
    });

    test('UPDATE_SELECTED_CHARACTERISTIC_VALUES', () => {
      const assayIndex = 0;
      const nodeIndex = 1; // extraction protocol node
      const testCharacteristicValues = [
        { label: 'non-polar fraction', value: 'non-polar fraction' }
      ];
      mutations[UPDATE_SELECTED_CHARACTERISTIC_VALUES](testPopulatedState, {
        assayIndex,
        nodeIndex,
        values: testCharacteristicValues
      });
      expect(
        testPopulatedState.assayPlan[assayIndex].workflow[nodeIndex][1].characteristics_value.values
      ).toStrictEqual(testCharacteristicValues.map(chV => chV.value));
    });

    test('ASSAY_TYPE_SELECTED_CELL_TOGGLE and ASSAY_TYPE_SAMPLE_TYPES_SET', () => {
      const assayIndex = 0;
      const armName = 'Arm_0';
      const epochIndex = 1;
      const testIsSelected = true;
      const testSampleTypes = ['saliva', 'blood'];
      mutations[ASSAY_TYPE_SELECTED_CELL_TOGGLE](testPopulatedState, {
        assayIndex,
        armName,
        epochIndex,
        value: testIsSelected
      });
      expect(
        testPopulatedState.assayPlan[assayIndex].selectedCells[armName][epochIndex]
      ).toBe(testIsSelected);
      mutations[ASSAY_TYPE_SAMPLE_TYPES_SET](testPopulatedState, {
        assayIndex,
        armName,
        epochIndex,
        values: testSampleTypes
      });
      expect(
        testPopulatedState.assayPlan[assayIndex].selectedSampleTypes[armName][epochIndex]
      ).toStrictEqual(testSampleTypes);
    });

    test('ASSAY_IN_TAB_SET', () => {
      const assayName = 'some assay';
      mutations[ASSAY_IN_TAB_SET](testBaseState, assayName);
      expect(testBaseState.assayInTab).toBe(assayName);
    });

    test('RESET_ALL', () => {
      mutations[RESET_ALL](testPopulatedState);
      expect(testPopulatedState).toStrictEqual(testBaseState);
    });
  });

  describe('actions', () => {
    test('getStudy', async() => {
      const commit = jest.fn();
      const dummyError = { message: 'fail!' };
      const dummyResponse = { data: allStudies[1] };
      axios.get.mockResolvedValueOnce(dummyResponse);
      await actions.getStudy({ commit }, dummyResponse.data._id);
      expect(commit).toHaveBeenCalledWith(WHOLE_STUDY_DESIGN_PLANNER_SET, {
        _id: dummyResponse.data._id,
        name: dummyResponse.data.name,
        description: dummyResponse.data.description,
        design: dummyResponse.data.design
      });
      axios.get.mockRejectedValueOnce(dummyError);
      await actions.getStudy({ commit }, dummyResponse.data._id);
      expect(commit).toHaveBeenCalledWith(RESET_ALL);
      expect(commit).toHaveBeenLastCalledWith(REMOTE_ERROR_SET, dummyError);
    });
    test('generateSubjectGroups', async() => {
      process.env.USE_REMOTE = true;
      const commit = jest.fn();
      // const testSubjectType = 'Homo homo';
      // const testObservationaFactors = [];
      const dummyResponse = { message: 'correct', data: {} };
      const dummyError = { message: 'failure!' };
      axios.post.mockResolvedValueOnce(dummyResponse);
      await actions.generateSubjectGroups({ state: testBaseState, commit });
      expect(commit).toHaveBeenCalledWith(GENERATED_SUBJECT_GROUPS, dummyResponse.data);
      axios.post.mockRejectedValueOnce(dummyError);
      await actions.generateSubjectGroups({ state: testBaseState, commit });
      expect(commit).toHaveBeenCalledWith(REMOTE_ERROR_SET, dummyError);
      process.env.USE_REMOTE = false;
    });
    test('generateArms', async() => {
      process.env.USE_REMOTE = true;
      const commit = jest.fn();
      const dummyDesign = {};
      const dummyResponse = { message: 'yeah!', data: {} };
      const dummyError = { message: 'no!' };
      axios.post.mockResolvedValueOnce(dummyResponse);
      await actions.generateArms({ state: testBaseState, commit }, dummyDesign);
      expect(commit).toHaveBeenCalledWith(GENERATED_STUDY_DESIGN_SET, dummyResponse.data);
      axios.post.mockRejectedValueOnce(dummyError);
      await actions.generateArms({ state: testBaseState, commit }, dummyDesign);
      expect(commit).toHaveBeenCalledWith(REMOTE_ERROR_SET, dummyError);
      process.env.USE_REMOTE = false;
    });
    test('addSamplePlanItem', () => {
      const commit = jest.fn();
      const testGetters = {
        armNames: getters.armNames(testPopulatedState),
        designGridHeaders: getters.designGridHeaders(testPopulatedState)
      };
      actions.addSamplePlanItem({ commit, getters: testGetters });
      expect(commit).toHaveBeenCalledWith(ADD_SAMPLE_PLAN_ITEM, expect.anything());
    });
    test('removeSamplePlanItem', () => {}); // TODO
    test('retrieveAssayConfigs', async() => {
      const commit = jest.fn();
      const testGetters = {
        armNames: getters.armNames(testPopulatedState),
        designGridHeaders: getters.designGridHeaders(testPopulatedState)
      };
      const dummyResponse = { message: 'yeah!', data: [{}] };
      const dummyError = { message: 'no!' };
      axios.get.mockResolvedValueOnce(dummyResponse);
      await actions.retrieveAssayConfigs({ commit, getters: testGetters });
      expect(commit).toHaveBeenCalledWith(ASSAY_CONFIGS_SET, expect.anything());
      axios.get.mockRejectedValueOnce(dummyError);
      await actions.retrieveAssayConfigs({ commit, getters: testGetters });
      expect(commit).toHaveBeenCalledWith(REMOTE_ERROR_SET, dummyError);
    });
    test('updateSampleTypesInAssayPlanItem', () => {
      const testAssayIndex = 1;
      const testArmName = 'Arm_7';
      const testEpochIndex = 5;
      const testValues = [];
      const commit = jest.fn();
      const testGetters = {
        sampleTypeTable: getters.sampleTypeTable(testPopulatedState)
      };
      const testPayload = {
        assayIndex: testAssayIndex,
        armName: testArmName,
        epochIndex: testEpochIndex,
        values: testValues
      };
      actions.updateSampleTypesInAssayPlanItem({ commit, getters: testGetters }, testPayload);
      expect(commit).toHaveBeenCalledWith(ASSAY_TYPE_SAMPLE_TYPES_SET, testPayload);
    });
  });
});
