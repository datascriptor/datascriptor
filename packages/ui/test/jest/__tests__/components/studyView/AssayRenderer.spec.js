import AssayRenderer from 'src/components/studyView/AssayRenderer';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../utils';
import { Quasar } from 'quasar';
import testCrossoverStudyDesign from '../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms.json';
import testFactorialStudyDesign from '../../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';
import { mermaidAPI } from 'mermaid';

describe('AssayRenderer', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components });
  localVue.use(Vuex);

  const assayPlan = testCrossoverStudyDesign.assayPlan.concat(testFactorialStudyDesign.assayPlan);

  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(AssayRenderer, {
      localVue,
      propsData: {
        assayPlan
      }
    });
  });

  describe('computed', () => {
    test('graph', () => {
      expect(wrapper.vm.graph).toMatch(/graph LR/);
      const subgraphMatches = wrapper.vm.graph.match(/subgraph/g);
      expect(subgraphMatches).toHaveLength(assayPlan.length);
    });
  });

  describe('methods', () => {
    const ASSAY_IX = 0;
    const ASSAY_ONTOLOGY_ANN_IX = testCrossoverStudyDesign.assayPlan.length;
    const PRODUCT_NODE_IX = 0;
    const PROTOCOL_NODE_IX = 0;
    const extractProductNode = assayPlan[ASSAY_IX].workflow[PRODUCT_NODE_IX];
    const extractionProtocolNode = assayPlan[ASSAY_IX].workflow[PROTOCOL_NODE_IX];
    test('productNode', () => {
      const [name, params] = extractProductNode;
      const res = wrapper.vm.productNode(name, params, ASSAY_IX, PRODUCT_NODE_IX);
      expect(typeof res).toBe('string');
      expect(res).toMatch(/assay/);
      expect(res).toMatch(/Extract/);
    });
    test('protocolNode', () => {
      const [name, params] = extractionProtocolNode;
      const res = wrapper.vm.productNode(name, params, ASSAY_IX, PRODUCT_NODE_IX);
      expect(typeof res).toBe('string');
      expect(res).toMatch(/assay/);
      expect(res).toMatch(/Extraction/);
    });
    test('productNode (ontology annotations)', () => {
      const [name, params] = extractProductNode;
      const res = wrapper.vm.productNode(name, params, ASSAY_ONTOLOGY_ANN_IX, PRODUCT_NODE_IX);
      expect(typeof res).toBe('string');
      expect(res).toMatch(/assay/);
      expect(res).toMatch(/Extract/);
    });
    test('protocolNode (ontology annotations)', () => {
      const [name, params] = extractionProtocolNode;
      const res = wrapper.vm.productNode(name, params, ASSAY_ONTOLOGY_ANN_IX, PRODUCT_NODE_IX);
      expect(typeof res).toBe('string');
      expect(res).toMatch(/assay/);
      expect(res).toMatch(/Extraction/);
    });
  });
});
