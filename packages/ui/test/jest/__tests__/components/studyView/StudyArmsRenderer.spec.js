import StudyArmsRenderer from 'src/components/studyView/StudyArmsRenderer';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../utils';
import { Quasar } from 'quasar';
import testStudyDesign from '../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms.json';
import { mermaidAPI } from 'mermaid';

// jest.mock('mermaid');

describe('StudyArmsRenderer', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components });
  localVue.use(Vuex);

  /*
    TODO move to the MermaidRenderer mixin
    describe('data()', () => {
        test('the data() is initialised correctly', () => {
            expect(typeof StudyArmsRenderer.data).toBe('function');
            const defaultData = StudyArmsRenderer.data();
            expect(defaultData.elementPlanSvg).toBeNull();
            expect(defaultData).toHaveProperty('config');
        });
    }); */

  describe('computed', () => {
    describe('elementPlanGraph', () => {
      test('returns the Mermaid graph the treatment and non-treatment elements', () => {
        const wrapper = shallowMount(StudyArmsRenderer, {
          localVue,
          propsData: {
            arms: testStudyDesign.arms.selected,
            elements: testStudyDesign.elements
          }
        });
        expect(mermaidAPI.render).toHaveBeenCalled();
        expect(wrapper.vm.graph).toMatch('graph LR');
        for (const arm of testStudyDesign.arms.selected) {
          if (arm.elements) {
            for (const element of testStudyDesign.elements) {
              expect(wrapper.vm.graph).toMatch(element.name);
            }
          }
        }
      });
    });
  });

  /*
    TODO move to the MermaidRenderer mixin
    describe('mounted()', () => {
        test('it mounts correctly', () => {
            const wrapper = mount(StudyArmsRenderer, {
                localVue,
                propsData: {
                    studyDesign: testStudy.design
                }
            });
            expect(wrapper.findAllComponents(components.QCard)).toHaveLength(1);
        });
    }); */
});
