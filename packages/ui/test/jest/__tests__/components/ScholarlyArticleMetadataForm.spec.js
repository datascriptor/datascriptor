import ScholarlyArticleMetadataForm from 'src/components/ScholarlyArticleMetadataForm';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { Quasar } from 'quasar';
import { components } from '../utils';
import { constants } from '@datascriptor/core';
import publicationStore from 'src/store/publication';

describe('ScholarlyArticleMetadataForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let state;
    let store;

    beforeEach(() => {
        state = {
            ...publicationStore.state
        };
        actions = {
            setScholarlyArticle: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    actions,
                    getters: publicationStore.getters
                }
            }
        });
    });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof ScholarlyArticleMetadataForm.data).toBe('function');
            const defaultData = ScholarlyArticleMetadataForm.data();
            const testRow = { name: 'some name', value: 'some value' };
            expect(defaultData.form).toStrictEqual(constants.DEFAULT_SCHOLARLY_ARTICLE_METADATA);
            expect(defaultData.columns[0].field(testRow)).toBe(testRow.name);
            expect(defaultData.columns[0].format(testRow.value)).toBe(testRow.value);
        });
    });

    describe('methods', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(ScholarlyArticleMetadataForm, {
                store,
                localVue
            });
        });

        describe('onSubmit()', () => {
            test('Generates a scholarly article and dispatches it', () => {});
            test('Pushes the router', () => {});
        });

        describe('onReset()', () => {
            test('resets the form', () => {
                // TODO improve this test
                wrapper.vm.onReset();
                expect(wrapper.vm.form).toStrictEqual(constants.DEFAULT_SCHOLARLY_ARTICLE_METADATA);
            });
        });
    });
});
