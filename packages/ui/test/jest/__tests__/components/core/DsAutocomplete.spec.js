import DsAutocomplete from 'src/components/core/DsAutocomplete';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import { components } from '../../utils';
import { Quasar } from 'quasar';

describe('DsAutocomplete', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });

    describe('data', () => {
        test('data is initialised correctly', () => {
            expect(typeof DsAutocomplete.data).toBe('function');
            const defaultData = DsAutocomplete.data();
            expect(defaultData).toStrictEqual({
                options: []
            });
        });
    });

    describe('methods', () => {
        let wrapper;
        let fetchOptions;
        let updateValue;

        const testOpts = [
            { label: 'random', iri: 'https://something.dunno/random', obo_id: null }
        ];

        beforeEach(() => {
            fetchOptions = jest.fn(() => testOpts);
            updateValue = jest.fn();
            wrapper = mount(DsAutocomplete, {
                localVue,
                propsData: {
                    label: 'test',
                    value: '',
                    fetchOptionsFn: fetchOptions,
                    updateValueFn: updateValue
                }
            });
        });

        describe('filterFn()', () => {
            test('it calls the remote method to fetch the values', async() => {
                const qSelect = wrapper.findComponent(components.QSelect);
                await qSelect.vm.$emit('filter', 'rand', jest.fn(fn => fn()));
                expect(fetchOptions).toHaveBeenCalledTimes(1);
            });
        });

        describe('setValue()', () => {
            test('calls the this.updateValueFn() function', () => {
                const value = 'rand';
                const qSelect = wrapper.findComponent(components.QSelect);
                qSelect.vm.$emit('input-value', value);
                expect(updateValue).toHaveBeenCalledWith(value);
            });
        });
    });
});
