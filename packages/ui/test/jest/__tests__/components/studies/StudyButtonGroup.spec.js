import StudyButtonGroup from 'src/components/studies/StudyButtonGroup';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { components } from '../../utils';
import { Quasar } from 'quasar';
import axios from 'axios';
import testStudies from '../../../../fixtures/all-studies.json';

jest.mock('axios');

describe('StudyButtonGroup', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, {
    components
  });

  const $router = {
    push: jest.fn()
  };

  const createComponent = () => {
    return shallowMount(StudyButtonGroup, {
      localVue,
      propsData: {
        studyId: testStudies[0]._id
      },
      mocks: {
        $router
      }
    });
  };

  describe('methods', () => {
    test('viewStudy()', () => {
      axios.post.mockResolvedValueOnce({
        data: testStudies
      });
      const wrapper = createComponent();
      const studyId = testStudies[0]._id;
      wrapper.vm.viewStudy(studyId);
      expect($router.push).toHaveBeenCalledTimes(1);
      expect($router.push).toHaveBeenCalledWith(`/study/${studyId}`);
    });

    test('editStudy()', () => {});
    test('cloneStudy()', () => {});
  });
});
