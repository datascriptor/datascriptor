import StudiesView, { defaultData } from 'src/components/studies/StudiesView';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { components } from '../../utils';
import { Quasar } from 'quasar';
import axios from 'axios';
import testStudies from '../../../../fixtures/all-studies.json';

jest.mock('axios');

describe('StudiesView', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, {
    components
  });

  const $router = {
    push: jest.fn()
  };

  const createComponent = () => {
    return shallowMount(StudiesView, {
      localVue,
      propsData: {
        grid: false
      },
      mocks: {
        $router
      }
    });
  };

  describe('data', () => {
    test('data is initialised correctly', () => {
      expect(typeof StudiesView.data).toBe('function');
      expect(StudiesView.data()).toStrictEqual(defaultData);
    });
  });

  describe('mounted()', () => {
    test('studies are set correctly', () => {
      axios.post.mockResolvedValueOnce({
        data: testStudies
      });
      const wrapper = createComponent();
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.studies).toStrictEqual(testStudies);
        expect(wrapper.vm.error).toBeNull();
      });
    });

    test('errors are handled correctly', () => {
      const dummyError = { message: 'fail!' };
      axios.get.mockRejectedValueOnce(dummyError);
      const wrapper = createComponent();
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.error).toStrictEqual(dummyError);
        expect(wrapper.vm.studies).toStrictEqual([]);
      });
    });
  });

  describe('computed', () => {
    test('formattedStudies', () => {
      axios.post.mockResolvedValueOnce({
        data: []
      });
      const wrapper = createComponent();
      wrapper.setData({
        studies: testStudies,
        error: null
      });
      expect(wrapper.vm.formattedStudies).toHaveLength(testStudies.length);
      for (const [ix, formattedStudy] of wrapper.vm.formattedStudies.entries()) {
        expect(formattedStudy.name).toBe(testStudies[ix].name);
      }
    });
  });
});
