import TextEditor from 'src/components/TextEditor';

describe('TextEditor', () => {
    describe('data()', () => {
        test('data is initialised correctly', () => {
            expect(typeof TextEditor.data).toBe('function');
            const defaultData = TextEditor.data();
            expect(defaultData).toStrictEqual({});
        });
    });
});
