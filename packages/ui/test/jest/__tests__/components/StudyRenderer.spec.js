import StudyRenderer from 'src/components/StudyRenderer';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { components } from '../utils';
import { Quasar } from 'quasar';
import Vuex from 'vuex';

import testStudyReport from '../../../fixtures/test-study-report';
import testStudy from '../../../fixtures/test-study';

describe('StudyRenderer', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components });
  localVue.use(Vuex);

  let emptyStore, populatedStore;

  beforeEach(() => {
    emptyStore = new Vuex.Store({
      getters: {
        // TODO investigate if this is the correct way of setting getters in the store.
        'study/studyData': () => { return {}; },
        'study/studyReport': () => { return {}; }
      }
    });
    populatedStore = new Vuex.Store({
      getters: {
        // TODO investigate if this is the correct way of setting getters in the store.
        'study/studyData': () => testStudy,
        'study/studyReport': () => testStudyReport
      }
    });
  });

  describe('computed', () => {
    describe('studyDesign, hasStudyDesign, reportText', () => {
      test('populated store ', () => {
        const wrapper = shallowMount(StudyRenderer, {
          localVue,
          store: populatedStore
        });
        expect(wrapper.vm.studyDesign).toStrictEqual(testStudy.design);
        expect(wrapper.vm.hasStudyDesign).toEqual(true);
        expect(wrapper.vm.reportText).not.toBeNull();
      });

      test('empty store ', () => {
        const wrapper = shallowMount(StudyRenderer, {
          localVue,
          store: emptyStore
        });
        expect(wrapper.vm.studyDesign).toStrictEqual({});
        expect(wrapper.vm.hasStudyDesign).toEqual(false);
        expect(wrapper.vm.reportText).toBeNull();
      });
    });
  });
});
