import SamplePlanForm from 'src/components/studyDesignEditor/steps/SamplePlanForm';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { components } from '../../../utils';
import { Quasar } from 'quasar';

describe('SamplePlanForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });

    describe('data', () => {
        test('data is initialised correctly', () => {
            expect(typeof SamplePlanForm.data).toBe('function');
            const defaultData = SamplePlanForm.data();
            expect(defaultData).toStrictEqual({});
        });
    });

    describe('computed', () => {});

    describe('methods', () => {});
});
