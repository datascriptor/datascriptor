import AssayPlanForm from 'src/components/studyDesignEditor/steps/AssayPlanForm';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms';
import testAssayConfigs from '@datascriptor/api/test/fixtures/assay-configs';
import { ASSAY_IN_TAB_SET, ASSAY_TYPE_SELECT, ASSAY_TYPE_DESELECT } from 'src/store/mutation-types';

describe('AssayPlanForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let mutations;
    let state;
    let store;
    const assayIndex = 0;
    const assayName = testStudyDesignPlanner.assayPlan[assayIndex].name;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner,
            assayConfigs: testAssayConfigs
        };
        actions = {
            retrieveAssayConfigs: jest.fn()
        };
        mutations = {
            [ASSAY_IN_TAB_SET]: jest.fn(),
            [ASSAY_TYPE_DESELECT]: jest.fn(),
            [ASSAY_TYPE_SELECT]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data', () => {
        test('data is initialised correctly', () => {
            expect(typeof AssayPlanForm.data).toBe('function');
            const defaultData = AssayPlanForm.data();
            expect(defaultData).toStrictEqual({
                splitterModel: 30,
                tab: 'assayName',
                selected: []
            });
        });
    });

    describe('computed', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(AssayPlanForm, {
                store,
                localVue,
                propsData: {
                    name: assayName
                }
            });
        });

        describe('assayConfigs', () => {
            test('correctly returns the list of assay configs', () => {
                expect(wrapper.vm.assayConfigs).toStrictEqual(testAssayConfigs);
            });
        });

        describe('selectedAssayTypes', () => {
            test('correctly returns the list of selected assay types', () => {
                expect(wrapper.vm.selectedAssayTypes).toStrictEqual(studyDesignPlannerStore.getters.selectedAssayTypes(state));
            });
        });

        describe('selectedAssayNames', () => {
            test('correctly returns the list of selected assay types', () => {
                expect(wrapper.vm.selectedAssayNames).toStrictEqual(
                    testStudyDesignPlanner.assayPlan.map(aT => aT.name)
                );
            });
        });

        describe('hasSelectedAssayTypes', () => {
            test('should return true', () => {
                expect(wrapper.vm.hasSelectedAssayTypes).toBe(true);
            });
        });

        describe('assayInTab', () => {
            test('get()', () => {
                expect(wrapper.vm.assayInTab).toBe(testStudyDesignPlanner.assayInTab);
            });

            test('set()', () => {
                const testAssayName = wrapper.vm.selectedAssayNames[0];
                wrapper.vm.assayInTab = testAssayName;
                expect(mutations[ASSAY_IN_TAB_SET]).toHaveBeenCalled();
            });
        });
    });

    describe('methods', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = mount(AssayPlanForm, {
                store,
                localVue,
                propsData: {
                    name: assayName
                }
            });
        });

        describe('toggleAssayType', () => {
            test('triggers a commit action to select the appropriate assay type', () => {
                // we use an assay that is not yet in the assay plan
                const testAssayTypeName = testAssayConfigs[1].name;
                wrapper.vm.toggleAssayType(testAssayTypeName);
                expect(mutations[ASSAY_TYPE_SELECT]).toHaveBeenCalledWith(
                    state, testAssayTypeName
                );
            });
            test('triggers a commit action to deselect the appropriate assay type', () => {
                // we use an assay that is already in the assay plan
                const testAssayTypeName = wrapper.vm.selectedAssayNames[0];
                wrapper.vm.toggleAssayType(testAssayTypeName);
                expect(mutations[ASSAY_TYPE_DESELECT]).toHaveBeenCalledWith(
                    state, testAssayTypeName
                );
            });
        });
    });

    describe('mounted()', () => {
        test('the action "retrieveAssayConfigs" must have been dispatched', () => {
            mount(AssayPlanForm, {
                store,
                localVue,
                propsData: {
                    name: assayName
                }
            });
            expect(actions.retrieveAssayConfigs).toHaveBeenCalledTimes(1);
        });
    });
});
