import TreatmentPlanForm from 'src/components/studyDesignEditor/steps/TreatmentPlanForm';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
// import { constants } from '@datascriptor/core';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';

describe('TreatmentPlanForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let state;
    let store;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        actions = {
            generateArms: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data()', () => {
        test('data is initialised correctly', () => {
            expect(typeof TreatmentPlanForm.data).toBe('function');
            const defaultData = TreatmentPlanForm.data();
            expect(defaultData).toStrictEqual({});
        });
    });

    describe('methods', () => {
        describe('onSubmit()', () => {
            test('it submits the treatment form', () => {
                const wrapper = shallowMount(TreatmentPlanForm, {
                    localVue,
                    store
                });
                wrapper.vm.onSubmit();
                expect(actions.generateArms).toHaveBeenCalledTimes(1);
            });
        });
    });
});
