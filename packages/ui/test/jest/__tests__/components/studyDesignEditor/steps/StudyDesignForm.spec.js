import StudyDesignForm from 'src/components/studyDesignEditor/steps/StudyDesignForm';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import Vuex from 'vuex';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';
import {
    NAME_SET, DESCRIPTION_SET, DESIGN_TYPE_SET,
    SUBJECT_TYPE_SET, SUBJECT_SIZE_SET,
    OBSERVATIONAL_FACTOR_ADD
} from 'src/store/mutation-types';

describe('StudyDesignForm', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let mutations;
    let actions;
    let state;
    let store;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        actions = {
            generateSubjectGroups: jest.fn(),
            addSamplePlanItem: jest.fn(),
            removeSamplePlanItem: jest.fn(),
            retrieveAssayConfigs: jest.fn(),
            addAssayPlanItem: jest.fn()
        };
        mutations = {
            [NAME_SET]: jest.fn(),
            [DESCRIPTION_SET]: jest.fn(),
            [SUBJECT_TYPE_SET]: jest.fn(),
            [SUBJECT_SIZE_SET]: jest.fn(),
            [OBSERVATIONAL_FACTOR_ADD]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof StudyDesignForm.data).toBe('function');
            const defaultData = StudyDesignForm.data();
            expect(defaultData).toHaveProperty('studyDesignTypes');
            expect(defaultData.studyDesignTypes).toHaveLength(3);
        });
    });

    describe('computed', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(StudyDesignForm, {
                store,
                localVue
            });
        });

        describe('name', () => {
            test('get()', () => {
                expect(wrapper.vm.name).toBe(testStudyDesignPlanner.name);
            });
            test('set()', () => {});
        });
        describe('description', () => {
            test('get()', () => {
                expect(wrapper.vm.description).toBe(testStudyDesignPlanner.description);
            });
            test('set()', () => {});
        });
        describe('subjectType', () => {
            test('get()', () => {
                expect(wrapper.vm.subjectType).toBe(testStudyDesignPlanner.subjectType);
            });
            test('set()', () => {});
        });
        describe('subjectSize', () => {
            test('get()', () => {
                expect(wrapper.vm.subjectSize).toBe(testStudyDesignPlanner.subjectSize);
            });
            test('set()', () => {});
        });
        describe('designType', () => {
            test('get()', () => {
                expect(wrapper.vm.designType).toBe(testStudyDesignPlanner.designType);
            });
            test('set()', () => {});
        });
    });

    describe('methods()', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = mount(StudyDesignForm, {
                store,
                localVue
            });
        });

        describe('onSubmit()', () => {
            test('it triggers the appropriate action', async() => {
                await wrapper.vm.onSubmit();
                expect(actions.generateSubjectGroups).toHaveBeenCalledTimes(1);
            });
        });
        describe('onReset()', () => {});
    });
});
