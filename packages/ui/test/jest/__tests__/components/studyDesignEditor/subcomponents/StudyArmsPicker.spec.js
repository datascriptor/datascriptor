import StudyArmsPicker from 'src/components/studyDesignEditor/subcomponents/StudyArmsPicker';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { Quasar } from 'quasar';
import { components } from '../../../utils';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms';

describe('StudyArmsPicker', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let state, actions, store, wrapper;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        actions = {
            generateStudyDesign: jest.fn(),
            addSamplePlanItem: jest.fn(),
            removeSamplePlanItem: jest.fn(),
            retrieveAssayConfigs: jest.fn(),
            addAssayPlanItem: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
        wrapper = shallowMount(StudyArmsPicker, {
            localVue,
            store,
            propsData: {
                index: 0
            }
        });
    });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof StudyArmsPicker.data).toBe('function');
            const defaultData = StudyArmsPicker.data();
            expect(defaultData).toHaveProperty('pagination');
            expect(defaultData.pagination).toStrictEqual({
                sortBy: 'asc',
                descending: false,
                page: 1,
                rowsPerPage: 10
            });
        });
    });

    describe('computed', () => {
        describe('elements', () => {
            test('it computes the study Design object', () => {
                expect(wrapper.vm.elements).toStrictEqual(testStudyDesignPlanner.elements);
            });
        });

        describe('tableColums', () => {
            test('it computes the columns of the arm table', () => {
                expect(Array.isArray(wrapper.vm.tableColumns)).toBe(true);
                expect(wrapper.vm.tableColumns.length).toBeGreaterThan(2);
            });
        });

        describe('tableData', () => {
            test('it computes the expanded JSON of the current Study Design object', () => {
                expect(Array.isArray(wrapper.vm.tableData)).toBe(true);
                expect(wrapper.vm.tableData.length).toBeGreaterThan(0);
            });
        });
    });

    /*
    describe('methods', () => {
        describe('onSubmit()', () => {
            test('it triggers the storeConvertedStudy action', () => {
                wrapper.vm.onSubmit();
                expect(actions['study/storeConvertedStudy']).toHaveBeenCalled();
            });
        });

        describe('onReset()', () => {
            test('it deselects all the arms in the `selectedArms` data field', () => {
                wrapper.vm.onReset();
                expect(wrapper.vm.$data.selectedArms).toStrictEqual(
                    Array(testStudy.design.arms.length).fill(false)
                );
            });
        });
    });
    */
});
