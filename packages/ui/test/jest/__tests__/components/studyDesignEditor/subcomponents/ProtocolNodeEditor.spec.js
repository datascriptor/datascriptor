import ProtocolNodeEditor from 'src/components/studyDesignEditor/subcomponents/ProtocolNodeEditor';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { Quasar } from 'quasar';
import { components } from '../../../utils';
import { NUM_REPLICATES } from 'src/utils/constants';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testAssayConfigs from '@datascriptor/api/test/fixtures/assay-configs';
import testStudyDesignPlanner from '../../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms';
import { cloneDeep } from 'lodash';

describe('ProtocolNodeEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let state;
    let store;
    const assayIndex = 0;
    const nodeIndex = 4;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner,
            assayConfigs: testAssayConfigs
        };
        actions = {
            generateStudyDesign: jest.fn(),
            addSamplePlanItem: jest.fn(),
            removeSamplePlanItem: jest.fn(),
            retrieveAssayConfigs: jest.fn(),
            addAssayPlanItem: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof ProtocolNodeEditor.data).toBe('function');
            const defaultData = ProtocolNodeEditor.data();
            expect(defaultData).toStrictEqual({});
        });
    });

    describe('computed', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(ProtocolNodeEditor, {
                store,
                localVue,
                propsData: {
                    assayIndex,
                    nodeIndex
                }
            });
        });

        describe('node', () => {
            test('correctly returns the current node', () => {
                expect(wrapper.vm.node).toStrictEqual(state.assayPlan[assayIndex].workflow[nodeIndex]);
            });
        });

        describe('nodeName', () => {
            test('correctly returns the current nodeName', () => {
                expect(wrapper.vm.nodeName).toBe(state.assayPlan[assayIndex].workflow[nodeIndex][0]);
            });
        });

        describe('nodeSpecs', () => {
            test('correctly returns the current node specs', () => {
                expect(wrapper.vm.nodeSpecs).toStrictEqual(state.assayPlan[assayIndex].workflow[nodeIndex][1]);
            });
        });

        describe('replicates', () => {
            test('correctly returns the number of replicates for the current node', () => {
                expect(wrapper.vm.replicates).toStrictEqual(state.assayPlan[assayIndex].workflow[nodeIndex][1][NUM_REPLICATES].value);
            });
        });

        describe('selectedProtocolParameterValues', () => {
            test('correctly initialises empty arrays ', () => {
                for (const [name, values] of Object.entries(wrapper.vm.selectedProtocolParameterValues)) {
                    expect(name).toBeTruthy();
                    expect(values).toBeInstanceOf(Array);
                    expect(values.length).toBeGreaterThanOrEqual(0);
                }
            });
            test('', () => {
                const newState = cloneDeep(state);
                const SELECTED_PARAM_NAME = 'instrument';
                newState.assayPlan[assayIndex].workflow[nodeIndex][1][SELECTED_PARAM_NAME].values = [
                    newState.assayPlan[assayIndex].workflow[nodeIndex][1][SELECTED_PARAM_NAME].options[0]
                ];
                store = new Vuex.Store({
                    modules: {
                        studyDesignPlanner: {
                            state: newState,
                            actions,
                            getters: studyDesignPlannerStore.getters
                        }
                    }
                });
                wrapper = shallowMount(ProtocolNodeEditor, {
                    store,
                    localVue,
                    propsData: {
                        assayIndex,
                        nodeIndex
                    }
                });
                for (const [name, values] of Object.entries(wrapper.vm.selectedProtocolParameterValues)) {
                    expect(name).toBeTruthy();
                    expect(values).toBeInstanceOf(Array);
                    if (name === SELECTED_PARAM_NAME) {
                        expect(values.length).toBeGreaterThanOrEqual(1);
                    } else {
                        expect(values.length).toBeGreaterThanOrEqual(0);
                    }
                }
            });
        });

        describe('protocolParamNames', () => {
            test('correctly returns the number of replicates for the current node', () => {
                expect(wrapper.vm.protocolParamNames).toHaveLength(3);
            });
        });

        describe('protocolParamOptions', () => {
            test('correctly returns the number of replicates for the current node', () => {
                // console.log(JSON.stringify(wrapper.vm.protocolParamOptions));
                for (const [name, options] of Object.entries(wrapper.vm.protocolParamOptions)) {
                    expect(wrapper.vm.protocolParamNames).toContain(name);
                    expect(options).toBeInstanceOf(Array);
                    options.map(option => {
                        expect(option).toHaveProperty('label');
                        expect(option).toHaveProperty('value');
                    });
                }
            });
        });
    });

    describe('methods', () => {
        describe('displayAnnotation', () => {});
        describe('updateSelectedParameterValues', () => {});
    });
});
