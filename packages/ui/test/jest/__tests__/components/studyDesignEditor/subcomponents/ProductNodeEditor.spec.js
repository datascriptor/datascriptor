import ProductNodeEditor from 'src/components/studyDesignEditor/subcomponents/ProtocolNodeEditor';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { Quasar } from 'quasar';
import { components } from '../../../utils';

describe('ProductNodeEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    describe('data()', () => {
        test('sets the correct default data', () => {
            expect(typeof ProductNodeEditor.data).toBe('function');
            const defaultData = ProductNodeEditor.data();
            expect(defaultData).toStrictEqual({});
        });
    });

    describe('computed', () => {
        describe('nodeSpecs', () => {});
        describe('characteristicOptions', () => {});
        describe('extension', () => {});
    });

    describe('methods', () => {
        describe('displayAnnotation', () => {});
    });
});
