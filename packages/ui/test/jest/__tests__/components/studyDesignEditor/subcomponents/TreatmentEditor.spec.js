import TreatmentEditor from 'src/components/studyDesignEditor/subcomponents/TreatmentEditor';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import { constants } from '@datascriptor/core';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import {
    REMOVE_TREATMENT
} from 'src/store/mutation-types';

describe('TreatmentEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let state;
    let store;
    let mutations;

    beforeEach(() => {
        state = {
            ...studyDesignPlannerStore.state
        };
        actions = {
            generateStudyDesign: jest.fn(),
            addSamplePlanItem: jest.fn(),
            removeSamplePlanItem: jest.fn(),
            retrieveAssayConfigs: jest.fn(),
            addAssayPlanItem: jest.fn()
        };
        mutations = {
            [REMOVE_TREATMENT]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data()', () => {
        test('data is initialised correctly', () => {
            expect(typeof TreatmentEditor.data).toBe('function');
            const defaultData = TreatmentEditor.data();
            expect(defaultData).toHaveProperty('interventionTypes');
            expect(defaultData.interventionTypes).toHaveLength(constants.INTERVENTION_TYPES.length);
        });
    });

    describe('methods', () => {
        describe('remove', () => {
            test('it calls $destroy, removes the component from the parent and emits a "deleted" signal', async() => {
                const index = 0;
                const wrapper = shallowMount(TreatmentEditor, {
                    localVue,
                    store,
                    propsData: {
                        index
                    }
                });
                wrapper.vm.remove();
                expect(mutations[REMOVE_TREATMENT]).toHaveBeenCalledTimes(1);
                expect(mutations[REMOVE_TREATMENT]).toHaveBeenCalledWith(state, index);
            });
        });
    });
});
