import FactorialDesign from 'src/components/studyDesignEditor/subcomponents/FactorialDesign';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';
import { constants } from '@datascriptor/core';
import {
    FACTORIAL_AGENTS_SET, FACTORIAL_INTENSITES_SET, FACTORIAL_INTENSITY_UNIT_SET,
    FACTORIAL_DURATIONS_SET, FACTORIAL_DURATION_UNIT_SET
} from 'src/store/mutation-types';

describe('FactorialDesign', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let mutations;
    let state;
    let store;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        mutations = {
            [FACTORIAL_AGENTS_SET]: jest.fn(),
            [FACTORIAL_INTENSITES_SET]: jest.fn(),
            [FACTORIAL_INTENSITY_UNIT_SET]: jest.fn(),
            [FACTORIAL_DURATIONS_SET]: jest.fn(),
            [FACTORIAL_DURATION_UNIT_SET]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data()', () => {
        test('data is initialised correctly', () => {
            expect(typeof FactorialDesign.data).toBe('function');
            const defaultData = FactorialDesign.data();
            expect(defaultData).toHaveProperty('interventionTypes');
            expect(defaultData.interventionTypes).toHaveLength(constants.INTERVENTION_TYPES.length);
        });
    });

    describe('computed', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = mount(FactorialDesign, {
                store,
                localVue
            });
        });

        describe('elementParams', () => {
            test('returns the element parameters for the Factorial Design', () => {
                expect(wrapper.vm.elementParams).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.elementParams
                );
            });
        });

        describe('interventionType', () => {
            test('get()', () => {
                const { interventionType } = testStudyDesignPlanner.treatmentPlan.elementParams;
                console.log(JSON.stringify(interventionType));
                expect(wrapper.vm.interventionType.label).toBe(
                    interventionType.label
                );
                expect(wrapper.vm.interventionType.value).toBe(
                    interventionType.iri
                );
            });
        });

        describe('agents', () => {
            test('get()', () => {
                expect(wrapper.vm.agents).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.elementParams.agents
                );
            });
            /*
            test('set()', () => {
                const testAgents = ['sugar', 'honey', 'maple syrup'];
                const select = wrapper.findComponent({ name: 'agents' });
                wrapper.vm.agents = testAgents;
                expect(mutations[FACTORIAL_AGENTS_SET]).toHaveBeenCalledWith(
                    state, testAgents
                );
            }); */
        });
        describe('intensities', () => {
            test('get()', () => {
                expect(wrapper.vm.intensities).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.elementParams.intensities
                );
            });
            /*
            test('set()', () => {
                const testIntensities = [1.2, 6, 30];
                wrapper.vm.agents = testIntensities;
                expect(mutations[FACTORIAL_INTENSITES_SET]).toHaveBeenCalledWith(
                    state, testIntensities
                );
            }); */
        });
        describe('intensityUnit', () => {
            test('get()', () => {
                expect(wrapper.vm.intensityUnit).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.elementParams.intensityUnit
                );
            });
            /*
            test('set()', () => {
                const testIntensityUnit = 'grams';
                wrapper.vm.agents = testIntensityUnit;
                expect(mutations[FACTORIAL_INTENSITY_UNIT_SET]).toHaveBeenCalledWith(
                    state, testIntensityUnit
                );
            }); */
        });
        describe('durations', () => {
            test('get()', () => {
                expect(wrapper.vm.durations).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.elementParams.durations
                );
            });
            /*
            test('set()', () => {
                const testDurations = [1.2, 6, 30];
                wrapper.vm.agents = testDurations;
                expect(mutations[FACTORIAL_DURATIONS_SET]).toHaveBeenCalledWith(
                    state, testDurations
                );
            }); */
        });
        describe('durationUnit', () => {
            test('get()', () => {
                expect(wrapper.vm.durationUnit).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.elementParams.durationUnit
                );
            });
            /*
            test('set()', () => {
                const testDurationUnit = 'grams';
                wrapper.vm.agents = testDurationUnit;
                expect(mutations[FACTORIAL_DURATION_UNIT_SET]).toHaveBeenCalledWith(
                    state, testDurationUnit
                );
            }); */
        });
    });
});
