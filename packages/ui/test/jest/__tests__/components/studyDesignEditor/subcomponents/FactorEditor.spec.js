import FactorEditor from 'src/components/studyDesignEditor/subcomponents/FactorEditor';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms';
import { OBSERVATIONAL_FACTOR_REMOVE } from 'src/store/mutation-types';

describe('FactorEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let mutations;
    let state;
    let store;
    const factorIndex = 0;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        mutations = {
            [OBSERVATIONAL_FACTOR_REMOVE]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('methods', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = mount(FactorEditor, {
                store,
                localVue,
                propsData: {
                    index: factorIndex
                }
            });
        });

        describe('remove', () => {
            test('it triggers the OBSERVATIONAL_FACTOR_REMOVE mutation', () => {
                wrapper.vm.remove();
                expect(mutations[OBSERVATIONAL_FACTOR_REMOVE]).toHaveBeenCalledWith(state, factorIndex);
            });
        });
    });
});
