import CrossoverDesign from 'src/components/studyDesignEditor/subcomponents/CrossoverDesign';
// import TreatmentEditor from 'src/components/TreatmentEditor';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { Quasar } from 'quasar';
import { components } from '../../../utils';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms';

describe('CrossoverDesign', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let state, actions, store, wrapper;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        actions = {
            generateStudyDesign: jest.fn(),
            addSamplePlanItem: jest.fn(),
            removeSamplePlanItem: jest.fn(),
            retrieveAssayConfigs: jest.fn(),
            addAssayPlanItem: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
        wrapper = shallowMount(CrossoverDesign, {
            localVue,
            store
        });
    });

    describe('data()', () => {
        test('data is initialised correctly', () => {
            expect(typeof CrossoverDesign.data).toBe('function');
            const defaultData = CrossoverDesign.data();
            expect(defaultData).toStrictEqual({});
        });
    });
    /* FIXME broken tests
    describe('computed', () => {
        describe('treatmentIds', () => {
            test('the treatment count is 2 once the component is mounted', () => {
                expect(wrapper.vm.treatmentIds.get()).toStrictEqual(
                    testStudyDesignPlanner.temp.treatmentPlan.treatments.map((treatment, index) => index)
                );
            });
        });
        describe('treatments', () => {
            test('returns as many treatment elements as the number of treatment editors', () => {
                console.log(testStudyDesignPlanner.temp.treatmentPlan.treatments);
                expect(wrapper.vm.treatmentCount).toBe(
                    testStudyDesignPlanner.temp.treatmentPlan.treatments.length
                );
            });
        });
    });

    describe('methods', () => {
        describe('addTreatment', () => {
            test('a new treatment editor element is added', () => {
                wrapper.vm.addTreatment();
            });
        });

        describe('removeTreatment', () => {
            test('a treatment editor element is removed', () => {
                wrapper.vm.removeTreatment(1);
            });
        });
    });
    */
});
