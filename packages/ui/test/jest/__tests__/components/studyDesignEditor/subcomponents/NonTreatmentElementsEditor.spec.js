import NonTreatmentElementsEditor from 'src/components/studyDesignEditor/subcomponents/NonTreatmentElementsEditor';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';
import {
    SCREEN_RESET, RUN_IN_RESET, WASHOUT_RESET, FOLLOW_UP_RESET
} from 'src/store/mutation-types';

describe('NonTreatmentElementsEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let mutations;
    let state;
    let store;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        mutations = {
            [SCREEN_RESET]: jest.fn(),
            [RUN_IN_RESET]: jest.fn(),
            [WASHOUT_RESET]: jest.fn(),
            [FOLLOW_UP_RESET]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data', () => {
        test('data is initialised correctly', () => {
            expect(typeof NonTreatmentElementsEditor.data).toBe('function');
            const defaultData = NonTreatmentElementsEditor.data();
            expect(defaultData).toStrictEqual({});
        });
    });

    describe('computed', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = mount(NonTreatmentElementsEditor, {
                store,
                localVue
            });
        });

        describe('screenDuration', () => {
            test('get()', () => {
                expect(wrapper.vm.screenDuration).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.screen.duration
                );
            });
        });

        describe('screenDurationUnit', () => {
            test('get()', () => {
                expect(wrapper.vm.screenDurationUnit).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.screen.durationUnit
                );
            });
        });

        describe('runInDuration', () => {
            test('get()', () => {
                expect(wrapper.vm.runInDuration).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.runIn.duration
                );
            });
        });

        describe('runInDurationUnit', () => {
            test('get()', () => {
                expect(wrapper.vm.runInDurationUnit).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.runIn.durationUnit
                );
            });
        });

        describe('washoutDuration', () => {
            test('get()', () => {
                expect(wrapper.vm.washoutDuration).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.washout.duration
                );
            });
        });

        describe('washoutDurationUnit', () => {
            test('get()', () => {
                expect(wrapper.vm.washoutDurationUnit).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.washout.durationUnit
                );
            });
        });

        describe('followUpDuration', () => {
            test('get()', () => {
                expect(wrapper.vm.followUpDuration).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.followUp.duration
                );
            });
        });

        describe('followUpDurationUnit', () => {
            test('get()', () => {
                expect(wrapper.vm.followUpDurationUnit).toStrictEqual(
                    testStudyDesignPlanner.treatmentPlan.followUp.durationUnit
                );
            });
        });
    });

    /*
    describe('watch', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(NonTreatmentElementsEditor, {
                store,
                localVue
            });
        });

        describe('hasScreen', () => {
            test('triggers the appropriate RESET mutation', async() => {
                await wrapper.setData({ hasScreen: true });
                await wrapper.setData({ hasScreen: false });
                expect(mutations[SCREEN_RESET]).toHaveBeenCalledTimes(1);
            });
        });

        describe('hasRunIn', () => {
            test('triggers the appropriate RESET mutation', async() => {
                await wrapper.setData({ hasRunIn: true });
                await wrapper.setData({ hasRunIn: false });
                expect(mutations[RUN_IN_RESET]).toHaveBeenCalledTimes(1);
            });
        });

        describe('hasWashout', () => {
            test('triggers the appropriate RESET mutation', async() => {
                await wrapper.setData({ hasWashout: true });
                await wrapper.setData({ hasWashout: false });
                expect(mutations[WASHOUT_RESET]).toHaveBeenCalledTimes(1);
            });
        });

        describe('hasFollowUp', () => {
            test('triggers the appropriate RESET mutation', async() => {
                await wrapper.setData({ hasFollowUp: true });
                await wrapper.setData({ hasFollowUp: false });
                expect(mutations[FOLLOW_UP_RESET]).toHaveBeenCalledTimes(1);
            });
        });
    }); */
});
