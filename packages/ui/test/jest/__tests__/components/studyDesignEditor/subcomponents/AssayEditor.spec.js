import AssayEditor from 'src/components/studyDesignEditor/subcomponents/AssayEditor';
import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import { processAnnotations } from 'src/utils/funcs';
import testStudyDesignPlanner from '../../../../../fixtures/crossover-study-design-4-arms-blood-derma-nmr-ms.json';
import { ASSAY_TYPE_SELECTED_CELL_TOGGLE } from 'src/store/mutation-types';

describe('AssayEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let actions;
    let mutations;
    let state;
    let store;
    const assayIndex = 0;
    const assayName = testStudyDesignPlanner.assayPlan[assayIndex].name;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        actions = {
            updateSampleTypesInAssayPlanItem: jest.fn()
        };
        mutations = {
            [ASSAY_TYPE_SELECTED_CELL_TOGGLE]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    actions,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data', () => {
        test('data is initialised correctly', () => {
            expect(typeof AssayEditor.data).toBe('function');
            const defaultData = AssayEditor.data();
            expect(defaultData).toStrictEqual({});
        });
    });

    describe('computed', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(AssayEditor, {
                store,
                localVue,
                propsData: {
                    name: assayName
                }
            });
        });

        describe('config', () => {
            test('correctly returns the current assay config', () => {
                expect(wrapper.vm.config).toStrictEqual(testStudyDesignPlanner.assayPlan[assayIndex]);
            });
        });

        describe('assayConfigIndex', () => {
            test('correctly returns the current assayConfigIndex', () => {
                expect(wrapper.vm.assayConfigIndex).toStrictEqual(assayIndex);
            });
        });

        describe('workflow', () => {
            test('correctly returns the current workflow', () => {
                expect(wrapper.vm.workflow).toStrictEqual(testStudyDesignPlanner.assayPlan[assayIndex].workflow);
            });
        });

        describe('selectedCells', () => {
            test('correctly returns the current assay selected cells', () => {
                expect(wrapper.vm.selectedCells).toStrictEqual(testStudyDesignPlanner.assayPlan[assayIndex].selectedCells);
            });
        });

        describe('sampleTypeOptions', () => {
            test('correctly returns the current assay sampleTypeOptions', () => {
                for (const [armName, armSampleTypes] of Object.entries(wrapper.vm.sampleTypeTable)) {
                    expect(wrapper.vm.sampleTypeOptions[armName]).toStrictEqual(armSampleTypes.map(processAnnotations));
                }
            });
        });

        describe('selectedSampleTypes', () => {
            test('correctly returns the current assay selectedSampleTypes', () => {
                for (const armName of Object.keys(wrapper.vm.config.selectedSampleTypes)) {
                    expect(Array.isArray(wrapper.vm.sampleTypeOptions[armName])).toBe(true);
                }
            });
        });
    });

    describe('methods', () => {
        let wrapper;

        const testProductNodeSpecs = testStudyDesignPlanner.assayPlan[assayIndex].workflow[1][1];
        const testProtocolNodeSpecs = testStudyDesignPlanner.assayPlan[assayIndex].workflow[0][1];

        beforeEach(() => {
            wrapper = mount(AssayEditor, {
                store,
                localVue,
                propsData: {
                    name: assayName
                }
            });
        });

        describe('isProductNode', () => {
            test('returns true for a product node', () => {
                expect(wrapper.vm.isProductNode(testProductNodeSpecs)).toBe(true);
            });

            test('returns false for a protocol node', () => {
                expect(wrapper.vm.isProductNode(testProtocolNodeSpecs)).toBe(false);
            });
        });
        describe('isProtocolNode', () => {
            test('returns false for a product node', () => {
                expect(wrapper.vm.isProtocolNode(testProductNodeSpecs)).toBe(false);
            });

            test('returns true for a protocol node', () => {
                expect(wrapper.vm.isProtocolNode(testProtocolNodeSpecs)).toBe(true);
            });
        });
        describe('toggleCell and setSampleTypes', () => {
            test('triggers a mutation', () => {
                const testCheckbox = wrapper.findComponent(components.QCheckbox);
                testCheckbox.trigger('click');
                expect(mutations[[ASSAY_TYPE_SELECTED_CELL_TOGGLE]]).toBeCalled();
                // TODO improve me
            });
        });
        describe('setSampleTypes', () => {
            test('triggers an action', () => {
                wrapper.vm.setSampleTypes(['some', 'values'], 'Arm_0', 0);
                /*
                const testSelect = wrapper.findComponent(components.QSelect);
                testSelect.trigger('click');
                */
                expect(actions.updateSampleTypesInAssayPlanItem).toBeCalled();
            });
        });
    });
});
