import SampleEditor, { defaultData } from 'src/components/studyDesignEditor/subcomponents/SampleEditor';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../../../utils';
import { Quasar } from 'quasar';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testStudyDesignPlanner from '../../../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';
import {
    SCREEN_RESET, RUN_IN_RESET, WASHOUT_RESET, FOLLOW_UP_RESET,
    SAMPLE_TYPE_SET,
    SAMPLE_TYPE_SELECTED_CELL_TOGGLE, SAMPLE_TYPE_SIZE_SET
} from 'src/store/mutation-types';

describe('SampleEditor', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components });
    localVue.use(Vuex);

    let mutations;
    let state;
    let store;

    beforeEach(() => {
        state = {
            ...testStudyDesignPlanner
        };
        mutations = {
            [SCREEN_RESET]: jest.fn(),
            [RUN_IN_RESET]: jest.fn(),
            [WASHOUT_RESET]: jest.fn(),
            [FOLLOW_UP_RESET]: jest.fn(),
            [SAMPLE_TYPE_SET]: jest.fn(),
            [SAMPLE_TYPE_SELECTED_CELL_TOGGLE]: jest.fn(),
            [SAMPLE_TYPE_SIZE_SET]: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                studyDesignPlanner: {
                    namespaced: true,
                    state,
                    mutations,
                    getters: studyDesignPlannerStore.getters
                }
            }
        });
    });

    describe('data', () => {
        test('data is initialised correctly', () => {
            expect(typeof SampleEditor.data).toBe('function');
            const actualData = SampleEditor.data();
            expect(actualData).toStrictEqual(defaultData);
        });
    });

    describe('computed', () => {
        const index = 1;
        let wrapper;

        beforeEach(() => {
            wrapper = shallowMount(SampleEditor, {
                store,
                localVue,
                propsData: {
                    index
                }
            });
        });

        describe('currentSampleItem', () => {
            test('returns the item at the given index', () => {
                const item = wrapper.vm.currentSampleItem;
                expect(item).toStrictEqual(state.samplePlan[index]);
            });
        });

        describe('sampleType', () => {
            test('get()', () => {
                expect(wrapper.vm.sampleType).toStrictEqual(wrapper.vm.currentSampleItem.sampleType);
            });
        });

        describe('selectedCells', () => {
            test('returns the item at the given index', () => {
                expect(wrapper.vm.selectedCells).toStrictEqual(state.samplePlan[index].selectedCells);
            });
        });

        describe('sampleTypeSizes', () => {
            test('it returns the sizes matrix for the current sample type', () => {
                expect(wrapper.vm.sampleTypeSizes).toStrictEqual(wrapper.vm.currentSampleItem.sampleTypeSizes);
            });
        });
    });

    describe('methods', () => {
        const index = 1;
        let wrapper;
        const testArm = testStudyDesignPlanner.arms.selected[0];
        const armName = testArm.name;

        beforeEach(() => {
            wrapper = mount(SampleEditor, {
                store,
                localVue,
                propsData: {
                    index
                }
            });
        });

        describe('toggleCell', () => {
            test('it triggers the SAMPLE_TYPE_SELECTED_CELL_TOGGLE mutation', async() => {
                const qCheckboxes = wrapper.findAllComponents(components.QCheckbox);
                const pos = qCheckboxes.length - 10 > 0 ? qCheckboxes.length - 10 : 0;
                const qCheckbox = qCheckboxes.at(pos);
                await qCheckbox.vm.$emit('input', false);
                expect(mutations[SAMPLE_TYPE_SELECTED_CELL_TOGGLE]).toHaveBeenCalledTimes(1);
            });
        });
        describe('getCell', () => {
            test('it returns the sizes matrix for the current sample type', () => {
                for (let epochIndex = 0; epochIndex < testArm.length; epochIndex++) {
                    expect(wrapper.vm.getCell(armName, epochIndex)).toStrictEqual(
                        wrapper.vm.selectedCells[armName][epochIndex]
                    );
                }
            });
        });
        describe('setSampleTypeSize', () => {
            test('it triggers the SAMPLE_TYPE_SIZE_SET mutation', async() => {
                const sampleSize = 3;
                const epochIndex = 0; // first epoch
                wrapper.vm.setSampleTypeSize(sampleSize, armName, epochIndex);
                /*
                const qInputs = wrapper.findAllComponents(components.QCheckbox);
                const pos = qInputs.length - 10 > 0 ? qInputs.length - 10 : 0;
                const qInput = qInputs.at(pos);
                await qInput.vm.$emit('input', sampleSize);
                */
                expect(mutations[SAMPLE_TYPE_SIZE_SET]).toHaveBeenCalledTimes(1);
            });
        });
    });
});
