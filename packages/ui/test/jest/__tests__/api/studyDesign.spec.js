import axios from 'axios';
import api from 'src/api/studyDesign';

jest.mock('axios');

const payload = {
    name: 'test study design',
    arms: []
};

describe('studyDesign API', () => {
    test('it make a POST call to /api/studyDesign/computeArms', async() => {
        axios.post.mockImplementationOnce(() => Promise.resolve({
            data: payload
        }));
        const res = await api.computeArms(payload);
        expect(res.data).toStrictEqual(payload);
    });
});
