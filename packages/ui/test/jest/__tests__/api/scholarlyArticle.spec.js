import axios from 'axios';
import api from 'src/api/scholarlyArticle';

jest.mock('axios');

const testScholarlyArticle = {
    name: 'A new article',
    article: 'Massimo Pizzo',
    abstract: 'How to generate random messages using a quasi-lapalissian distribution.',
    keyword: ['cowdung', 'rock', 'roll']
};

describe('scholarlyArticle API', () => {
    test('it make a POST call to /api/scholarlyArticle', async() => {
        axios.post.mockImplementationOnce(() => Promise.resolve({
            data: testScholarlyArticle
        }));
        const res = await api.generateScholarlyArticle(testScholarlyArticle);
        expect(res).toStrictEqual(testScholarlyArticle);
    });
});
