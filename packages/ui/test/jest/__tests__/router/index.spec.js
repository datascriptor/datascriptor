import VueRouter from 'vue-router';
import router from 'src/router';

describe('router', () => {
    test('it is a router', () => {
        const datascriptorRouter = router();
        expect(datascriptorRouter).toBeInstanceOf(VueRouter);
        const res = datascriptorRouter.options.scrollBehavior();
        expect(res).toHaveProperty('x');
        expect(res).toHaveProperty('y');
    });
});
