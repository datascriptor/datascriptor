import { mount, createLocalVue, shallowMount } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { components } from './utils';
import { Quasar } from 'quasar';
import App from 'src/App';

describe('App', () => {
    const localVue = createLocalVue();
    localVue.use(Quasar, { components }); // , lang: langEn
    localVue.use(VueRouter);
    localVue.use(Vuex);

    let actions;
    let state;
    let store;

    beforeEach(() => {
        state = {};
        actions = {
            autoLogin: jest.fn()
        };
        store = new Vuex.Store({
            modules: {
                auth: {
                    namespaced: true,
                    state,
                    actions
                }
            }
        });
    });

    describe('mounted()', () => {
        test('triggers the autoLogin operation', () => {
            const wrapper = shallowMount(App, {
                store,
                localVue
            });
            wrapper.vm.$nextTick(() => {
                expect(actions.autoLogin).toHaveBeenCalledTimes(1);
            });
        });
    });
});
