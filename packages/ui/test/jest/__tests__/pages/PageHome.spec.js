import PageHome from 'src/pages/PageHome';
// import { shallowMount, createLocalVue } from '@vue/test-utils';
// import Vuex from 'vuex';
// import { components } from '../utils';
// import { Quasar, ClosePopup } from 'quasar';

describe('PageHome', () => {
  describe('data', () => {
    test('data is initialised correctly', () => {
      expect(typeof PageHome.data).toBe('function');
      const defaultData = PageHome.data();
      expect(defaultData).toStrictEqual({
        slide: 'style'
      });
    });
  });
});
