import StudyDesignEditor from 'src/pages/StudyDesignEditor';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../utils';
import { Quasar, ClosePopup } from 'quasar';
import authStore from 'src/store/auth';
import studyDesignPlannerStore from 'src/store/studyDesignPlanner';
import testUser from '../../../fixtures/user.json';
import testStudyDesignPlanner from '../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';
import {
  SCREEN_RESET, RUN_IN_RESET, WASHOUT_RESET, FOLLOW_UP_RESET
} from 'src/store/mutation-types';

describe('StudyDesignEditor', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, {
    components,
    directives: {
      ClosePopup
    }
  });
  localVue.use(Vuex);

  let mutations;
  let plannerState;
  let store;
  let userState;
  let actions;

  const $route = {
    params: {
      id: 1
    }
  };

  beforeEach(() => {
    plannerState = {
      ...testStudyDesignPlanner
    };
    userState = {
      user: {
        ...testUser
      },
      token: testUser.tokens[0].token
    };
    mutations = {
      [SCREEN_RESET]: jest.fn(),
      [RUN_IN_RESET]: jest.fn(),
      [WASHOUT_RESET]: jest.fn(),
      [FOLLOW_UP_RESET]: jest.fn()
    };
    actions = {
      getStudy: jest.fn()
    };
    store = new Vuex.Store({
      modules: {
        studyDesignPlanner: {
          namespaced: true,
          state: plannerState,
          mutations,
          getters: studyDesignPlannerStore.getters,
          actions
        },
        auth: {
          namespaced: true,
          state: userState,
          getters: authStore.getters
        }
      }
    });
  });

  describe('data', () => {
    test('data is initialised correctly', () => {
      expect(typeof StudyDesignEditor.data).toBe('function');
      const defaultData = StudyDesignEditor.data();
      expect(defaultData).toStrictEqual({
        step: 1,
        alert: false,
        errorPayload: null,
        statusCode: null
      });
    });
  });

  describe('computed', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = shallowMount(StudyDesignEditor, {
        store,
        localVue,
        mocks: {
          $route
        }
      });
    });

    describe('studyDesignPlanner', () => {
      test('returns the studyDesignPlanner object', () => {
        expect(wrapper.vm.studyDesignPlanner).toStrictEqual(plannerState);
      });
    });

    describe('studyDesign', () => {
      test('returns the studyDesign object', () => {
        expect(wrapper.vm.studyDesign).toStrictEqual(plannerState.studyDesign);
      });
    });
  });
});
