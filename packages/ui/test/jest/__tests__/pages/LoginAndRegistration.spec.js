import LoginAndRegistration, { defaultData as data } from 'src/pages/LogInAndRegistration';
import Vue from 'vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { components } from '../utils';
import { Quasar, Dialog } from 'quasar';
import axios from 'axios';
import { state, getters, mutations } from 'src/store/auth';

// have to add the lines below to avoid this.$q being undefined, and the following message: [Vue warn]: Error in render: "TypeError: Cannot read property 'dark' of undefined"
// see this discussion: https://forum.quasar-framework.org/topic/3461/quasar-testing-vue-warn-error-in-render-typeerror-cannot-read-property-lang-of-undefined/2
Vue.use(Quasar, {
  components,
  plugins: { Dialog }
});

const testUser = {
  name: 'Pippo',
  email: 'pippo@pippo.org',
  isAdmin: true
};
const testToken = 'dgfrhtjhpth.ghirjhoptjhop.jtkjhkj';

jest.mock('axios');

describe('LoginAndRegistration', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, {
    components,
    plugins: { Dialog }
  });
  localVue.use(Vuex);

  let actions;
  let store;

  beforeEach(() => {
    actions = {
      setCredentials: jest.fn()
    };
    store = new Vuex.Store({
      modules: {
        auth: {
          namespaced: true,
          state,
          mutations,
          getters,
          actions
        }
      }
    });
  });

  describe('data', () => {
    test('data is initialised correctly', () => {
      expect(typeof LoginAndRegistration.data).toBe('function');
      const defaultData = LoginAndRegistration.data();
      expect(defaultData).toStrictEqual(data);
    });
  });

  describe('methods', () => {
    let wrapper, $router;

    beforeEach(() => {
      $router = {
        push: jest.fn()
      };
      wrapper = shallowMount(LoginAndRegistration, {
        store,
        localVue,
        mocks: {
          $router
        }
      });
    });

    describe('logIn()', () => {
      test('signs in successfully', async() => {
        // TODO we should mock the resolved value on the basis of the inputs
        axios.post.mockResolvedValueOnce({
          data: {
            user: testUser,
            token: testToken
          }
        });
        await wrapper.setData({
          signIn: {
            email: testUser.email,
            password: 'fjshdhdhjdg'
          }
        });
        await wrapper.vm.logIn();
        expect(actions.setCredentials).toHaveBeenCalledTimes(1);
        expect($router.push).toHaveBeenCalledTimes(1);
      });
      test('opens a dialog on error', async() => {
        const spy = jest.spyOn(wrapper.vm.$q, 'dialog');
        const dummyError = {
          message: 'User auth failed. Could not log in.'
        };
        axios.post.mockRejectedValueOnce(dummyError);
        await wrapper.vm.logIn();
        expect(spy).toHaveBeenCalledWith({
          title: 'User Login Failed.',
          message: dummyError.message
        });
      });
    });

    describe('register()', () => {
      test('signs up successfully', async() => {
        // TODO we should mock the resolved value on the basis of the inputs
        axios.post.mockResolvedValueOnce({
          data: {
            user: testUser,
            token: testToken
          }
        });
        await wrapper.setData({
          signIn: {
            username: testUser.name,
            email: testUser.email,
            password: 'fjshdhdhjdg'
          }
        });
        await wrapper.vm.register();
        expect(actions.setCredentials).toHaveBeenCalledTimes(1);
        expect($router.push).toHaveBeenCalledTimes(1);
      });
      test('opens a dialog on error', async() => {
        const spy = jest.spyOn(wrapper.vm.$q, 'dialog');
        const dummyError = {
          message: 'User registration failed. Could not sign up the user.'
        };
        axios.post.mockRejectedValueOnce(dummyError);
        await wrapper.vm.register();
        expect(spy).toHaveBeenCalledWith({
          title: 'User Registration Failed.',
          message: dummyError.message
        });
      });
    });
  });
});
