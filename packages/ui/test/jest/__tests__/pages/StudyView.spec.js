import StudyView from 'src/pages/StudyView';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import { components } from '../utils';
import { Quasar } from 'quasar';
import researchStore from 'src/store/research';
import Vuex from 'vuex';
import testStudyDesignPlanner from '../../../fixtures/factorial-study-design-12-arms-blood-saliva-genomeseq-ms.json';

describe('StudyView', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components });
  localVue.use(Vuex);

  const $route = {
    params: {
      id: 1
    }
  };

  const {
    assayConfigs,
    assayInTab,
    remoteError,
    errors,
    name,
    description,
    ...design
  } = testStudyDesignPlanner;

  let state;
  let actions;
  let store;

  beforeEach(() => {
    state = {
      study: {
        name,
        description,
        design
      }
    };
    actions = {
      getStudy: jest.fn()
    };
    store = new Vuex.Store({
      modules: {
        research: {
          namespaced: true,
          state,
          mutations: researchStore.mutations,
          getters: researchStore.getters,
          actions
        }
      }
    });
  });

  const createComponent = () => {
    return shallowMount(StudyView, {
      localVue,
      store,
      mocks: {
        $route
      }
    });
  };

  describe('computed', () => {
    test('the computed properties are correctly set for a factorial design', () => {
      const wrapper = createComponent();
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.showSpinner).toBe(false);
        expect(wrapper.vm.error).toBe(null);
        expect(wrapper.vm.design).toStrictEqual(design);
        expect(wrapper.vm.designType).toStrictEqual(design.designType);
        expect(wrapper.vm.designElements).toStrictEqual(design.elements);
        expect(wrapper.vm.subjectTypeLabel).toStrictEqual(design.subjectType);
        expect(wrapper.vm.subjectTypeIri).toStrictEqual(null);
        expect(wrapper.vm.hasObservationalFactors).toStrictEqual(false);
        expect(wrapper.vm.treatments).toStrictEqual(design.treatmentPlan.treatments);
        expect(wrapper.vm.hasTreatments).toStrictEqual(true);
        expect(wrapper.vm.studyArms).toStrictEqual(design.arms.selected);
        expect(wrapper.vm.hasArms).toStrictEqual(true);
        expect(wrapper.vm.assayPlan).toStrictEqual(design.assayPlan);
        expect(wrapper.vm.hasAssayPlan).toStrictEqual(true);
      });
    });
  });

  describe('watch', () => {
    test('assayPlan', () => {
      const wrapper = createComponent();
      wrapper.vm.$options.watch.assayPlan.call(wrapper.vm, []);
      expect(wrapper.vm.assayInTab).toBeNull();
      wrapper.vm.$options.watch.assayPlan.call(wrapper.vm, design.assayPlan);
      expect(wrapper.vm.assayInTab).toBe(design.assayPlan[0].name);
    });
  });

  describe('mounted()', () => {
    test('dispatches the getStudy action', () => {
      createComponent();
      expect(actions.getStudy).toHaveBeenCalledTimes(1);
    });
  });

  describe('methods', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallowMount(StudyView, {
        localVue,
        store,
        mocks: {
          $route
        }
      });
    });

    describe('removeSpuriousTreatmentKeys', () => {
      test('should remove the _id and interventionType fields', () => {
        const testIntervention = {
          interventionType: {
            id: 'ero:class:http://purl.obolibrary.org/obo/ERO_0000600',
            iri: 'http://purl.obolibrary.org/obo/ERO_0000600',
            short_form: 'ERO_0000600',
            obo_id: 'ERO:0000600',
            label: 'drug intervention',
            ontology_name: 'ero',
            ontology_prefix: 'ERO',
            type: 'class'
          },
          agent: 'ibuprofen',
          intensity: 8,
          intensityUnit: 'mg/day',
          duration: 12,
          durationUnit: 'days'
        };
        expect(wrapper.vm.removeSpuriousTreatmentKeys(testIntervention)).toStrictEqual({
          agent: 'ibuprofen',
          intensity: 8,
          intensityUnit: 'mg/day',
          duration: 12,
          durationUnit: 'days'
        });
      });
    });
  });
});
