import * as funcs from 'src/utils/funcs';

describe('funcs', () => {
    describe('processAnnotation', () => {
        test('process an array of free-text strings', () => {
            const annotations = ['head', 'body', 'heart'];
            expect(funcs.processAnnotations(annotations)).toStrictEqual([
                { value: 'head', label: 'head' },
                { value: 'body', label: 'body' },
                { value: 'heart', label: 'heart' }
            ]);
        });

        test('process an array of ontology annotations', () => {
            const annotations = [
                { term: 'head', iri: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { term: 'body', iri: 'http://purl.obolibrary.org/obo/NCIT_C13041' },
                { term: 'heart', iri: 'http://purl.obolibrary.org/obo/NCIT_C12727' }
            ];
            expect(funcs.processAnnotations(annotations)).toStrictEqual([
                { label: 'head', value: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { label: 'body', value: 'http://purl.obolibrary.org/obo/NCIT_C13041' },
                { label: 'heart', value: 'http://purl.obolibrary.org/obo/NCIT_C12727' }
            ]);
        });

        test('process an array of ontology annotations with the label key', () => {
            const annotations = [
                { label: 'head', iri: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { label: 'body', iri: 'http://purl.obolibrary.org/obo/NCIT_C13041' }
            ];
            expect(funcs.processAnnotations(annotations)).toStrictEqual([
                { label: 'head', value: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { label: 'body', value: 'http://purl.obolibrary.org/obo/NCIT_C13041' }
            ]);
        });
    });

    describe('displayAnnotation', () => {
        test('display an annotation', () => {
            const annotation = { term: 'head', iri: 'http://purl.obolibrary.org/obo/NCIT_C12419' };
            expect(funcs.displayAnnotation(annotation)).toBe(annotation.term);
        });

        test('display an annotation with label', () => {
            const annotation = { label: 'head', iri: 'http://purl.obolibrary.org/obo/NCIT_C12419' };
            expect(funcs.displayAnnotation(annotation)).toBe(annotation.label);
        });

        test('display a string', () => {
            const annotation = 'body';
            expect(funcs.displayAnnotation(annotation)).toBe(annotation);
        });
    });

    describe('filterAnnotationsByOptions', () => {
        test('process an array of free-text strings', () => {
            const allAvailableAnnotations = ['head', 'body', 'heart'];
            const selectedDisplayOptions = [
                { value: 'head', label: 'head' },
                { value: 'body', label: 'body' }
            ];
            expect(funcs.filterAnnotationsByOptions(allAvailableAnnotations, selectedDisplayOptions)).toStrictEqual([
                'head', 'body'
            ]);
        });

        test('process an array of ontology annotations', () => {
            const allAvailableAnnotations = [
                { term: 'head', iri: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { term: 'body', iri: 'http://purl.obolibrary.org/obo/NCIT_C13041' },
                { term: 'heart', iri: 'http://purl.obolibrary.org/obo/NCIT_C12727' }
            ];
            const selectedDisplayOptions = [
                { label: 'head', value: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { label: 'heart', value: 'http://purl.obolibrary.org/obo/NCIT_C12727' }
            ];
            expect(funcs.filterAnnotationsByOptions(allAvailableAnnotations, selectedDisplayOptions)).toStrictEqual([
                { term: 'head', iri: 'http://purl.obolibrary.org/obo/NCIT_C12419' },
                { term: 'heart', iri: 'http://purl.obolibrary.org/obo/NCIT_C12727' }
            ]);
        });
    });
});
