# Datascriptor App (datascriptor-ui)

The Datascriptor User Interface, built on Quasar

##Prerequisites:
- have yarn or  npm installed (on MacOS, `brew install yarn` or `brew install node` respectively).
- have [quasar](https://quasar.dev) installed:
`yarn global add @quasar/cli` or `npm install -g @quasar/cli`


## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
