const app = require('./index.js');
const winston = require('winston');

const port = process.env.PORT || 3000;

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console()
  ]
});

app.listen(port, () => {
  logger.info(`Datascriptor API listening on port ${port}`);
});

module.exports = exports = app;