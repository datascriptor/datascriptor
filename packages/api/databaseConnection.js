// Import the mongo db library
// params: url, port, email, password
// connect to the database
// return the connection
const mongoose = require('mongoose');
const { User } = require('./app/user/user-model');
const { Study } = require('./app/study/study-model');
const Promise = require('bluebird');
mongoose.Promise = Promise;
mongoose.set('useFindAndModify', false);
const config = require('./config');

async function syncAllCollectionIndexes() {
  try {
    await User.syncIndexes({
      background: false
    });
    await Study.syncIndexes({
      background: false
    });
  } catch(err) {
    console.log(`MongoDB index syncing failed: ${err}`);
    throw err;
  }
}

module.exports = function dbConnect() {
  const { uri } = config.mongodb;
  console.log(`Config is: ${JSON.stringify(config)}`);
  console.log(`MongoDB connection is ${uri}`);
  console.log(`PROCESS.env MongoDB connection is ${process.env.MONGODB_URI}`);
  return mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(() => {
    console.log('Connected to MongoDB...');
    mongoose.connection.on('error', err => {
      console.log(`Something shucked up while connecting to MongoDB: ${err}`);
    });
    // return syncAllCollectionIndexes();
  }).then(() => {
    console.log('MongoDB indexes synced with mongoose schema');
  }).catch(err => {
    console.log(`MongoDB error occurred: ${err}`);
    process.exit(-1);
  });
};