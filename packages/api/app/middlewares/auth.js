const { retrieveAndVerifyToken } = require('../user/user-model');
const errorHandler = require('../utils/errorHandler');
const { JSON_WEB_TOKEN_ERROR } = require('../utils/errors');

const isAuth = async(req, res, next) => {
  console.log(`Req url: ${req.url}`);
  console.log(`Req baseUrl: ${req.baseUrl}`);
  console.log(`Req originalUrl: ${req.originalUrl}`);
  console.log(`Req path: ${req.path}`);
  console.log(`Req host: ${req.headers.host}`);
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    /* do we really need to check the backend every time? */
    const user = await retrieveAndVerifyToken(token);
    req.user = user;
    req.token = token;
    next();
  } catch (error) {
    return errorHandler(res).handleAndSendResponse({
      ...error,
      name: JSON_WEB_TOKEN_ERROR
    });
  }

};

const isAdmin = async(req, res, next) => {
  const { user } = req;
  if (!user) {
    return errorHandler(res).unauthorized();
  }
  if (user.isAdmin) {
    next();
  } else {
    return errorHandler(res).forbidden({
      message: `User ${user.name} is not an admin. Only admins can perform this operation.`
    });
  }
};

module.exports = exports = {
  isAuth,
  isAdmin
};
