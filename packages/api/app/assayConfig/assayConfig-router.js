const express = require('express');
const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');
const router = express.Router();
const errorHandler = require('../utils/errorHandler');
// const LOCATION_HEADER = '/api/assayConfig';

const readFileAsync = Promise.promisify(fs.readFile);


async function retrieveAll(req, res) {
  try {
    const filePath = path.join(__dirname, '..', '..', 'test', 'fixtures', 'assay-configs.json');
    const data = await readFileAsync(filePath);
    const assayConfigurations =  JSON.parse(data);
    // console.log(`AssayConfigRouter.retrieveAll - found configurations: ${JSON.stringify(assayConfigurations)}`);
    return res.status(200).json(assayConfigurations);
  }
  catch(err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

router.get('/assayConfig', retrieveAll);

module.exports = exports = router;