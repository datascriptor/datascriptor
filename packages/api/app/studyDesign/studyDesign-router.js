const express = require('express');
const router = express.Router();

const errorHandler = require('../utils/errorHandler');
const { research: { 
  armCollectionBuilder, computeSubjectGroupsFromObservationalFactors
} } = require('@datascriptor/core');

function computeSubjectGroups(req, res) {
  try {
    const { subjectType, size, observationalFactors } = req.body;
    const subjectGroups = computeSubjectGroupsFromObservationalFactors(
      subjectType, observationalFactors, size
    );
    return res.status(200).json(subjectGroups);
  }
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

function computeArms(req, res) {
  try {
    const { type, ...reqPayload } = req.body;
    const studyDesign = armCollectionBuilder[type](reqPayload);
    return res.status(200).json(studyDesign);
  }
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

router.post('/studyDesign/computeSubjectGroups', computeSubjectGroups);
router.post('/studyDesign/computeArms', computeArms);

module.exports = exports = router;