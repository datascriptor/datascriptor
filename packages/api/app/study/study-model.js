const mongoose = require('mongoose');
const uuid = require('uuid');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const studySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  complete: {
    type: Boolean,
    default: true
  },
  private: {
    type: Boolean,
    default: false
  },
  design: {
    designType: {
      type: Schema.Types.Mixed  // string or pojo (ontology annotation)
    },
    subjectType: {
      type: Schema.Types.Mixed  // string or pojo (ontology annotation)
    },
    subjectSize: {
      type: Number
    },
    observationalFactors: {
      type: Array,
      required: true
    },
    subjectGroups: {
      selected: {
        type: Array,
        required: true
      },
      unselected: {
        type: Array,
        required: true
      }
    },
    treatmentPlan: {
      observationPeriod: {
        name: String,
        duration: Number,
        durationUnit: Schema.Types.Mixed // string or pojo (ontology annotation)
      },
      screen: {
        selected: Boolean,
        name: String,
        duration: Number,
        durationUnit: Schema.Types.Mixed // string or pojo (ontology annotation)
      },
      runIn: {
        selected: Boolean,
        name: String,
        duration: Number,
        durationUnit: Schema.Types.Mixed // string or pojo (ontology annotation)
      },
      washout: {
        selected: Boolean,
        name: String,
        duration: Number,
        durationUnit: Schema.Types.Mixed // string or pojo (ontology annotation)
      },
      followUp: {
        selected: Boolean,
        name: String,
        duration: Number,
        durationUnit: Schema.Types.Mixed // string or pojo (ontology annotation)
      },
      treatments: [
        {
          interventionType: Schema.Types.Mixed,  // string or pojo (ontology annotation)
          agent: Schema.Types.Mixed,
          intensity: Number,
          intensityUnit: Schema.Types.Mixed,  // string or pojo (ontology annotation)
          duration: Number,
          durationUnit: Schema.Types.Mixed  // string or pojo (ontology annotation)
        }
      ], // for Crossover Design
      elementParams: {
        interventionType: Schema.Types.Mixed,  // string or pojo (ontology annotation)
        agents: Array,
        intensities: Array,
        intensityUnit: Schema.Types.Mixed,   // string or pojo (ontology annotation)
        durations: Array,
        durationUnit: Schema.Types.Mixed   // string or pojo (ontology annotation)
      }
    },
    elements: [{
      id: String,
      name: String,
      duration: Number,
      durationUnit: Schema.Types.Mixed,  // string or pojo (ontology annotation)
      agent: Schema.Types.Mixed,
      intensity: Number,
      intensityUnit: Schema.Types.Mixed  // string or pojo (ontology annotation)
    }],
    /* events is basically DEPRECATED
    events: [{
      id: String,
      action: String,
      input: Schema.Types.Mixed,
      output: Schema.Types.Mixed,
      outputSize: Number,
      parameters: Schema.Types.Mixed
    }], */
    arms: {
      selected:[{
        id: String,
        name: {
          type: String,
          required: true
        },
        subjectType: Schema.Types.Mixed,  // string or pojo (ontology annotation)
        size: Number,
        observationalFactors: [{
          name: Schema.Types.Mixed,  // string or pojo (ontology annotation)
          value: Schema.Types.Mixed,  // string or pojo (ontology annotation)
          unit: Schema.Types.Mixed,  // string or pojo (ontology annotation)
          isQuantitative: Boolean
        }],
        epochs: [{
          elements: [{
            type: String  // ids of the elements contained in the element array above
          }]
          /* events is basically DEPRECATED
          events: [{
            type: String
          }] */
        }]
      }],
      unselected: Array
    },
    samplePlan: Array,
    assayPlan: Array
  },
  resultTables: Array,
  owner: {
    type: String,
    required: true
  }
}, {
  timestamps: true
});

// wildcard text index for full-text search
studySchema.index({ '$**' : 'text' });

studySchema.plugin(AutoIncrement, {
  // eslint-disable-next-line camelcase
  inc_field: 'mongo_id'
});

const Study = mongoose.model('Study', studySchema);

/**
 * @function
 * @name create
 * @param {Object} study - object to be created
 * @returns Object - the created study
 */
async function create(study) {
  /*
  if (!study.identifier) {
    study.identifier = `s_${uuid.v4()}`; // TODO replace with uuidv5?
  } */
  const createdStudy = await Study.create(study);
  return createdStudy;
}

async function retrieveOne(id) {
  // const study = await Study.findOne({ identifier });
  const study = await Study.findById(id);
  return study;
}

async function retrieveAll(criteria =  {}) {
  const studies = await Study
    .find(criteria.filter)
    .skip(criteria.skip)
    .limit(criteria.limit)
    .sort(criteria.sort || {
      createdAt: -1
    });
  return studies;
}

async function countAll(filterCriteria) {
  return await Study.countDocuments(filterCriteria);
}

async function update(studyObj) {
  // An alternative option would be to use `Model.findByIdAndUpdate()`. But does it update the `updatedAt` field?
  const { _id } = studyObj;
  console.log(`StudyModel.update() - ${_id}`);
  const study = await Study.findById(_id);
  if (!study) {
    // TODO make it a resource not found error
    throw new Error({
      name: 'ResourceNotFoundError',
      message: `Study ${_id} not found`
    });
  }
  // Is this condition too restrictive?
  if (study.owner && study.owner !== studyObj.owner) {
    console.log(`PROBLEM WITH OWNER ${study.owner}`);
    throw new Error(`Study owner ${study.owner} does not match with updated study owner ${studyObj.owner}`);
  }
  console.log(`FOUND STUDY ${JSON.stringify(study)}`);
  if (!study.owner) {
    study.owner = studyObj.owner;
  }
  console.log(`OWNER ${study.owner}`);
  study.name = studyObj.name;
  study.description = studyObj.description;
  study.private = studyObj.private;
  study.design = studyObj.design;
  study.resultTables = studyObj.resultTables;
  console.log('READY TO SAVE');
  await study.save();
  return study;
}

async function deleteOne(id) {
  const study = await Study.findByIdAndDelete(id);
  if (!study) {
    throw new Error({
      name: 'NotFoundError',
      message: `Study ${id} was not found on this server.`
    });
  }
  return study;
}

module.exports = exports = {
  Study,
  create,
  retrieveOne,
  retrieveAll,
  countAll,
  update,
  deleteOne
};