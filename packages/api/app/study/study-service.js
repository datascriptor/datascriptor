const StudyModel = require('./study-model');
const {
  parseParams,
  composePaginationInfo,
  composePaginationHeaders
} = require('../utils/queries');

async function countAllAndPaginate({
  host,
  path,
  parsedParams,
  // paginationInfoasHeaders = true
}) {
  const totalCount = await StudyModel.countAll(parsedParams.filter);
  return await composePaginationInfo({
    host,
    path,
    parsedParams,
    totalCount
  });
}

/**
 * @function
 * @name getAllWithPagination
 * @param {String} host - the URL of the Datascriptor application (e.g. https://datascriptor.netlify.app)
 * @param {String} path - the relative path from the host to the current endpoint (e.g /api/study)
 * @param {Object} queryParams - this coulf contain:
 * @param {String} queryParams.text - strinng to filter studies against the 'text' index of the database
 * @param {Number} queryParams.limit - LIMIT for pagination, an integer
 * @param {Number} queryParams.skip - SKIP for pagination, an integer
 * @param {String} queryParams.sort - SORT criteria for pagination
 * @param {Boolean} queryParams._metadata - if true the pagination infos are sent in the response body
 *  The response body has an "envelope" containing '_metadata' and 'records'. Otherwise, the pagination info
 *  is only shipped in the response headers (custom headers used, see utils/queries.js)
 * @returns 
 */
async function getAllWithPagination({
  host,
  path,
  queryParams,
  reqPayload = {}
}) {
  const parsedParams = parseParams(queryParams, reqPayload);
  const [records, paginationInfo] = await Promise.all([
    StudyModel.retrieveAll(parsedParams),
    countAllAndPaginate({
      host, // FIXME!! Add originalURL
      path,
      parsedParams
    })
  ]);
  const _headers = composePaginationHeaders(paginationInfo);
  console.log(_headers);
  return {
    statusCode: 200,
    headers: {
      ..._headers
    },
    resPayload: queryParams._metadata 
      ? {
        _metadata: paginationInfo,
        records
      }
      : records
  };
}

module.exports = exports = {
  getAllWithPagination
};