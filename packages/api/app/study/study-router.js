const express = require('express');
// const fs = require('fs');
// const formidable = require('formidable');
const router = express.Router();
const StudyModel = require('./study-model');
const { getAllWithPagination } = require('./study-service');
// const { research: { Study } } = require('@datascriptor/core');
// const { text: { StudyTextFactory } } = require('@datascriptor/core');
const { isAuth } = require('../middlewares/auth');
const errorHandler = require('../utils/errorHandler');

// const LOCATION_HEADER = '/api/study';

async function findAll(req, res) {
  try {
    console.log(req.headers);
    console.log(req.get('host'));
    console.log(req.headers.host);
    const {
      headers,
      resPayload
    } = await getAllWithPagination({
      host: req.get('host'),
      path: req.baseUrl + req.path,
      queryParams: req.query
    });
    res.set(headers);
    return res.json(resPayload);
  } 
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function findOne(req, res) {
  try {
    const record = await StudyModel.retrieveOne(req.params.id);
    return res.json(record);
  } 
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function create(req, res) {
  const { body: payload, user: { name: username }  } = req;
  const study = { 
    ...payload,
    owner: username
  };
  try {
    const created = await StudyModel.create(study);       
    return res.status(201)
      .location(`${req.headers.host}${req.originalUrl}/${created._id}`)
      .json(created);
  }
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function update(req, res) {
  try {
    // username should be fetch from auth token
    const { body: payload, user: { name: username }  } = req;
    // check username correspond to owner
    if (!payload.owner) {
      payload.owner = username;
    }
    if (payload.owner === username) {
      // do full update
      const updated = await StudyModel.update(payload);
      return res.json(updated);
    }
    else {
      return errorHandler(res).forbidden({
        message: `User ${username} is not the owner of study ${payload._id}`
      });
    }   
  }
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}
// async function updatePartial(req, res) {}
async function deleteOne(req, res) {
  try {
    // username should be fetch from auth token
    const { params: { id }, user: { name: username }  } = req;
    const study = await StudyModel.retrieveOne(id);
    // check username correspond to owner
    if (study.owner === username) {
      // delete OR soft-delete
      await StudyModel.deleteOne(id);
    }
    else {
      throw new Error({
        name: 'ForbiddenError',
        message: `User ${username} is not allowed to delete study ${id}`
      });
    }
    return res.status(204);
  }
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

/**
 * FIXME - rework this endpoint to generate text from the study
 * TODO - rename 
 * @function
 * @name convert
 * @param {*} req 
 * @param {*} res 
 * @returns 
 *
function convert(req, res) {
    try {
        console.log(`study-model.convert() - request payload: ${JSON.stringify(req.body)}`);
        const study = Study.fromJSON(req.body);
        console.log(`study-model.convert() - study to convert: ${JSON.stringify(study)}`);
        const textFactory = new StudyTextFactory(study);
        const resPayload = {
            arms: textFactory.generateArmsReport(), // FIXME this needs to be fixed
            elements: textFactory.generateElementsReport(),
            events: textFactory.generateEventsReport()
        };
        return res.status(200).json(resPayload);
    }
    catch (err) {
        console.log(err);
        return res.status(500).json({
            message: err.message  
        });
    }
} 

function uploadAndConvert(req, res) {
    try {
        const form = new formidable.IncomingForm();
        form.parse(req, (_, fields, files) => {
            console.log('\n-----------');
            console.log('Fields', fields);
            console.log('Received:', files);
            const [file, ...rest] = Object.values(files);
            const reqPayload = JSON.parse(fs.readFileSync(file.path, 'utf8'));
            const study = Study.fromJSON(reqPayload);
            const textFactory = new StudyTextFactory(study);
            const studyReport = {
                arms: textFactory.generateArmsReport(),
                events: textFactory.generateEventsReport(),
                elements: textFactory.generateElementsReport()
            };
            const resPayload = {
                studyData: reqPayload,
                studyReport
            };
            res.status(200).json(resPayload);
        });
    }
    catch (err) {
        console.log(err);
        return res.status(500).json({
            message: err.message  
        });
    }
}

function createAndConvert(req, res) {}

function getAndConvert(req, res) {}
*/
router.get('/study', findAll);
router.get('/study/:id', findOne);
router.post('/study', isAuth, create);
router.put('/study/:id', isAuth, update);
// router.patch('/study/:id', isAuth, updatePartial);
router.delete('/study/:id', isAuth, deleteOne);
// router.post('/study/convert', convert);
// router.post('/study/uploadAndConvert', uploadAndConvert);

module.exports = exports = router;