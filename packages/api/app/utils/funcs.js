const ORCID_REGEX = /^(\d|X){4}-(\d|X){4}-(\d|X){4}-(\d|X){4}$/gi;

function isOrcid(input) {
  return ORCID_REGEX.test(input);
}

module.exports = exports = {
  isOrcid
};