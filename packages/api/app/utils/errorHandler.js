/**
 * @module errorHandler;
 * @description Utility object for controller responses (wraps Node/Express response object).
 *              Inspired by a similar controller for Sails.js
 * @see {@link https://github.com/grokible/sails-tutorial-1/blob/master/TUTORIALS/TUTORIAL02.md}
 * @see {@link http://sailsjs.org/#/documentation/reference/res|Sails Response('res') Reference}
 */
const errors = require('./errors');

const env = process.env.NODE_ENV || 'development';

const BAD_REQUEST_STATUS_CODE = 400;
const UNAUTHORISED_STATUS_CODE = 401;
const FORBIDDEN_STATUS_CODE = 403;
const NOT_FOUND_STATUS_CODE = 404;
const UNPROCESSABLE_ENTITY_CODE = 422;
const SERVER_ERROR_STATUS_CODE = 500;

class ErrorHandler {

  constructor(response = null) {
    this.response = response;
  }

  /**
     * @method
     * @name handle
     * @description this will handle a generic error for you depending on the error message
     */
  handle(error, options) {
    // Options properties {code, message} override error's
    // After options, underbar takes precedence (_code or _message) override error's

    options = options || {};
    let code = options.code || error._code || error.code || error.name || 'general.unknown';

    // Attempt to pull out any conceivable error message that we can use

    let message = options.message || error._message || error.message || error.description ||
            error.title || error.summary || error.reason || 'No message, unknown error.';

    let rc = {
      code: code,
      message: message
    };

    // console.log(error);

    // If we are in a Node dev environment, supply all the error details
    if (env === 'development') {
      rc._internal = error;
    }

    let statusCode = this.getStatusCodeForError(error);

    return {
      statusCode,
      handledError: rc
    };
  }

  /**
     * @method
     * @name handleAndSendResponse
     * @description this will handle a generic error for you depending on the error message (for Express)
     */
  handleAndSendResponse(err, options) {
    const { statusCode, handledError } = this.handle(err, options);
    return this.response.status(statusCode).json({
      code: statusCode,
      error: handledError
    });
  }

  /**
     * @method
     * @name badRequest
     */
  badRequest(options) {

    options = options || {};

    let code = options.code || errors.BAD_REQUEST_ERROR;

    let message = options.message || 'Not authorized to this REST endpoint';

    return this.response.status(BAD_REQUEST_STATUS_CODE).json({
      code: code,
      message: message
    });

  }


  /**
     * @method
     * @name unauthorized
     */
  unauthorized(options) {

    options = options || {};

    let code = options.code || errors.UNAUTHORIZED_ERROR;

    let message = options.message || 'Not authendicated';

    return this.response.status(UNAUTHORISED_STATUS_CODE).json({
      code: code,
      message: message
    });

  }

  /**
     * @method
     * @name forbidden
     */
  forbidden(options) {

    options = options || {};

    let code = options.code || errors.FORBIDDEN_ERROR;

    let message = options.message || 'Not authorized to this REST endpoint';

    return this.response.status(FORBIDDEN_STATUS_CODE).json({
      code: code,
      message: message
    });

  }

  /**
     * @method
     * @name getStatusCodeForError
     * @param{Error} error
     */
  getStatusCodeForError(error) {

    let errName = error.name || error.code;

    // 401
    if (errName === errors.UNAUTHORIZED_ERROR) {
      return UNAUTHORISED_STATUS_CODE;
    }

    // 403
    if (errName === errors.FORBIDDEN_ERROR) {
      return FORBIDDEN_STATUS_CODE;
    }

    // 404
    if (errName === errors.NOT_FOUND_ERROR) {
      return NOT_FOUND_STATUS_CODE;
    }

    // 422
    if (errName === errors.VALIDATION_ERROR) {
      return UNPROCESSABLE_ENTITY_CODE;
    }

    // 400
    if (errName === errors.INVALID_FORMAT_ERROR) {
      return BAD_REQUEST_STATUS_CODE;
    }

    // 401
    if (errName === errors.JSON_WEB_TOKEN_ERROR) {
      return UNAUTHORISED_STATUS_CODE;
    }

    // 400 (or should it be 422?)
    if (errName === errors.MONGO_ERROR) {
      return BAD_REQUEST_STATUS_CODE;
    }

    // 500
    return SERVER_ERROR_STATUS_CODE;

  }

}

/**
 * @function
 * @name errorHandler
 * @param {Response} response
 */
function errorHandler(response = null) {
  return new ErrorHandler(response);
}

module.exports = exports = errorHandler;
