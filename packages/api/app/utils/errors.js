const BAD_REQUEST_ERROR = 'BadRequestError';
const UNAUTHORIZED_ERROR = 'UnauthorizedError';
const FORBIDDEN_ERROR = 'ForbiddenError';
const NOT_FOUND_ERROR = 'NotFoundError';

const VALIDATION_ERROR = 'ValidationError';
const INVALID_FORMAT_ERROR = 'InvalidFormatError';
const JSON_WEB_TOKEN_ERROR = 'JsonWebTokenError';
const MONGO_ERROR = 'MongoError';

module.exports = exports = {
  BAD_REQUEST_ERROR,
  UNAUTHORIZED_ERROR,
  FORBIDDEN_ERROR,
  NOT_FOUND_ERROR,
  VALIDATION_ERROR,
  INVALID_FORMAT_ERROR,
  JSON_WEB_TOKEN_ERROR,
  MONGO_ERROR
};