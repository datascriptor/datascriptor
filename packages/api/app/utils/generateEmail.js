const nodemailer = require('nodemailer');


async function generateEmail(token, userEmail, origin) {
  const mailOptions = {
    from: 'massimorgon@ethereal.email',
    to: userEmail,
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
  };
  try {
    let testAccount = await nodemailer.createTestAccount();
    let transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: testAccount.user,
        pass: testAccount.pass,
      },
    });
    let info = await transporter.sendMail(mailOptions);
    mailOptions.text = `Your reset link is ${origin}/#/resetPassword?token=${token}.`;
    console.log(`successfully sent ${info.messageId}`);
    return mailOptions;
  }
  catch (e) {
    console.log('Error with email ' + mailOptions);
  }
}
module.exports = exports = generateEmail;
