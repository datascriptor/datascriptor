const querystring = require('querystring');
const { pagination } = require('../../config');

function parseSkip({ skip } = {}) {
  return skip ? Number.parseInt(skip) : pagination.defaultSkip;
}
function parseLimit({ limit } = {}) {
  return limit ? Number.parseInt(limit) : pagination.defaultPageSize;
}
function parseSort({ sort } = {}) {
  return  sort || pagination.defaultSort;
}

/**
 * @function
 * @name parseFilterCriteria
 * @description takes a parsed query object and returns an object with all the filter criteria
 * @param {Object} params 
 * @returns 
 */
function parseFilterCriteria({ text } = {}) {
  // TODO: in order of priority
  // full text search (FTS): see https://docs.mongodb.com/manual/text-search/
  const res = {};
  if (text) {
    res.$text = {
      $search: text
    };
  }
  // filter by Study Name and Description (contains search)
  // filter by Subject Type (string or onto annotations)
  // filter by Treatment Type
  // filter by Sample Type (string or onto annotations)
  // filter by Assay Type (assay name or assay ID?)
  return res;
}

/**
 * @function
 * @name parseParams
 * @param {String/Object} queryStringParams - The query string
 * @param {Object/null} reqPayload - The request payload (i.e. req.body) 
 * @returns Object - The parameters object
 */
function parseParams(queryParams, reqPayload = null) {
  // parse the query string (could you want to merge this this the request body)?
  const params = {
    ...queryParams,
    ...reqPayload
  }; 
  const res = {
    filter: parseFilterCriteria(params), // query selectors, see https://docs.mongodb.com/manual/reference/operator/query/#query-selectors
    // projection: null, // won't be implemented for now, see https://docs.mongodb.com/manual/reference/operator/query/#projection-operators
    sort: parseSort(params),
    limit: parseLimit(params),
    skip: parseSkip(params),
    // population: null // could be useful to populate associated text/report
    _query: queryParams
  };
  if (reqPayload) {
    res._body = reqPayload;
  }
  return res;
}

module.exports = exports = {

  parseSkip,
  parseLimit,
  parseSort,
  parseFilterCriteria,
  parseParams,

  /**
     * @method
     * @name composeHeaderInfo
     * @async
     * @param{String} originalUrl - original URL (?)
     * @param{String} path - path params (?)
     * @param{Object} params - the request params already parsed from the path and query params
     * @return{Promise/Object} all the info to be shipped as response header
     */
  composePaginationInfo: function composePaginationInfo({
    host,
    path = '',
    parsedParams,
    totalCount = 0
  }) {
    // first count all records matching the criteria
    // const params = parseParams(queryString, reqPayload);
    console.log(parsedParams);
    const { 
      // filter,
      // sort,
      limit: pageSize,
      skip,
      _query: queryParams
    } = parsedParams; 
    // const totalCount = await StudyModel.countAll(filter);
    const numPages = Math.ceil(totalCount/pageSize),
      currPage = Math.ceil(skip/pageSize) + 1;

    let queryNext, queryPrevious, queryLast, queryFirst;

    // if we are not returning the last page
    if (currPage <= numPages - 1) {
      queryNext = querystring.stringify(Object.assign({}, queryParams, { limit: pageSize, skip: pageSize + skip }));
      queryLast = querystring.stringify(Object.assign({}, queryParams, { limit: pageSize, skip: (numPages-1)*pageSize }));
    }
    // if we are not returning the first page
    if (currPage > 1) {
      queryPrevious = querystring.stringify(Object.assign({}, queryParams, { limit: pageSize, skip: skip - pageSize }));
      queryFirst = querystring.stringify(Object.assign({}, queryParams, { limit: pageSize, skip: undefined }));
    }

    return {
      totalCount,
      pageSize: pageSize,
      numPages: numPages,
      currPage: currPage,
      links: [
        { value: queryNext ? `${host}${path}?${queryNext}` : null, rel: 'next' },
        { value: queryPrevious ? `${host}${path}?${queryPrevious}` : null, rel: 'previous'},
        { value: queryFirst ? `${host}${path}?${queryFirst}` : null, rel: 'first' },
        { value: queryLast ? `${host}${path}?${queryLast}` : null, rel: 'last'}
      ]
    };

  },

  composePaginationHeaders({
    totalCount,
    pageSize,
    numPages,
    currPage,
    links = []
  }) {
    const headers = {
      'Access-Control-Expose-Headers': [
        'DS-Total-Count',
        'DS-Page-Size',
        'DS-Total-Pages',
        'DS-Current-Page', 
        'Link'
      ].join(', '),
      'DS-Total-Count': totalCount,
      'DS-Page-Size': pageSize,
      'DS-Total-Pages': numPages,
      'DS-Current-Page': currPage,
      // 'Link': linkArr.join(', ')
      link: links.filter(link => link.value).map(({ value, rel }) => {
        return `<${value}>; rel=${rel}`;
      }).join(', ')
    };
    return headers;
  }


};