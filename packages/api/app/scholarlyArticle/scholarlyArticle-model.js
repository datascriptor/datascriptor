const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const scholarlyArticleSchema = new Schema({    
  /*
    id: {
        type: String,
        unique: true,
        required: false
    }, */
  name: {
    type: String,
    required: true
  },
  abstract: {
    type: String,
    required: true
  },
  keywords: [{
    type: String
  }],
  authors: [{
    type: String, // TODO modify this to allow for identifiers (e.g. ORCID)
    required: true
  }]
});

scholarlyArticleSchema.plugin(AutoIncrement, {
  // eslint-disable-next-line camelcase
  inc_field: 'id'
});

const ScholarlyArticle = mongoose.model('ScholarlyArticle', scholarlyArticleSchema);

/**
 * @function
 * @name create
 * @param {Object} study - object to be created
 * @returns Object - the created study
 */
async function create(article) {
  const createdArticle = await ScholarlyArticle.create(article);
  return createdArticle;
}

async function retrieveOne(name) {
  const article = await ScholarlyArticle.findOne({ name });
  return article;
}

async function retrieveAll(criteria =  null) {
  const articles = await ScholarlyArticle.find(criteria);
  return articles;
}

function update() {}

function destroy() {}

module.exports = exports = {
  ScholarlyArticle,
  create,
  retrieveOne,
  retrieveAll,
  update,
  destroy
};