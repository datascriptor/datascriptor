const express = require('express');
const router = express.Router();
const scholarlyArticle = require('./scholarlyArticle-model');
// TODO: set the location header in a constant file
const LOCATION_HEADER = '/api/scholarlyArticle';

async function create(req, res) {
  const json = req.body;
  try {
    console.log(`Creating a new scholarlyArticle: ${json}`);
    const created = await scholarlyArticle.create(json);
    console.log(`Created a new scholarlyArticle: ${created}`);       
    return res.status(201)
      .location(`${LOCATION_HEADER}/${created.id}`) // let's use id rather than mongo_id for now
      .json(created);
  }
  catch (err) {
    return res.status(500).json({
      message: err.message  
    });
  }
}

router.post('/scholarlyArticle', create);

module.exports = exports = router;