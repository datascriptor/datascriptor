const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { JWT_SECRET_KEY, RESET_JWT_SECRET_KEY } = require('../../config');
const { JSON_WEB_TOKEN_ERROR } = require('../utils/errors');
const { isOrcid } = require('../utils/funcs');

const userSchema = mongoose.Schema({
  // the unique identifier must be either an email (local auth)
  // or an ORCiD id (oauth via orcid)
  identifier: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: value => {
      // const isorcid = isOrcid(value);
      const isorcid = true;
      console.log(`isOrcid = ${isorcid}, ${value}`);
      if (isorcid) {
        return true;
      }
      if (validator.isEmail(value)) {
        return true;
      }
      throw new Error({
        message: 'Invalid Identifier: must be email address or orcID'
      });
    }
  },
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    trim: true
  },
  email: {
    type: String,
    required: false,
    index: { unique: true, sparse: true },
    lowercase: true,
    validate: value => {
      if (!validator.isEmail(value)) {
        throw new Error({
          message: 'Invalid Email address'
        });
      }
    }
  },
  orcid: {
    type: String,
    required: false,
    index: { unique: true, sparse: true },
    // validate: isOrcid
  },
  password: {
    type: String,
    // password is required only for local authentication
    required: function pwIsRequired() {
      console.log(this); 
      return validator.isEmail(this.identifier);
    },
    minLength: 8,
    select: false
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false
  },
  verified: {
    type: Boolean,
    required: true,
    default: false
  },
  tokens: [{
    token: {
      type: String,
      required: true
    },
    issuer: {
      type: String,
      required: true,
      default:'datascriptor'
    },
    scope: {
      type: String,
      required: true,
      default: 'authentication'
    }
  }],
  resetToken: {
    type: String
  }
});

userSchema.pre('save', async function hashPassword(next){
  // Hash the password before saving the user model
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

userSchema.methods.generateResetToken = async function generateResetToken() {
  const token = jwt.sign({
    identifier: this.identifier
  }, RESET_JWT_SECRET_KEY, {
    expiresIn: '1h'
  });
  this.resetToken = token;
  await this.save();
  return token;
};

userSchema.methods.generateAuthToken = async function generateAuthToken() {
  // TODO: reuse that i if there is already a valid token
  // Generate an auth token for the user
  const user = this;
  const token = jwt.sign({
    identifier: user.identifier,
    isAdmin: user.isAdmin
  }, JWT_SECRET_KEY);
  user.tokens = user.tokens.concat({token});
  await user.save();
  return token;
};

/**
 * @function
 * @name findByCredentials
 * @description Update password.
 * @param {String} password
 */
userSchema.methods.patchPassword = async function patchPassword(password) {
  this.password = password;
  this.tokens = [];
  delete this.resetToken;
  await this.save();
};

const User = mongoose.model('User', userSchema);

/**
 * @function
 * @name findByCredentials
 * @description Search for a user by email and password.
 * @param {String} email
 * @param {String} password
 * @returns User
 */
async function retrieveByCredentials(email, password) {
  // Search for a user by email and password.
  const user = await User.findOne({ identifier: email } ).select('+password');
  if (!user) {
    throw new Error({ error: 'Invalid login credentials' });
  }
  const isPasswordMatch = await bcrypt.compare(password, user.password);
  if (!isPasswordMatch) {
    throw new Error({ error: 'Invalid login credentials' });
  }
  delete user.password;
  return user;
}

async function retrieveAndVerifyToken(token) {
  const data = jwt.verify(token, JWT_SECRET_KEY);
  const user = await User.findOne({ 
    identifier: data.identifier,
    'tokens.token': token,
    // 'tokens.issuer': 'datascriptor',
    'tokens.scope': 'authentication'
  });
  if (!user) {
    throw new Error({ 
      name: JSON_WEB_TOKEN_ERROR,
      message: 'Invalid or malformed token.'
    });
  }
  return user;
}

/**
 * @function
 * @name create
 * @param {Object} study - object to be created
 * @returns Object - the created study
 */
async function create(userObj) {
  userObj.identifier = userObj.email || userObj.orcid;
  const createdUser = await User.create(userObj);
  return createdUser;
}

async function updatePassword(email, newPassword) {
  const user = await User.findOne({ identifier: email }).select('+password');
  await user.patchPassword(newPassword);
  return user;
}

async function retrieveOne(identifier) {
  const user = await User.findOne({ identifier });
  return user;
}

async function retrieveById(id) {
  const user = await User.findById(id);
  return user;
}

async function retrieveAll(criteria =  null) {
  const users = await User.find(criteria);
  return users;
}

module.exports = exports = {
  User,
  create,
  retrieveById,
  retrieveOne,
  retrieveAll,
  retrieveByCredentials,
  retrieveAndVerifyToken,
  updatePassword
};
