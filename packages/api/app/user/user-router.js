const express = require('express');
const UserModel = require('./user-model');
const jwt = require('jsonwebtoken');
const { JWT_SECRET_KEY, RESET_JWT_SECRET_KEY } = require('../../config');
const errorHandler = require('../utils/errorHandler');
const generateEmail = require('../utils/generateEmail');
const { isAuth, isAdmin } = require('../middlewares/auth');
const router = express.Router();

// const LOCATION_HEADER = '/api/user';

async function create(req, res) {
  try {
    const user = await UserModel.create(req.body);
    // console.log(`user-router.js - User: ${JSON.stringify(user)}`);
    const token = await user.generateAuthToken();
    return res.status(201)
      .location(`${req.headers.host}${req.originalUrl}/${user._id}`)
      .json({
        user,
        token
      });
  }
  catch(err) {
    console.log(`user-router.create() - error caught: ${err}`);
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function findAll(req, res) {
  try {
    const records = await UserModel.retrieveAll();
    return res.json(records);
  } 
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function findOne(req, res) {
  try {
    const record = await UserModel.retrieveById(req.params.id);
    return res.json(record);
  } 
  catch (err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function login(req, res) {
  try {
    const { email, password } = req.body;
    const user = await UserModel.retrieveByCredentials(email, password);
    if (!user) {
      return errorHandler(res).badRequest({
        message: 'Wrong usernname and/or password!'
      });
    }
    const token = await user.generateAuthToken();
        
    res.send({ user, token });
  }
  catch(err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function logout(req, res) {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const { identifier } = jwt.verify(token, JWT_SECRET_KEY);
    const user = await UserModel.retrieveOne(identifier);
    user.tokens = user.tokens.filter(tok => {
      return tok.token !== token;
    });
    user.save();
    res.send({
      message: 'successfully logged out'
    });
  }
  catch(err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function logoutAll(req, res) {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const { identifier } = jwt.verify(token, JWT_SECRET_KEY);
    const user = await UserModel.retrieveOne(identifier);
    user.tokens = [];
    user.save();
    res.send({
      message: 'successfully logged out from all devices.'
    });
  }
  catch(err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

async function resetPwdToken(req, res) {
  try {
    const { body: { identifier } } = req;
    const origin = req.get('origin');
    const user = await UserModel.retrieveOne(identifier);
    const resetToken = await user.generateResetToken();
    let mailOptions = await generateEmail(resetToken, identifier, origin); // replace it with identifier
    console.log(mailOptions);
    res.status(204).send();
  }
  catch(err) {
    return errorHandler(res).handleAndSendResponse(err);
  }
}

/**
Renew the password of the user providing a generated tokens
@param {Object} req - the full express request
@param {Object} res - the prepared express response object
@return {Object} - the filled up response containing the data or an error
*/
async function renewPassword(req, res) {
  const { password, token } = req.body;
  try {
    const { identifier } = jwt.verify(token, RESET_JWT_SECRET_KEY);
    await UserModel.updatePassword(identifier, password);    
    res.status(204).send();
  } catch (err) {
    // console.log(JSON.stringify(err));
    return errorHandler(res).handleAndSendResponse(err);
  }
}

router.post('/user', create);
router.get('/user', isAuth, isAdmin, findAll);
router.get('/user/:id', findOne);
router.post('/login', login);
router.post('/logout', logout);
router.post('/logoutAll', logoutAll);
router.post('/resetPwdToken', resetPwdToken);
router.patch('/user', renewPassword);

module.exports = exports = router;
module.exports.login = login;