const { expect } = require('chai');
const request = require('supertest');
const app = require('../../server');
const testFullFactorialPayload = {
  name: 'Test FF Study Design',
  type: 'fullFactorial',
  description: '`This is meant to be a full factorial design for testing purposes',
  elementParams: {
    agents: ['honey', 'sugar', 'molasses'],
    intensities: [0.1, 1.0, 10.0],
    durations: [7, 14],
    intensityUnit: 'cl/Kg',
    durationUnit: 'day'
  },
  subjectType: 'Human',
  size: 8,
  observationalGroups: [
    {
      name: 'SubjectGroup_0',
      // type: 'Human',  /* this would be a duplication */
      characteristics: [
        {
          name: 'sex',
          value: 'M',
          isQuantitative: false
        },
        {
          name: 'age',
          value: 'teenagers',
          isQuantitative: false
        }
      ]
    },
    {
      name: 'SubjectGroup_2',
      // type: 'Human',  /* this would be a duplication */
      characteristics: [
        {
          name: 'sex',
          value: 'F',
          isQuantitative: false
        },
        {
          name: 'age',
          value: 'teenagers',
          isQuantitative: false
        }
      ]
    }
  ]
};

const testObservationalVariablesPayload = {
  subjectType: 'organism',
  observationalFactors: [
    { name: 'species', values: ['chimpanzee', 'mandingus', 'homo sapiens' ], isQuantitative: false },
    { name: 'sex', values: ['M', 'F' ], isQuantitative: false },
  ],
  size: 10
};

describe('studyDesign - router', function() {

  describe('POST /studyDesign/computeArms', function() {
    it('should generate a full factorial design from element parameters', function(done) {
      request(app)
        .post('/api/studyDesign/computeArms')
        .send(testFullFactorialPayload)
        .expect('Content-Type', /json/)
        .expect(200)
        .then(res => {
          const payload = res.body;
          const { name, subjectType, size,
            observationalGroups, elementParams: {
              agents, intensities, durations
            } } = testFullFactorialPayload;
          const treatmentCombinations = agents.length * intensities.length * durations.length;
          expect(payload.name).to.equal(name);
          expect(payload.subjectType).to.equal(subjectType);
          expect(payload).to.have.property('arms');
          expect(payload).to.have.property('elements');
          expect(payload).to.have.property('events');
          expect(payload.arms).to.have.length(
            treatmentCombinations * observationalGroups.length
          );
          for (const [ix, arm] of payload.arms.entries()) {
            // console.log(JSON.stringify(arm));
            expect(arm.observationalFactors).to.eql(
              observationalGroups[Math.floor(ix/treatmentCombinations)].characteristics
            );
            // expect(arm.subjectType).to.equal(subjectType);
            expect(arm.size).to.equal(size);
            expect(arm).to.have.property('epochs');
          }
          done();
        })
        .catch(done);
    });
  });

  describe('POST /studyDesign/computeSubjectGroups', function() {
    it('should return an array of subject groups', function(done) {
      request(app)
        .post('/api/studyDesign/computeSubjectGroups')
        .send(testObservationalVariablesPayload)
      // .send(testFullFactorialPayload)
        .expect('Content-Type', /json/)
        .expect(200)
        .then(res => {
          const subjectGroups = res.body;
          const {
            observationalFactors: [ species, sex ]
          } = testObservationalVariablesPayload;
          expect(subjectGroups).to.have.length(
            species.values.length * sex.values.length
          );
          done();
        })
        .catch(done);

    });
  });

});

