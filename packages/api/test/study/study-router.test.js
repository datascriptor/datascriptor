const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../server');
const { Study } = require('../../app/study/study-model');
const { create: userCreate } = require('../../app/user/user-model');
const testStudy = require('../fixtures/lite/study');
const { testUser, loginUser } = require('../utils-test/auth');

Study.counterReset('mongo_id', err => {
  if (err) {
    console.log(`Error thrown while resetting counter: ${err}`);
  }
});

describe('study - router', function() {

  const auth = {};

  before(async function() {
    await userCreate(testUser);
    const res = await Study.remove({});
    console.log(`study-router.test.js - setUp method: #${res.deletedCount} items were removed from the collection`);
    await loginUser(auth, testUser);
    console.log('study - router. Before function done.');
    console.log(`study - router. before() hook. Token: ${auth.token}.`);
  });

  describe('POST /api/study', function() {
    it('should return unauthorized if the user is not authenticated', function() {
      request(app)
        .post('/api/study')
        .send(testStudy)
        .expect('Content-Type', /json/)
        .expect(401);
    });

    it('should create a new instance of a study', function(done) {
      console.log('study - router. This should create a new instance of a study.');
      console.log(`study - router. POST /api/study Token: ${auth.token}.`);
      request(app)
        .post('/api/study')
        .auth(auth.token, { type: 'bearer' })
        .send(testStudy)
        .expect('Content-Type', /json/)
        .expect(201)
        .then(res => {
          const payload = res.body;
          // console.log(JSON.stringify(payload));
          const { _id } = payload;
          expect(res.header).to.have.property('location');
          expect(res.header.location).to.include(`/api/study/${_id}`);
          expect(payload.mongo_id).to.equal(1);
          expect(payload.name).to.equal(testStudy.name);
          expect(payload).to.have.nested.property('design.arms');
          for (const [index, arm] of payload.design.arms.selected.entries()) {
            expect(arm.name).to.equal(testStudy.design.arms.selected[index].name);
            expect(arm.subjectType).to.equal(testStudy.design.arms.selected[index].subjectType);
            expect(arm.size).to.equal(testStudy.design.arms.selected[index].size);
          }
          done();
        })
        .catch(err => {
          console.log(err);
          done(err);
        });
    });
  });

  describe('GET /api/study', function() {
    it('should return a list with one item', function(done) {
      request(app)
        .get('/api/study')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(res => {
          const payload = res.body;
          expect(payload).to.be.an.instanceOf(Array);
          expect(payload).to.have.lengthOf(1);
          expect(payload[0]._id).not.to.be.empty;
          expect(payload[0].mongo_id).to.equal(1);
          expect(payload[0].name).to.equal(testStudy.name);
          done();
        })
        .catch(done);
    });
  });

  describe('GET /api/study/:id', function() {
    let studyToFind = null;

    before(async function() {
      studyToFind = (await Study.findOne({
        name: testStudy.name
      })).toObject();
    });

    it('should find the existing study', function(done) {
      request(app)
        .get(`/api/study/${studyToFind._id}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .then(res => {
          const payload = res.body;
          // expect(payload._id).to.equal(studyToFind._id);
          expect(payload.name).to.equal(studyToFind.name);
          expect(payload.description).to.equal(studyToFind.description);
          // expect(payload.design).to.eql(studyToFind.design); /* this strict equality check seems problematic */
          done();
        })
        .catch(done);
    });
  });

  describe('PUT /api/study/:id', function() {
    let studyToUpdate = null;
    const updatedDescription = 'This is the new study description. You can read many interesting things here.';

    before(async function() {
      studyToUpdate = (await Study.findOne({
        name: testStudy.name
      })).toObject();
      studyToUpdate.description = updatedDescription;
    });

    it('should update the existing study', function(done) {
      request(app)
        .put(`/api/study/${studyToUpdate._id}`)
        .auth(auth.token, { type: 'bearer' })
        .send(studyToUpdate)
        .expect('Content-Type', /json/)
        .expect(200)
        .then(res => {
          const payload = res.body;
          // expect(payload._id).to.equal(studyToUpdate._id.toString());
          expect(payload.name).to.equal(studyToUpdate.name);
          // expect(payload.design).to.eql(studyToUpdate.design); /* this strict equality seems problematic */
          console.log(payload.description);
          expect(payload.description).to.equal(updatedDescription);
          done();
        })
        .catch(done);
    });
  });

  describe('DELETE /api/study/:id', function() {
    let studyToDelete = null;

    before(async function() {
      studyToDelete = await Study.findOne({
        name: testStudy.name
      });
    });

    it('should delete the existing study', function() {
      request(app)
        .delete(`/api/study/${studyToDelete._id}`)
        .auth(auth.token, { type: 'bearer' })
        .expect(204);
    });


  });

  /* FIXME this method must be updated
    describe('POST /api/study/convert', function() {
        it('should return a json with the text describing the dataset', function(done) {
            request(app)
                .post('/api/study/convert')
                .send(testStudy)
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    const payload = res.body;
                    expect(payload).to.have.property('arms');
                    expect(payload.arms).to.have.property('control');
                    expect(payload.arms).to.have.property('treatment');
                    done();
                })
                .catch(done);
        });
    }); */

  after(async function() {
    const res = await Study.remove({});
    console.log(`study-router.test.js - tearDown method: #${res.deletedCount} items were removed from the Study collection`);
  });

});