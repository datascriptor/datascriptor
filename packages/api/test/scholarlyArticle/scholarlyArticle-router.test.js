const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../server');
const { ScholarlyArticle } = require('../../app/scholarlyArticle/scholarlyArticle-model');
const testScholarlyArticle = require('../fixtures/lite/scholarlyArticle');

ScholarlyArticle.counterReset('id', err => {
  if (err) {
    console.log(`Error thrown while resetting counter: ${err}`);
  }
});

describe('scholarlyArticle - router', function() {

  before(async function() {
    const res = await ScholarlyArticle.remove({});
    console.log(`study-router.test.js - setUp method: #${res.deletedCount} items were removed from the collection`);
  });

  describe('POST /api/scholarlyArticle', function() {
    it('should create a new instance of a scholarlyArticle', function(done) {
      const newId = 1;
      request(app)
        .post('/api/scholarlyArticle')
        .send(testScholarlyArticle)
        .expect('Content-Type', /json/)
        .expect(201)
        .expect('Location', `/api/scholarlyArticle/${newId}`)
        .then(res => {
          const payload = res.body;
          expect(payload.name).to.equal(testScholarlyArticle.name);
          expect(payload.abstract).to.equal(testScholarlyArticle.abstract);
          expect(payload.keywords).to.eql(testScholarlyArticle.keywords);
          expect(payload.authors).to.eql(testScholarlyArticle.authors);
          done();
        })
        .catch(done);
    });
  });

});