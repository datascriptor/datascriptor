const request = require('supertest');
const app = require('../../server');

const testUser = {
  name: 'test',
  email: 'test@test.org',
  password: 'IAmATest77!'
};

const testAdmin = {
  name: 'admin',
  email: 'admin@admin.org',
  password: 'indovinalagonzo33!',
  isAdmin: true
};

function loginUser(auth, user) {
  return request(app)
    .post('/api/login')
    .send({
      email: user.email,
      password: user.password
    })
    .expect(200)
    .then(res => {
      console.log(`Token: ${res.body.token}`);
      auth.token = res.body.token;
      auth.user = res.body.user;
    })
    .catch(err => {
      console.log(`loginUser - Error caught: ${err}`);
    });
}

module.exports = exports = {
  testUser,
  testAdmin,
  loginUser
};