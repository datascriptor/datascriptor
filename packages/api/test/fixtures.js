const { testUser } = require('./utils/auth');
const { User } = require('../app/user/user-model');

exports.mochaHooks = {
  async beforeAll() {
    console.log('Mocha global setup...');
    await User.create(testUser);
  },

  async afterAll() {
    await User.remove({});
  }
};