const { expect } = require('chai');
const request = require('supertest');
const app = require('../../server');
const { User } = require('../../app/user/user-model');
const { testAdmin, testUser, loginUser } = require('../utils-test/auth');

/*
const testUser = {
  name: 'test-user',
  email: 'test-user@test.org',
  identifier: 'test-user@test.org',
  password: 'IAmATest4You!'
}; */

describe('user -  router', function() {

  before(async function() {
    const res = await User.remove({});
    console.log(`study-router.test.js - setUp method: #${res.deletedCount} items were removed from the collection`);
    const found = await User.find({});
    console.log(`user-router.test - before hook. Models found (should be empty): ${found}`);
    // await userCreate(testAdmin);
  });

  after(async function() {
    const res = await User.remove({});
    console.log(`study-router.test.js - setUp method: #${res.deletedCount} items were removed from the collection`);
  });

  describe('POST /user', function() {
    it('creates a new user with the given credentials', function(done) {
      request(app)
        .post('/api/user')
        .send(testUser)
        .expect('Content-Type', /json/)
        .expect(201)
        .then(res => {
          const payload = res.body;
          expect(payload).to.haveOwnProperty('user');
          expect(payload).to.haveOwnProperty('token');
          done();
        })
        .catch(done);
    });

    it('creates a new user with admin role', function(done) {
      request(app)
        .post('/api/user')
        .send(testAdmin)
        .expect('Content-Type', /json/)
        .expect(201)
        .then(res => {
          const { user, token } = res.body;
          expect(user.isAdmin).to.be.true;
          expect(token).not.to.be.empty;
          done();
        })
        .catch(err => {
          console.log(`POST /user - admin creation failed with error: ${err}`);
          done(err);
        });
    });


  });

  describe('GET /user and GET /user/:id', function() {
    const adminAuth = {};
    const userAuth = {};
    

    before(async function() {
      await Promise.all([
        loginUser(adminAuth, testAdmin),
        loginUser(userAuth, testUser)
      ]);
      console.log('study - router. Before function done.');
      console.log(`study - router. before() hook. Admin Token: ${adminAuth.token}.`);
      console.log(`study - router. before() hook. User Token: ${userAuth.token}.`);
    });

    it('successfully retrieves all the users (GET /user)', function(done) {
      request(app)
        .get('/api/user')
        .auth(adminAuth.token, { type: 'bearer' })
        .expect(200)
        .then(res => {
          const payload = res.body;
          // there are at least two users in the array
          expect(payload).to.be.an('array').that.has.length.greaterThan(1);
          const adminCount = payload.map(usr => usr.isAdmin).length;
          expect(adminCount).to.be.greaterThan(0);
          done();
        })
        .catch(done);
    });

    it('fails to retrieves all the users if auth is not admin (GET /user)', function() {
      request(app)
        .get('/api/user')
        .auth(userAuth.token, { type: 'bearer' })
        .expect(403);
    });

    it('successfully retrieves one user  (GET /user/:id)', function(done) {
      request(app)
        .get(`/api/user/${userAuth.user._id}`)
        .auth(adminAuth.token, { type: 'bearer' })
        .expect(200)
        .then(res => {
          const payload = res.body;
          // there are at least two users in the array
          const {
            password,
            ...expectedUserWithoutPwd
          } = userAuth.user;
          expect(payload).to.eql(expectedUserWithoutPwd);
          done();
        })
        .catch(done);
    });

  });

  describe('POST /login', function() {
    it('successfully logs in an existing user', function(done) {
      request(app)
        .post('/api/login')
        .send({
          email: testUser.email,
          password: testUser.password
        })
        .expect(200)
        .then(res => {
          const payload = res.body;
          expect(payload).to.haveOwnProperty('user');
          expect(payload).to.haveOwnProperty('token');
          done();
        })
        .catch(done);
    });

    /* TODO implement 400 error if user does not exist */
  });

  describe('POST /logout', function() {

    let authToken;

    beforeEach(function(done) {
      request(app)
        .post('/api/login')
        .send({
          email: testUser.email,
          password: testUser.password
        })
        .then(res => {
          authToken = res.body.token;
          done();
        })
        .catch(done);
    });

    it('successfully logs out an existing user', function(done) {
      request(app)
        .post('/api/logout')
        .set('Authorization', `Bearer ${authToken}`)
        .expect(200)
        .then(res => {
          const payload = res.body;
          expect(payload).to.haveOwnProperty('message');
          done();
        })
        .catch(done);
    });
  });

  describe('POST /logoutAll', function() {

    /* TODO refactor this to avoid copy/paste of code */
    let authToken;

    beforeEach(function(done) {
      request(app)
        .post('/api/login')
        .send({
          email: testUser.email,
          password: testUser.password
        })
        .then(res => {
          authToken = res.body.token;
          done();
        })
        .catch(done);
    });

    it('successfully logs out an existing user', function(done) {
      request(app)
        .post('/api/logoutAll')
        .set('Authorization', `Bearer ${authToken}`)
        .expect(200)
        .then(res => {
          const payload = res.body;
          expect(payload).to.haveOwnProperty('message');
          done();
        })
        .catch(done);
    });
  });

  describe('POST /resetPwdToken', function() {
    it('sends the reset token in a link via email', function() {
      return request(app)
        .post('/api/resetPwdToken')
        .send({
          identifier: testUser.email
        })
        .expect(204);
    });
  });

  describe('PATCH /user', function() {
    it('updates the password', function() {
      return request(app)
        .patch('/api/user')
        .send({
          password: 'bao1234ciao',
          token: '1234'
        })
        .expect(401);
    });
  });

});
