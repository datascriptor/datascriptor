const { expect } = require('chai');
const { isOrcid } = require('../../app/utils/funcs');

describe('queries', function() {
  it('isOrcid', function() {
    expect(isOrcid('0000-1234-2345-8765')).to.equal(true);
    expect(isOrcid('0000-1234-2345-87655')).to.equal(false);
    expect(isOrcid('0000-1o34-2345-8765')).to.equal(false);
    expect(isOrcid('00000-1234-2345-87655')).to.equal(false);
    expect(isOrcid('0000-0002-8100-6142')).to.equal(true);
  });
});