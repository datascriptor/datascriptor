const { expect } = require('chai');
// const sinon = require('sinon');
const queries = require('../../app/utils/queries');
const { pagination } = require('../../config');
// const StudyModel = require('../../app/study/study-model');

'use strict';

const testParamsObj = Object.freeze({
  text: 'eee',
  skip: 100,
  sort: 'updatedAt DESC',
  limit: 25
});

const testCriteria = {
  skip: testParamsObj.skip,
  sort: testParamsObj.sort,
  limit: testParamsObj.limit,
  filter: {
    $text: { $search: testParamsObj.text }
  },
  _query: testParamsObj
};

// const testParamsString = `text=${testParamsObj.text}&skip=${testParamsObj.skip}&sort=${testParamsObj.sort}&limit=${testParamsObj.limit}`;

describe('queries', function() {
  it('parseSkip', function() {
    expect(queries.parseSkip()).to.equal(pagination.defaultSkip);
    expect(queries.parseSkip(testParamsObj)).to.equal(testParamsObj.skip);
  });
  it('parseLimit', function() {
    expect(queries.parseLimit()).to.equal(pagination.defaultPageSize);
    expect(queries.parseLimit(testParamsObj)).to.equal(testParamsObj.limit);
  });
  it('parseSort', function() {
    expect(queries.parseSort()).to.equal(pagination.defaultSort);
    expect(queries.parseSort(testParamsObj)).to.equal(testParamsObj.sort);
  });
  it('parseFilterCriteria', function() {
    expect(queries.parseFilterCriteria()).to.eql({});
    const filterCriteria = queries.parseFilterCriteria(testParamsObj);
    expect(filterCriteria.$text).to.eql({ $search: testParamsObj.text });
  });
  it('parseParams', function() {
    expect(queries.parseParams()).to.eql({
      filter: {},
      skip: pagination.defaultSkip,
      sort: pagination.defaultSort,
      limit: pagination.defaultPageSize,
      _query: undefined,
    });
    expect(queries.parseParams(testParamsObj)).to.eql(testCriteria);
  });
  describe('composePaginationInfo', function() {


    // let stub;
    const testCount = 200;

    /*
    beforeEach(function() {
      stub = sinon.stub(StudyModel, 'countAll');
      stub.resolves(testCount);
    });

    afterEach(function() {
      stub.restore();
    });
    */

    it('composes the pagination info', function() {
      const paginationInfo = queries.composePaginationInfo({
        host: 'http://localhost:3000',
        path: '/api/study',
        parsedParams: testCriteria,
        totalCount: testCount
      });
      console.log(testCriteria);
      console.log(testParamsObj);
      console.log(paginationInfo);
      expect(paginationInfo.totalCount).to.equal(testCount);
      expect(paginationInfo.pageSize).to.equal(testParamsObj.limit);
      expect(paginationInfo.numPages).to.equal(Math.ceil(testCount/testParamsObj.limit));
      const expectedCurrentPage = Math.ceil(testParamsObj.skip/testParamsObj.limit) + 1;
      expect(paginationInfo.currPage).to.equal(expectedCurrentPage);
      expect(paginationInfo.links).to.have.length(4);
    });

  });

  it('composePaginationHeaders', function() {});
});