module.exports = exports = {
  'env': {
    'mocha': true
  },
  'rules': {
    'func-names': 0                // require function expressions to have a name (off by default)
  }
};