const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../server');
const assayConfigs = require('../fixtures/assay-configs.json');

describe('assayConfig - router', function() {
  describe('GET /api/assayConfig', function() {
    it('should return a list with some item', function(done) {
      request(app)
        .get('/api/assayConfig')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(res => {
          const payload = res.body;
          expect(payload).to.be.an.instanceOf(Array);
          expect(payload).to.have.lengthOf(assayConfigs.length);
          expect(payload).to.eql(assayConfigs);
          done();
        })
        .catch(done);
    });
  });

});