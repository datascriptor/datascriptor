const config = {
  env: 'development',
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || 'someC0mpl3xD€f4ultS3cr3t!',
  RESET_JWT_SECRET_KEY: process.env.RESET_JWT_SECRET_KEY || 'd€f4ultR£s£tS3cr3t!!'
};

config.express = {
  port: process.env.EXPRESS_PORT || 3000,
  ip: '127.0.0.1'
};

config.mongodb = {
  uri: process.env.MONGODB_URI || 'mongodb://localhost:27017/datascriptor',
  port: process.env.MONGODB_PORT || 27017,
  host: process.env.MONGODB_HOST || 'localhost',
  database: 'datascriptor'
};

config.pagination = {
  defaultPageSize: process.env.DEFAULT_PAGE_SIZE || 5,
  defaultSkip: process.env.DEFAULT_SKIP || 0,
  defaultSort: process.env.DEFAULT_SORT || 'createdAt DESC'
};

config.orcid = {
  tokenRequestUrl: 'https://orcid.org/oauth/token',
  clientId: process.env.ORCID_CLIENT_ID || 'APP-WMBRNJSU12GIG0UZ',
  clientSecret: process.env.ORCID_CLIENT_SECRET || '046fe1a7-7162-4300-9aa1-edc47cc523b0',
  redirectUrl: process.env.ORCID_REDIRECT_URL ||  'https://localhost:8443/api/oauth-redirect',
  finalUrl: process.env.ORCID_FINAL_URL ||'https://localhost:8443/#/oauth-redirect'
};

module.exports = exports = config;
