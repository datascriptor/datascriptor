const config = {};

config.pagination = {
  defaultPageSize: process.env.DEFAULT_PAGE_SIZE || 20,
  defaultSkip: process.env.DEFAULT_SKIP || 0,
  defaultSort: process.env.DEFAULT_SORT || 'createdAt DESC'
};

module.exports = exports = config;