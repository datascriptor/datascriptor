const _env = process.env.NODE_ENV || 'development';
// this import is required so that netlify functions find all the dependencies correctly
const config = _env === 'production' ? require('./production') : 
  _env === 'test' ? require('./test') : require('./development');

const PRODUCTION = process.env.NODE_ENV === 'production';

if (PRODUCTION) {
  // for example
  config.express.ip = '0.0.0.0';
}

module.exports = config;