const config = {
  env: 'test',
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || 'defaultSecret',
  RESET_JWT_SECRET_KEY: process.env.RESET_JWT_SECRET_KEY || 'defaultResetSecret'
};

config.express = {
  port: process.env.EXPRESS_PORT || 4000,
  ip: '127.0.0.1'
};
  
config.mongodb = {
  uri: process.env.MONGODB_URI || 'mongodb://localhost:27017/datascriptorTest',
  port: process.env.MONGODB_PORT || 27017,
  host: process.env.MONGODB_HOST || 'localhost',
  database: 'datascriptorTest'
};

config.pagination = {
  defaultPageSize: process.env.DEFAULT_PAGE_SIZE || 20,
  defaultSkip: process.env.DEFAULT_SKIP || 0,
  defaultSort: process.env.DEFAULT_SORT || 'createdAt DESC'
};

module.exports = exports = config;