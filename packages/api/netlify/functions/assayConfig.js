const assayConfigs = require('../../test/fixtures/assay-configs.json');

exports.handler = async function handler() {
  // your server-side functionality
  return {
    statusCode: 200,
    body: JSON.stringify(assayConfigs)
  };
};