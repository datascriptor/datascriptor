const dbConnect = require('../../databaseConnection');
const UserModel = require('../../app/user/user-model');
const generateEmail = require('../../app/utils/generateEmail');
const errorHandler = require('../../app/utils/errorHandler');

exports.handler = async event => {
  try {
    await dbConnect();
    if (event.httpMethod === 'POST') {
      const { identifier } = JSON.parse(event.body);
      const origin = event.headers.origin;
      const user = await UserModel.retrieveOne(identifier);
      const resetToken = await user.generateResetToken();
      let mailOptions = await generateEmail(resetToken, identifier, origin);
      console.log(mailOptions);
      return {
        statusCode: 204
      };
    }
  } catch (err) {
    const { statusCode, handledError } = errorHandler.handle(err);
    return {
      statusCode,
      body: JSON.stringify(handledError)
    };
  }
};