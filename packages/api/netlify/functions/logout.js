const dbConnect = require('../../databaseConnection');
const UserModel = require('../../app/user/user-model');
const { JWT_SECRET_KEY } = require('../../config');
const jwt = require('jsonwebtoken');
const errorHandler = require('../../app/utils/errorHandler');

exports.handler = async (event) => {
  try {
    await dbConnect();
    if (event.httpMethod === 'POST') {
      const token = event.header.authorization.replace('Bearer ', '');
      const { identifier } = jwt.verify(token, JWT_SECRET_KEY);
      const user = await UserModel.retrieveOne(identifier);
      user.tokens = user.tokens.filter(tok => {
        return tok.token !== token;
      });
      user.save();
      return {
        statusCode: 200,
        body: JSON.stringify({ message: 'successfully logged out' })
      };
    }
  } catch (err) {
    const { statusCode, handledError } = errorHandler.handle(err);
    return {
      statusCode,
      body: JSON.stringify(handledError)
    };
  }
};