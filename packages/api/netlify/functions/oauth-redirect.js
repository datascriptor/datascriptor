const axios = require('axios');
const querystring = require('querystring');
const { retrieveOne, create } = require('../../app/user/user-model');
const dbConnect = require('../../databaseConnection');
const config = require('../../config');

const orcidConf = config.orcid;

exports.handler = async (event) => {
  console.log(orcidConf);
  const { queryStringParameters: { code } } = event;
  let data = querystring.stringify({
    client_id: orcidConf.clientId,
    client_secret: orcidConf.clientSecret,
    grant_type: 'authorization_code',
    redirect_uri: orcidConf.redirectUrl,
    code
  });
  console.log(data);
  try {
    await dbConnect();
    const response = await axios({
      method: 'POST',
      url: orcidConf.tokenRequestUrl,
      headers: {
        accept: 'application/json',
        // Authorization: `Bearer ${CLIENT_SECRET}`,
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      },
      data
    });
    const { data: { 
      orcid,
      access_token: accessToken,
      refresh_token: refreshToken,
      name
    } } = response;
    let user = await retrieveOne(orcid);
    if (!user) {
      user = await create({ name, orcid });
      console.log(user);
    }
    /*
    store the two orcid tokens
    TODO do I need a 'datascriptor' auth token as well?
    login the user
    get the jwt and username
    return the redirect URL
    user.tokens.push([
      { token: accessToken, scope: 'authentication', issuer: 'orcid' },
      { token: refreshToken, scope: 'refresh', issuer: 'orcid' }
    ]);
    await user.save();
    */
    let token = await user.generateAuthToken();
    const urlString = querystring.stringify({
      name: user.name,
      identifier: user.identifier,
      token
    });
    return {
      statusCode: 301,
      headers: {
        location: `${orcidConf.finalUrl}?${urlString}`
      }
    };
  }
  catch(e) {
    console.log(e);
    return {
      statusCode: 400,
      body: JSON.stringify(e)
    };
  }
  
  
};