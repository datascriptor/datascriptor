const middy = require('@middy/core');
const jsonBodyParser = require('@middy/http-json-body-parser');
// const dbConnect = require('../../databaseConnection');
const StudyModel = require('../../app/study/study-model');
const { getAllWithPagination } = require('../../app/study/study-service');
const errorHandler = require('../../app/utils/errorHandler');
const authMiddleware = require('../middlewares/auth');
const dbConnectMiddleware = require('../middlewares/dbConnect');

const ALLOWED_METHODS = ['GET', 'POST', 'PUT', 'DELETE'];

const studyResourceHandler = async event => {
  try {
    // const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '');
    console.log(`event: ${JSON.stringify(event)}`);
    const path = event.path.replace(/api\/[^/]+/, '');
    const segments = path.split('/').filter(e => e);
    console.log(`Segments: ${segments}`);
    // await dbConnect();
    switch(event.httpMethod) {
    case 'OPTIONS': {
      return {
        statusCode: 200,
        headers: {
          allow: ALLOWED_METHODS.join(',')
        },
        body: JSON.stringify(ALLOWED_METHODS.join(','))
      };
    }
    case 'POST': {
      const { body: payload, user: { name: username }  } = event;
      const study = await StudyModel.create({ ...payload, owner: username });
      return {
        statusCode: 201,
        headers: {
          location: `${event.headers.host}${event.path}/${study._id}`
        },
        body: JSON.stringify(study)
      };
    }
    case 'GET': {
      if (segments.length === 0) {
        console.log(event);
        const {
          statusCode, headers, resPayload
        } = await getAllWithPagination({
          host: event.headers.host,
          path: event.path,
          queryParams: event.queryStringParameters
        });
        return {
          statusCode, headers, body: JSON.stringify(resPayload)
        };
      } else if (segments.length === 1) {
        const resPayload = await StudyModel.retrieveOne(segments[0]);
        return {
          statusCode: 200,
          body: JSON.stringify(resPayload)
        };
      } else {
        return {
          statusCode: 400,
          body: JSON.stringify({
            message: 'too many segments in GET request, must be either /api/study or /api/study/{_id}'
          })
        };
      }
    }
    case 'PUT': {
      // TODO check user is owner of the resource
      console.log(`PUT: ${JSON.stringify(event.body)}`);
      const { user: { name: username }, body: reqPayload } = event;
      console.log(`Username: ${username}`);
      const resPayload = await StudyModel.update({
        ...reqPayload,
        owner: username
      });
      console.log(`resPayload: ${JSON.stringify(resPayload)}`);
      return {
        statusCode: 200,
        body: JSON.stringify(resPayload)
      };
    }
    case 'DELETE': {
      // TODO implement soft delete
      if (segments.length === 1) {
        const id = segments[0];
        const toDelete = await StudyModel.retrieveOne(id);
        if (toDelete) {  // TODO check user is authenticated and authorised
          await StudyModel.deleteOne(id);
        }
      } else {
        return {
          statusCode: 400,
          body: JSON.stringify({
            message: 'too many segments in DELETE request, must be /api/study/{_id}'
          })
        };
      }
      return {
        statusCode: 204
      };
    }
    }
  } catch (err) {
    console.log(`Error caught: ${err.message}`);
    console.log(`Error caught: ${err.toString()}`);
    console.log(`Error caught: ${Object.keys(err)}`);
    const handler = errorHandler();
    const { statusCode, handledError } = handler.handle(err);
    console.log(JSON.stringify(handledError));
    return {
      statusCode,
      body: JSON.stringify(handledError)
    };
  }
};

const handler = middy(studyResourceHandler)
  .use(dbConnectMiddleware())
  .use(authMiddleware())
  .use(jsonBodyParser());

module.exports = exports = {
  handler
};