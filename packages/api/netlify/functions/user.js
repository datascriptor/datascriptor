// import the login function
const dbConnect = require('../../databaseConnection');
const UserModel = require('../../app/user/user-model');
const errorHandler = require('../../app/utils/errorHandler');
const jwt = require('jsonwebtoken');
const { RESET_JWT_SECRET_KEY } = require('../../config');

exports.handler = async (event) => {
  try {
    await dbConnect();
    // FIXME we need to secure this endpoint: only admins can access the list of all the users
    // only admins or the same user can access the single user 
    console.log(`event: ${JSON.stringify(event)}`);
    if (event.httpMethod === 'GET') {
      const path = event.path.replace(/api\/[^/]+/, '');
      const segments = path.split('/').filter(e => e);
      console.log(`Segments: ${segments}`);
      let resPayload = null, statusCode = 200;
      if (segments.length === 0) {
        resPayload = await UserModel.retrieveAll();
      } else if (segments.length === 1) {
        resPayload = await UserModel.retrieveOne(segments[0]);
      } else {
        statusCode = 400;
        resPayload = 'too many segments in GET request, must be either /api/study or /api/study/{_id}';
      }
      return {
        statusCode,
        body: JSON.stringify(resPayload)
      };
    }
    if (event.httpMethod === 'POST') {
      const user = await UserModel.create(JSON.parse(event.body));
      // console.log(`user-router.js - User: ${JSON.stringify(user)}`);
      const token = await user.generateAuthToken();
      return {
        headers: {
          location: `${event.headers.host}${event.path}/${user._id}`
        },
        statusCode: 201,
        body: JSON.stringify({
          user,
          token
        })
      };
    }
    if (event.httpMethod === 'PATCH') {
      const { password, token } = JSON.parse(event.body);
      const { identifier } = jwt.verify(token, RESET_JWT_SECRET_KEY);
      const user = await UserModel.retrieveOne(identifier);
      await user.patchPassword(password);
      return {
        statusCode: 204
      };
    }
  }
  catch(err) {
    const handler = errorHandler();
    const { statusCode, handledError } = handler.handle(err);
    return {
      statusCode,
      body: JSON.stringify(handledError)
    };
  }
};