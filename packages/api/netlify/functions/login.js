// import the login function
const dbConnect = require('../../databaseConnection');
const UserModel = require('../../app/user/user-model');
const errorHandler = require('../../app/utils/errorHandler');

exports.handler = async (event) => {
  try {
    await dbConnect();
    const { email, password } = JSON.parse(event.body);
    console.log(email);
    const user = await UserModel.retrieveByCredentials(email, password);
    const token = await user.generateAuthToken();
    return {
      statusCode: 200,
      body: JSON.stringify({
        user,
        token
      })
    };
  }
  catch(err) {
    const { statusCode, handledError } = errorHandler.handle(err);
    return {
      statusCode,
      body: JSON.stringify(handledError)
    };
  }
};