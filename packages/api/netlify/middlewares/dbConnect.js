const dbConnect = require('../../databaseConnection');

module.exports = () => {
  const dbConnectBefore = async request => {
    try {
      await dbConnect();
    } catch (err) {
      throw new Error({
        message: 'Database connection failed'
      });
    }
  };

  const dbConnectAfter = async request => {};

  return {
    before: dbConnectBefore,
    after: dbConnectAfter
  };
};
