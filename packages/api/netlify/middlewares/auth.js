const { retrieveAndVerifyToken } = require('../../app/user/user-model');
const { JSON_WEB_TOKEN_ERROR } = require('../../app/utils/errors');

const defaults = {};

const SAFE_METHODS = new Set(['GET', 'HEAD', 'OPTIONS', 'TRACE']);

module.exports = (opts = {}) => {
  const options = { ...defaults, ...opts };

  const checkAuthBefore = async request => {
    // might read options
    const { event } = request;
    // console.log(`auth.checkAuthBefore: event = ${JSON.stringify(event)}`);
    if (SAFE_METHODS.has(event.httpMethod)) {
      console.log(`Method ${event.httpMethod} is safe`);
      return;
    }
    else {
      try {
        const token = event.headers.authorization.replace('Bearer ', '');
        console.log(token);
        const user = await retrieveAndVerifyToken(token);
        console.log(`User authenticated: ${user}`);
        event.user = user;
        event.token = token;
        return;
      } catch(err) {
        console.log(`Error caught during bearer auth: ${err}`);
        request.error = {
          name: JSON_WEB_TOKEN_ERROR,
          message: 'Token was malformed, missing, or could not be verified'
        };
        throw new Error(request.error);
      }
    }
  };
  const checkAuthAfter = async () => {
    // might read options 
  };
  const checkAuthOnError = async request => {
    const { response, error } = request;
    console.log(`auth.checkAuthOnError: request: ${JSON.stringify(request)}`);
    console.log(`auth.checkAuthOnError: response: ${JSON.stringify(response)}`);
    console.log(`auth.checkAuthOnError: error: ${JSON.stringify(error)}`);
    if (error.name === JSON_WEB_TOKEN_ERROR) {
      request.response = {
        statusCode: 401,
        body: JSON.stringify({
          name: JSON_WEB_TOKEN_ERROR,
          message: 'Unauthorized Request. Token was malformed, missing, or could not be verified.'
        })
      };
    }
  };
  
  return {
    // Having descriptive function names will allow for easier tracking of perormance bottlenecks using @middy/core/profiler
    before: checkAuthBefore,
    after: checkAuthAfter,
    onError: checkAuthOnError
  };
};