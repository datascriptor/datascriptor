// This middleware handles access to the User resource

module.exports = () => {
  // anyone can POST to /user
  // only same user or admin can GET to /user/:id
  // only same user or admin can PATCH or PUT to /user/:id
  // only admin can  GET to /user
  const userInfoAuthBefore = async request => {
    try {
      //
    } catch (err) {
      throw new Error({
        message: 'Database connection failed'
      });
    }
  };

  const userInfoAuthAfter = async request => {};

  return {
    before: userInfoAuthBefore,
    after: userInfoAuthAfter
  };
};