/**
 * This project structure was inspired by https://github.com/focusaurus/express_code_structure
 */
const express = require('express');
require('dotenv').config({ path: '../../.env' });

// create database connection
const dbConnect = require('./databaseConnection');
const app = express();

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// Wire/Load all the routes here
app.use('/api', require('./app/user/user-router'));
app.use('/api', require('./app/scholarlyArticle/scholarlyArticle-router'));
app.use('/api', require('./app/study/study-router'));
app.use('/api', require('./app/studyDesign/studyDesign-router'));
app.use('/api', require('./app/assayConfig/assayConfig-router'));
// app.set('useStudyExpanded', false);

dbConnect();

module.exports = exports = app;