const interventionTypes = require('../resources/intervention-types');

const CONSTANTS = {
    DATASCRIPTOR_HOME_URL: 'https://datascriptor.org',
    RUN_IN: 'run-in',
    SCREEN: 'screen',
    FOLLOW_UP: 'follow-up',
    WASHOUT: 'washout',
    INTERVENTIONS: {
        CHEMICAL: 'chemical intervention',
        BEHAVIOURAL: 'behavioural intervention',
        SURGICAL: 'surgical intervention',
        RADIOLOGICAL: 'radiological intervention',
        DIETARY: 'dietary intervention'
    },
    ORDINALS: ['first', 'second', 'third', 'fourth', 'fifth', 
    'sixth', 'seventh', 'eigth', 'ninth', 'tenth'],
    DEFAULT_SCHOLARLY_ARTICLE_METADATA: {
        name: '',
        authors: [],
        abstract: '',
        keywords: [],
        identifier: '',
        pageStart: null,
        pageEnd: null,
        creativeWorkStatus: '',
        publisher: '',
        dateCreated: null,
        dateModified: null,
        datePublished: null,
        license: '',
        url: '',
        sameAs: '',
        inLanguage: '',
        wordCount: null,
        about: '',
        backstory: '',
        articleBody: ''
    },
    CREATIVE_WORK_DEFAULT_STATUSES: ['INCOMPLETE', 'DRAFT', 'PUBLISHED', 'OBSOLETE'],
    FULL_FACTORIAL_DESIGN_TYPE: {
        term: 'full factorial design',
        id:  'STATO:0000270',
        iri: 'http://purl.obolibrary.org/obo/STATO_0000270',
        label: 'Study subjects receive a single treatment',
        value: 'fullFactorial' 
    },
    CROSSOVER_DESIGN_TYPE: {
        term: 'crossover design',
        id:  'OBI:0500003',
        iri: 'http://purl.obolibrary.org/obo/OBI_0500003',
        label: 'Study subjects receive repeated treatments',
        value: 'crossover'
    },
    PARALLEL_GROUP_DESIGN_TYPE: { // this is analogous to CROSSOVER
        term: 'parallel group design',
        id: 'OBI:0500006',
        iri: 'http://purl.obolibrary.org/obo/OBI_0500006',
        label: 'Study subjects receive repeated treatments',
        value: 'parallel'
    },
    OBSERVATIONAL_DESIGN_TYPE: {
        term: 'observational design',
        id: 'NCIT:C147138',
        iri: 'http://purl.obolibrary.org/obo/NCIT_C147138',
        label: 'Study subjects do not receive treatments',
        value: 'observational'
    },
    DEFAULT_SUBJECT_SIZE: 10,
    DEFAULT_SUBJECT_TYPE: {
        term: 'Homo sapiens',
        id: 'NCBITaxon:9606',
        iri: 'http://purl.obolibrary.org/obo/NCBITaxon_9606'
    },
    INTERVENTION_TYPES: interventionTypes
};

module.exports = exports = CONSTANTS;

