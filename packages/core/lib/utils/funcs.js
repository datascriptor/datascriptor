const crypto = require('crypto');

const zip = (arr1, arr2) => arr1.map((item, index) => [item, arr2[index]]);

/**
 * @function
 * @name perm
 * @description computes all the possible permutations of the items contained in the `items` array
 *              borrowed from https://stackoverflow.com/questions/37579994/generate-permutations-of-javascript-array
 * @param {Array} items
 * @returns Array
 */
const perm = items => {
    const res = [];

    for (let i = 0; i < items.length; i = i + 1) {
        let rest = perm(items.slice(0, i).concat(items.slice(i + 1)));

        if (!rest.length) {
            res.push([items[i]]);
        } else {
            for (let j = 0; j < rest.length; j = j + 1) {
                res.push([items[i]].concat(rest[j]));
            }
        }
    }
    return res;
};

/**
 * @function
 * @name cartProd
 * @description computes the cartesian product of the items contained in N lists
 * @param {Array} lists - an array of N array over which we can perform the cartesian product
 * @returns Array
 */
const cartProd = lists => {
    let ps = [],
        acc = [
            []
        ], // output
        i = lists.length; // list of values lists.
    while (i--) {
        let subList = lists[i],
            j = subList.length; // list of values
        while (j--) {
            let x = subList[j], // list of values for the keen
                k = acc.length;
            while (k--) {
                ps.push([x].concat(acc[k]));
            }
        }
        acc = ps;
        ps = [];
    }
    return acc.reverse(); // reverse because the algo starts at the end
};

/**
 * @function
 * @name intersperse
 * @description borrowed from https://github.com/curvedmark/intersperse/blob/master/lib/intersperse.js
 * @param{Array} arr - the array to intersperse
 * @param{Object} obj - the object to interspers
 * @returns Array
 */
const intersperse = (arr, obj) => {
    if (!arr.length) {
        return [];
    }
	if (arr.length === 1) {
        return arr.slice(0);
    }
	const items = [arr[0]];
	for (let i = 1, len = arr.length; i < len; ++i) {
		items.push(obj, arr[i]);
	}

	return items;
};

/**
 * @function
 * @name uuidv4
 * @description generate a UUID (Universally Unique Identifier)
 * @see https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
 */
const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
    /*
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    ); */
};

module.exports = exports = {
    zip,
    perm,
    intersperse,
    cartProd,
    uuidv4
};
