const _ = require('lodash');
/**
 * @class
 * @name StudyTextFactory
 * @description a utility class to generate text snippets/short sentences out of StudyDesign Arms, Elements, and Events.
 */
class StudyTextFactory {

    /**
     * @constructor
     * @param {Study} study 
     */
    constructor(study) {
        this.study = study;
    }

    /**
     * @static
     * @method
     * @param {String/Object} item - if object item shoul have the properties: 'term' a natural language string 
     *                               that describes the item and 'id' (or 'termAccession' or 'uri'/'iri'), a string that represents a valid IRI
     * @returns String - the report for the item
     */
    static generateBasicItemReport(item) {
        if (item instanceof String) {
            return item;
        }
        if (_.isObject(item)) {
            return `${item.term}${item.id ? ` (${item.id})` : ''}`;
        }
        return '';
    }

    /**
     * @method
     * @name generateArmsReport
     * @returns Object
     */
    generateArmsReport() {
         const { design:  { arms = [] } = {}  } = this.study;
         const armsReport = {};
         for (const [index, arm] of arms.entries()) {
            const { name, size, subjectType } = arm;
            const armTemplate = `The ${name} group consists of ${size} ${subjectType} ${size > 1 ? 'subjects' : 'subject'}.`; 
            armsReport[name] = armTemplate;
         }
         return armsReport;
    }

    /**
     * @method
     * @name generateEventsReport
     * @returns Object - each key is the ID of an event and each value is its textual representation
     * TODO test me!!
     */
    generateEventsReport() {
        return this.study.design.events.reduce((report, event) => {
            const { action, input, output = null, outputSize = 1 } = event;
            let eventTemplate = `${action} was performed on each ${input}`;
            if (output) {
                eventTemplate += `, producing ${outputSize} ${outputSize > 1 ? 'items of ' : ''}${output} per ${input}`;
            }
            eventTemplate += '.\n';
            report[event.id] = eventTemplate;
            return report;
        }, {});
    }

    /**
     * @method
     * @name generateElementsReport
     * @returns Object - each key is the ID of an event and each value is its textual representation
     */
    generateElementsReport() {
        return this.study.design.elements.reduce((report, element) => {
            const { 
                name, agent, intensity, intensityUnit, duration, durationUnit 
            } = element;
            let elementTemplate = '';
            if (agent && intensity) {
                elementTemplate = `During the ${name} phase, ${duration ? `which lasted ${duration} ${durationUnit}` : null}, we supplied the agent ${agent} with dosage ${intensity} ${intensityUnit}.`;
            }
            else if (agent) {
                elementTemplate = `During the ${name} phase, ${agent} was performed on the subject.`;
            }
            else {
                elementTemplate = `The ${name} phase lasted ${duration} ${durationUnit}.`;
            }    
            report[element.id] = elementTemplate;
            return report;
        }, {});
    }
}

module.exports = exports = StudyTextFactory;