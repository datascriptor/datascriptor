
const {
    DEFAULT_SUBJECT_TYPE, DEFAULT_SUBJECT_SIZE: DEFAULT_SIZE,
    OBSERVATIONAL_DESIGN_TYPE,
    FULL_FACTORIAL_DESIGN_TYPE, CROSSOVER_DESIGN_TYPE
} = require('../utils/constants');
const { perm, cartProd, intersperse } = require('../utils/funcs');
const Element = require('./Element');
const Arm = require('./Arm');
const ArmCollection = require('./ArmCollection');

/**
 * @method
 * @name computeSubjectGroupsFromObservationaFactors
 * @description computes the observational factors combinations and returns them
 * in a list
 */
function computeSubjectGroupsFromObservationalFactors(
    subjectType, observationalFactors
) {
    const factorValues = observationalFactors.map(oF => oF.values);
    const factorCombinations = cartProd(factorValues); // [['M', 'infants', 1], [...]]
    return factorCombinations.map((factorCombination, ix) => {
        return {
            name: `SubjectGroup_${ix}`,
            type: subjectType,
            characteristics: factorCombination.map((factorValue, ix) => {
               return {
                   name: observationalFactors[ix].name,
                   value: factorValue,
                   unit: observationalFactors[ix].unit,
                   isQuantitative: observationalFactors[ix].isQuantitative
               };
            })
        };
    });
}


/**
 * @method
 * @name computeFullFactorialElements
 * @descriptions it computes of the all the treatment elements in a full factorial design
 *               as the cartesian product of all the factor values
 *              (considering that we have three default factor values: agents, intensity, and duration)
 */
function computeFullFactorialElements({
    interventionType = null,
    agents = [], intensities = [], durations = [],
    intensityUnit = null, durationUnit = null
}) {
    const factorCombinations = cartProd([agents, intensities, durations]);
    const elements = [];
    for (const [ix, [agent, intensity, duration]] of factorCombinations.entries()) {
        elements.push(new Element({
            id: ix,
            name: `treatment_${ix}`,
            interventionType, 
            agent,
            intensity,
            intensityUnit,
            duration,
            durationUnit
        }));
    }
    return elements;
}

/**
 * @method
 * @name computeArmsFromObservationalAndTreatmentGroups
 * @param {Array} observationalGroups
 * @param {Array} treatmentGroups
 * @return {Array}
 */
function computeArmsFromObservationalAndTreatmentGroups(observationalGroups, treatmentGroups, size) {
    return observationalGroups.flatMap((observationalGroup, ix) => {
        return treatmentGroups.map((treatmentGroup, jx) => {
            const index = ix*treatmentGroups.length + jx;
            // return [observationalGroup, treatmentGroup];
            return new Arm({
                id: index,
                name: `Arm_${index}`,
                size,
                observationalFactors: observationalGroup.characteristics,
                epochs: treatmentGroup.epochs
            });
        });
    });    
}

/**
 * @method
 * @name observationalDesign
 * @param {string} name - the name of the Study/StudyDesign
 * @param {string} description
 * @param {Number} size
 * @param {Array} observationalGroups - the observational groups based on the observational variable
 * @returns {StudyDesign}
 */
function observationalDesign({
    name, description = '', subjectType = DEFAULT_SUBJECT_TYPE,
    observationalGroups = [],
    size = DEFAULT_SIZE, observationPeriod, followUp = null
}) {
    const followUpEl = followUp ? new Element(followUp) : null;
    const observationPeriodEl = new Element(observationPeriod);
    const noTreatmentGroup = {
        epochs: [{ elements: [observationPeriodEl] }]
    };
    if (followUpEl) {
        noTreatmentGroup.epochs.push({
            elements: [followUpEl]
        });
    }
    return new ArmCollection({
        name,
        type: OBSERVATIONAL_DESIGN_TYPE,
        description,
        subjectType,
        arms: computeArmsFromObservationalAndTreatmentGroups(
            observationalGroups,
            [noTreatmentGroup],
            size
        )
    });
}

/**
 * @method
 * @name fullFactorialDesign
 * @param {string} name - the name of the Study/StudyDesign
 * @param {string} description
 * @param {Number} size
 * @param {Array} observationalGroups - the observational groups based on the observational variables
 * @param {Object} elementParams - these params are passed to the underlying computeFullFactorialElements()
 * @returns {StudyDesign}
 * @description generates the full factorial design out of the existing combinations of element parameters
 */
function fullFactorialDesign({
    name, description = '', subjectType = DEFAULT_SUBJECT_TYPE,
    observationalGroups = [],
    size = DEFAULT_SIZE, elementParams = {},
    screen = null, runIn = null, followUp = null
}) {
    const elements = computeFullFactorialElements(elementParams);
    let screenEl = null, runInEl = null, followUpEl = null;
    if (screen) {
        screenEl = new Element(screen);
        if (runIn) {
            runInEl = new Element(runIn);
        }
    }
    if (followUp) {
        followUpEl = new Element(followUp);
    }
    const treatmentGroups = [];
    for (const element of elements) {
        const epochs = [];
        if (screen) {
            epochs.push({elements: [screenEl], events: []});
            if (runIn) {
                epochs.push({elements: [runInEl], events: []});
            }
        }
        epochs.push({elements: [element], events: []});
        if (followUp) {
            epochs.push({elements: [followUpEl], events: []});
        }
        treatmentGroups.push({
            epochs
        });
    }
    return new ArmCollection({
        name,
        type: FULL_FACTORIAL_DESIGN_TYPE,
        description,
        subjectType,
        arms: computeArmsFromObservationalAndTreatmentGroups(
            observationalGroups,
            treatmentGroups,
            size
        )
    });
}

    /**
 * @method
 * @name crossoverDesign
 * @description computes the crossover design
 * @param {string} name - the name of the Study/StudyDesign
 * @param {string} description
 * @param {Number} size
 * @returns {StudyDesign}
 * TODO add tests on this
 */
function crossoverDesign({
    name, description = '', subjectType = DEFAULT_SUBJECT_TYPE,
    observationalGroups = [],
    size = DEFAULT_SIZE, treatments = [],
    screen = null, runIn = null, washout = null, followUp = null
}) {
    const screenEl = screen ? new Element(screen) : null;
    const runInEl = runIn ? new Element(runIn) : null;
    const washoutEl = washout ? new Element(washout) : null;
    const followUpEl = followUp ? new Element(followUp) : null;
    const namedTreatments = treatments.map((treatment, index) => {
        return {
            name: `treatment_${index}`,
            ...treatment
        };
    });
    const permutations = perm(namedTreatments.map(el => {
        return {
            elements: [new Element(el)],
            events: []
        };
    }));
    const treatmentGroups = permutations.map(permutation => {
        // TODO need
        const epochs = washoutEl ? intersperse(permutation, {
            elements: [washoutEl],
            events: []
        }) : permutation;
        if (screenEl) {
            epochs.unshift({elements: [screenEl], events: []});
            if (runInEl) {
                epochs.splice(1, 0, {elements: [runInEl], events: []});
            }
        }
        if (followUpEl) {
            epochs.push({elements: [followUpEl], events: []});
        }
        return {
            epochs
        };
    });
    return new ArmCollection({
        name,
        type: CROSSOVER_DESIGN_TYPE,
        description,
        subjectType,
        arms: computeArmsFromObservationalAndTreatmentGroups(
            observationalGroups, treatmentGroups, size
        )
    });
}

/*
function parallelDesign() {}

function singleArmDesign() {}

function concomirantTreatmentDesign() {}
*/

const armCollectionBuilder = {

    fullFactorial: function generateFullFactorial(payload) {
        return fullFactorialDesign(payload);
    },

    crossover: function generateCrossover(payload) {
        return crossoverDesign(payload);
    },

    observational: function generateObservational(payload) {
        return observationalDesign(payload);
    }

};

module.exports = exports = {
    computeFullFactorialElements,
    observationalDesign,
    fullFactorialDesign,
    crossoverDesign,
    armCollectionBuilder,
    computeSubjectGroupsFromObservationalFactors,
    computeArmsFromObservationalAndTreatmentGroups
};
