// const Event = require('./Event');
// const Element = require('./Element');

class Epoch {
    /**
     * @constructor
     * @param {Array} elements - An array of valid Element objects 
     *      Elements within the same Epoch are considered to be concomitant.
     * @param {Array} events - An array of valid Event objects
     */
    constructor(elements, events) {
        this.elements = elements;
        this.events = events;
    }

}

module.exports =  exports = Epoch;