const { uuidv4 } = require('../utils/funcs');

class Event {
    /**
     * @constructor
     * @description An Event is an object that models something that happens
     *              at a specific time point. By time point, we generally mean
     *              something that lasts significantly less than an Element (and it should
     *              generally last less than 1 day).  It is meant to model sampling events
     *              analysis (i.e. assays) and processing steps.
     * @param {Object} props 
     * @param {String} props.id
     * @param {String} props.action - this is a name/key for the event action
     *      For instance it could be sampling, MR imaging, or Whole Genome Sequencing.
     * @param {String/Object} props.input - this item specifies the upstream specimen, material or data.
     *      It could/should be the output of a previous event. 
     *      If no event is specified, it means that the source was the Subject itself
     * @param {String/Object} props.output - this item specifies the pr
     * @param {Number} props.outputSize - the number of outputs produces per input. Should be an integer
     * @param {String/Object/[Array]} props.instrumentation - any instrument used to perform the analysis
     *      Multiple instruments can be encoded in an Array (ALPHA, tentative)
     * @param {Object/Array} props.parameters if Object contains a set of key/value pairs for the protocol parameters
     * @param {String/[Object]} props.protocol if a String it should be either a URI pointing to the protocol or a textual representation
     *      of the protocol. If an object it contains a more detailed representation of the protocol (TBD)
     */
    constructor(props) {
        const { 
            id = null, action, input = null, 
            output = null, outputSize = null,
            instrumentation = null, parameters = null,
            protocol = null, ...otherProps
        } = props;
        this.id = id || uuidv4();
        this.action = action;
        this.input = input;
        this.output = output; 
        this.outputSize = outputSize;
        this.instrumentation = instrumentation;
        this.parameters = parameters;
        this.protocol = protocol;
        // any optional additional metadata would be stored here (ALPHA)
        // another option would be to save all of them as object properties.
        for (const [key, value] of Object.entries(otherProps)) {
            this[key] = value;
        }
    }

    /**
     * @method
     * @name toJSON
     */
    toJSON() {
        const json = {
            action: this.action,
        };
        for (const prop of [
            'id', 'input', 'output', 'outputSize', 'instrumentation', 'parameters', 'protocol'
        ]) {
            if (this[prop] !== null && this[prop] !== undefined) {
                json[prop] = this[prop];
            }
        }
        return json;
    }

}

module.exports = exports = Event;