module.exports = exports = {
    Arm: require('./Arm'),
    Dataset: require('./Dataset'),
    Element: require('./Element'),
    Event: require('./Event'),
    Group: require('./Group'),
    ArmCollection: require('./ArmCollection'),
    Study: require('./Study'),
    armCollectionFactory: require('./armCollectionFactory'),
    fullFactorialDesign: require('./armCollectionFactory').fullFactorialDesign,
    crossoverDesign: require('./armCollectionFactory').crossoverDesign,
    observationalDesign: require('./armCollectionFactory').observationalDesign,
    armCollectionBuilder: require('./armCollectionFactory').armCollectionBuilder,
    computeSubjectGroupsFromObservationalFactors: require('./armCollectionFactory').computeSubjectGroupsFromObservationalFactors
};
