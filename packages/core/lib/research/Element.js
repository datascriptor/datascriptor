const { isPlainObject, isString } = require('lodash');
const { uuidv4 } = require('../utils/funcs');

class Element {

    /**
     * @constructor
     * @param {Object} props, containing:
     * @param {String} props.id
     * @param {Number} duration - the Element duration, how long it lasts
     * @param {String/Object} durationUnit - a free-text string a URI, 
     *      or an object describing an Ontology Annotation (containing at least a URI property)
     * @param {String/Object} agent - the agent somministrated to the subject (e.g. a drug). A free-text string a URI, 
     *      or an object describing an Ontology Annotation (containing at least a URI property)
     * @param {Number}  intensity - the intensity of the agent
     * @param {String/Object} intensityUnit - a free-text string a URI, 
     *      or an object describing an Ontology Annotation (containing at least a URI property)
     */
    constructor(props) {
        const {
            id = null, 
            name, 
            // epoch = 0,
            duration = null, 
            durationUnit = null,  
            interventionType = null,
            agent = null,
            intensity = null,
            intensityUnit = null,
        } = props;
        this.id = id || uuidv4();
        this.name = name;
        // this.epoch = epoch;
        this.duration = duration;
        this.durationUnit = durationUnit;
        this.agent = agent;
        this.intensity = intensity;
        this.intensityUnit = intensityUnit;
        if (isPlainObject(interventionType)) {
            this.interventionType = {
                term: interventionType.term || interventionType.label,
                iri: interventionType.iri
            };
        }
        else if (isString(interventionType)) {
            this.interventionType = interventionType;
        }
        else {
            this.interventionType = null;
        }
    }

    /**
     * @description checks if the Element is a treatment or not
     * @returns {Boolean} true if it is a treatment, false otherwise 
     */
    isTreatment() {
        return Boolean(this.agent);
    }

    /**
     * @method
     * @name toJSON
     * @description generates the serialised representation of the Event
     */
    toJSON() {
        const json = {};
        for (const prop of [
            'id', 'name', 'interventionType', 'duration', 'durationUnit', 'agent', 'intensity', 'intensityUnit'
        ]) {
            if (this[prop] !== null && this[prop] !== undefined) {
                json[prop] = this[prop];
            }
        }
        return json;
    }

}

module.exports = exports = Element;