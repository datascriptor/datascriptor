const Dataset = require('./Dataset');
const ArmCollection = require('./ArmCollection');
const _ = require('lodash');
const { uuidv4 } = require('../utils/funcs');

/**
 * @class
 * @name Study
 * @description this object contains all the information about a Research Study.
 *      A study can be defined as a detailed examination, analysis, or critical inspection of a subject 
 *      designed to discover facts about it, see http://purl.obolibrary.org/obo/NCIT_C63536
 *      A study is alsp a process that realizes the steps defined in a study design.
 *      In Datascriptor, each study should be associated to (i.e. contain) a StudyDesign object.
 *      This is basically equal to the ISA concept of study, see: https://isa-tools.org/format/specification.html
 *      It is affine to the schema.org entity MedicalStudy, though more generic: https://schema.org/MedicalStudy
 * 
 */
class Study {

    /**
     * @param {Object} props, containing:
     * @param {String} props.id - the id of the Study. This must be unique within the Datascriptor domain
     * @param {String} props.name - the name of the Study
     * @param {String} props.description - a description of the Study
     * @param {StudyDesign} props.design - the object containing all the study design metadata
     * @param {Array} props.datasets - an array of Datasets - datasets should generally represent 
     *      structured (i.e. tabular-like) data pertaining to the study
     * Additional params should describe subjects, samples, files, and possibly other metadata 
     */
    constructor({id = '', name = '', description = '', design = null,  datasets = []}) {
        this.id = id || `#study/${uuidv4()}`;
        this.name = name;
        this.description = description;
        this.design = design;
        this.datasets = datasets;
    }

    /**
     * @static
     * @method
     * @name fromJSON
     * Loads or deserializes a Study object from its JSON representation
     * @param {Object} json 
     * @returns {Study}
     */
    static fromJSON(json = {}) {
        const { design, datasets, ...otherProps } = json;
        const res = new Study(otherProps);
        if (!_.isEmpty(design)) {
            res.design = ArmCollection.fromJSON(design);
        } 
        /* very experimental/ALPHA down here, not used in any way yet
        subject to change: could make a mapping of datasets rather than an array? */
        if (!_.isEmpty(datasets)) {
            res.datasets = datasets.map(d => new Dataset({data: d.data, schema: d.schema}));
        } 
        /* here we may consider how to set subject, sample, and file information */   
        // if (!_.isEmpty(otherProps)) {}
        return res;
    }

    /**
     * @method
     * @static
     * @name fromExpandedJSON
     * TODO
     */
    fromExpandedJSON() {}

    /**
     * @method
     * @name toJSON
     * TODO
     */
    toJSON() {}

    /**
     * @method
     * @name toExpandedJSON
     * TODO
     */
    toExpandedJSON() {}

}

module.exports = exports = Study;