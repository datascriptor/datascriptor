const { isEmpty, isObject } = require('lodash');

class Dataset {

    constructor({data, schema = null}) {
        this.data = data;
        this.schema = schema;
    }

    hasSchema() {
        return isObject(this.schema) && !isEmpty(this.schema);
    }

    /**
     * @method isValid
     * @description returns true if the Dataset data can be validate against the schema, false otherwise
     * @returns Boolean
     */
    isValid() {
        if (isEmpty(this.schema)) {
            return true;
        }
        return false;
    }
}


module.exports = exports = Dataset;