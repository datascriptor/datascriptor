/**
 * @class
 * @name Group
 */
class Group {

    /**
     * @constructor
     * @param {String} name 
     * @param {Array} subjects 
     * @param {Array} samples 
     * @param {Array} extracts 
     * @param {Array} otherMaterials 
     * @param {Dataset} dataset 
     * @param {Object} metadata 
     * @param {Array} files 
     */
    constructor(name, subjects = [], samples = [], extracts = [], otherMaterials = [], dataset = [],
        metadata = {}, files = []) {
        this.name = name;
        this.subjects = subjects;
        this.samples = samples;
        this.extracts = extracts;
        this.otherMaterials = otherMaterials;
        this.dataset = dataset;
        this.metadata = metadata;
        this.files = files;
        
    }
}

module.exports = exports = Group