// const { DATASCRIPTOR_HOME_URL } = require('../../utils/constants');
// const { uuidv4 } = require('../../utils/funcs');
const { uniq } = require('lodash');
const Arm = require('./Arm');
const Epoch = require('./Epoch');
const Element = require('./Element');
const Event = require('./Event');

/**
 * @class
 * @name ArmCollection
 * @description The object capturing al the information pertaining to a collection of study amrms.
 *  NCI defines study design as "a plan detailing how a study will be performed in order to represent the phenomenon 
 *  under examination, to answer the research questions that have been asked, and defining the methods of data analysis. 
 *  Study design is driven by research hypothesis being posed, study subject/population/sample available, 
 *  logistics/resources: technology, support, networking, collaborative support, etc."
 *  This class is better used by loading study design using the .fromJSON() static method rather than building StudyDesign object from scratch.
 *  If you are building a StudyDesign from scratch you MUST ensure that elements and events are not duplicated.
 */
class ArmCollection {

    /**
     * @constructor
     * @param {Object} props - containing
     * @param {String} props.name - a name that "uniquely defines" the study design
     * @param {String/Objecte} props.type - the type of the Study Design (e.g factorial, cross-over, etc) 
     * @param {String} props.description - a description of the current study design
     * @param {Arm} props.arms 
     */
    constructor({name = '', type, description = '', subjectType = null, arms = []}) {
        // this.id = id || uuidv4(); TODO whould we need an ID here or only for the Study? 
        this.name = name;
        this.type = type;
        this.description = description;
        this.subjectType = subjectType;
        this.arms = arms;
    }

    /**
     * NOTE: this relies on Element uniqueness on Design creation
     * Otherwise Set() will not filter duplicates even though all 
     * the properties are the same
     */
    get events() {
        return [...new Set(this.arms.flatMap(
            arm => arm.epochs.flatMap(
                epoch => epoch.events
            )
        ).filter(el => el instanceof Event))];
    }

    /**
     * NOTE: this relies on Element uniqueness on Design creation
     * Otherwise Set() will not filter duplicates even though all 
     * the properties are the same
     */
    get elements() {
        return [...new Set(this.arms.flatMap(
            arm => arm.epochs.flatMap(
                epoch => epoch.elements
            )
        ).filter(el => el instanceof Element))];
    }

    /* TODO implement me 
       FIXME do we really need this?
    get epochs() {
        return this.arms.map(arm => arm.epochs);
    } */

    /**
     * @method
     * @static
     * @description builds a StudyDesign out of a valid JSON object
     * @param {Object} json - the POJO to be loaded into a StudyDesign 
     * @returns StudyDesign
     */
    static fromJSON(json) {
        const elements = json.elements ? json.elements.map(el => new Element(el)) : [];
        const events = json.events ? json.events.map(ev => new Event(ev)) : [];
        const arms = [];
        for (const armJson of json.arms) {
            const { epochs = [], ...armProps } = armJson;
            const armEpochs = [];
            for (const epochJson of epochs) {
                const epoch = new Epoch();
                epoch.elements = epochJson.elements ? epochJson.elements.map(id => {
                    const element = elements.find(
                        el => el.id === id
                    );
                    if (!element) {
                        throw Error(`Missing element ${id} in the StudyDesign JSON.`);
                    }
                    return element;
                }) : null;
                epoch.events = epochJson.events ? epochJson.events.map(id => {
                    const event = events.find(
                        el => el.id === id
                    );
                    if (!event) {
                        throw Error(`Missing event ${id} in the StudyDesign JSON.`);
                    }
                    return event;
                }) : null;
                armEpochs.push(epoch);
            }
            arms.push(new Arm({...armProps, epochs: armEpochs}));
        }
        return new ArmCollection({
            name: json.name,
            type: json.type,
            description: json.description,
            subjectType: json.subjectType,
            arms
        });
    }

    /**
     * @method
     * @static
     * @name fromExpandedJSON
     * @description
     */
    static fromExpandedJSON(json) {
        let elementsArr = [], eventsArr = [];
        // TODO this for cycle would better be rewritten in a more functional way
        for (const arm of json.arms) {
            elementsArr =  elementsArr.concat(arm.elements);
            eventsArr = eventsArr.concat(arm.events);
        }
        elementsArr = uniq(elementsArr).map(el => new Element(el));
        eventsArr = uniq(eventsArr).map(ev => new Event(ev));
        const arms = json.arms.map(armJson => {
            const { elements, events, ...rest } = armJson;
            const elementEpochs = elements.map(el => el.epoch);
            const eventEpochs = events.map(ev => ev.epoch);
            const lastEpoch = Math.max(...elementEpochs, ...eventEpochs);
            const epochs = [];
            for (let i = 0; i <= lastEpoch; i++) {
                const elementsInEpoch = elements.filter(el => el.epoch == i);
                const eventsInEpoch = events.filter(ev => ev.epoch == i);
                const epoch = new Epoch();
                if (elements) {
                    epoch.elements = elementsInEpoch.map(el => elementsArr.find(e => e.id === el.id));
                }
                if (events) {
                    epoch.events = eventsInEpoch.map(ev => eventsArr.find(e => e.id === ev.id));
                }
                epochs.push(epoch);
            }
            return new Arm({...rest, epochs });
        });
        return new ArmCollection({
            name: json.name, type: json.type,
            subjectType: json.subjectType,
            description: json.description,
            arms
        });
    }

    /**
     * @method
     * @name toJSON
     * @description serializes a StudyDesign to JSON. The JSON can be safely loaded back
     *              into a StudyDesign using StudyDesign.fromJSON()
     * @returns the JSON-serializable Plain Old JavaScript Object describing the StudyDesign
     */
    toJSON() {
        const json = {};
        for (const prop of ['name', 'description', 'type', 'subjectType']) {
            if (this[prop]) { // works also for empty strings
                json[prop] = this[prop];
            }
        }
        json.elements =  this.elements.map(el => el.toJSON());
        json.events =  this.events.map(ev => ev.toJSON());
        const serializedArms = [];
        for (const arm of this.arms) {
            const armJson = arm.toShallowDescription();
            const serializedEpochs = [];
            for (const epoch of arm.epochs) {
                const serializedEpoch = {};
                if (epoch.elements) {
                    serializedEpoch.elements = epoch.elements.map(el => json.elements.find(e => e.id === el.id).id);
                }
                if (epoch.events) {
                    serializedEpoch.events = epoch.events.map(ev => json.events.find(e => e.id === ev.id).id);
                }
                serializedEpochs.push(serializedEpoch);
            }
            armJson.epochs = serializedEpochs;
            serializedArms.push(armJson);
        }
        json.arms = serializedArms;
        return json;
    }

    /**
     * @private
     * @method
     * @static
     * @name _expandItems
     * @description utility method to expand Elements and Events within .toExpandedJSON()
     */
    static _expandItems(items, epoch) {
        if (!items) {
            return [];
        }
        return items.map(it => {
            return {
                ...it.toJSON(),
                epoch
            };
        });
    }

    /**
     * @method
     * @name toExpandedJSON
     * @description serializes a StudyDesign to JSON. The JSON can be safely loaded back
     *              into a StudyDesign using StudyDesign.fromExpandedJSON()
     *              The difference with the standard .toJSON() methods is that this representation
     *              does not rely on IDs to place events and elements within the epoch,
     *              but all the arms are actually populated with the full JSON representation
     *              of Elements and Events, thus yielding to many duplicates in the JSON and most
     *              likely a larger JSON. 
     * @returns the expanded JSON-serializable Plain Old JavaScript Object describing the StudyDesign
     */
    toExpandedJSON() {
        const json = {};
        for (const prop of ['name', 'description', 'type', 'subjectType']) {
            if (this[prop]) { // works also for empty strings
                json[prop] = this[prop];
            }
        }
        const serializedArms = [];
        for (const arm of this.arms) {
            const armJson = arm.toShallowDescription();
            armJson.elements = [];
            armJson.events = [];
            for (const [ix, epoch] of arm.epochs.entries()) {
                armJson.elements.push(...this.constructor._expandItems(epoch.elements, ix));
                armJson.events.push(...this.constructor._expandItems(epoch.events, ix));
            }
            serializedArms.push(armJson);
        }
        json.arms = serializedArms;
        return json;
    }

}

module.exports = exports = ArmCollection;