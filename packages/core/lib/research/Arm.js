// const Element = require('./Element');
// const Event = require('./Event');

class Arm {

    /**
     * @constructor
     * @param {Object} props
     * @param {String} props.id
     * @param {String} props.name
     * @param {Array} props.observationalFactors
     * @param {Number} props.size - must be a positive integer
     * @param {Array} props.epochs
     */
    constructor({
        id = null, name,
        // subjectType = '', 
        size,
        observationalFactors = [],
        epochs = []
    }) {
        // private attributes
        // this._elements = null;
        // this._events = null;

        // public interface
        this.id = id;
        this.name = name;
        // this.subjectType = subjectType;
        this.size = size;
        this.observationalFactors = observationalFactors;
        this.epochs = epochs;
    }

    /**
     * @method
     * @name toShallowDescription
     * @description this returns a "plain" JSON representation of the Arm 
     *      (i.e. without the epochs' information)
     *      This method is meant to be used mostly for the full-fledged StudyDesign serialization
     */
    toShallowDescription() {
        const json = {};
        for (const prop of ['id', 'name', 'observationalFactors', 'size']) {
            if (this[prop] !== null && this[prop] !== undefined) {
                json[prop] = this[prop];
            }
        }
        return json;
    } 

}

module.exports = exports = Arm;