module.exports = exports = {
    research: require('./lib/research'),
    text: require('./lib/text'),
    constants: require('./lib/utils/constants')
};