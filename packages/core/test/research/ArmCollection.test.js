const { expect } = require('chai');
const ArmsCollection = require('../../lib/research/ArmCollection');
const Arm = require('../../lib/research/Arm');
const Epoch = require('../../lib/research/Epoch');
const Element = require('../../lib/research/Element');
const Event = require('../../lib/research/Event');
const testJson = require('../fixtures/research/study');
const testExpandedJson = require('../fixtures/research/study-expanded');

describe('ArmCollection', function() {

    describe('constructor', function() {

        it('creates a StudyDesign object', function() {
            const name = 'test study design';
            const type = ArmsCollection.FULL_FACTORIAL_DESIGN_TYPE;
            const design = new ArmsCollection({ name, type });
            expect(design).to.be.instanceOf(ArmsCollection);
            expect(design.name).to.equal(name);
            expect(design.type).to.eql(type);
            expect(design.description).to.equal('');
            expect(design.arms).to.eql([]);
        });

    });

    describe('getters', function() {

        const design = ArmsCollection.fromJSON(testJson.design);

        describe('events()', function() {
            it('returns all the events contained in the study design, each only once', function() {
                expect(design.events).to.have.length(testJson.design.events.length);
                for (const [ix, event] of design.events.entries()) {
                    expect(event).to.be.instanceOf(Event);
                    expect(event.id).to.equal(testJson.design.events[ix].id);
                }
            });
        });

        describe('elements()', function() {
            it('returns all the elements contained in the study design, each only once', function() {
                expect(design.elements).to.have.length(testJson.design.elements.length);
                for (const [ix, el] of design.elements.entries()) {
                    expect(el).to.be.instanceOf(Element);
                    expect(el.id).to.equal(testJson.design.elements[ix].id);
                }
            });
        });

        /* 
        describe('epochs()', function() {
            it('returns all the epochs, each with the elements and events happening in each Arm', function() {

            });
        }); */

    });

    describe('fromJSON()', function() {
        it('instantiate the StudyDesign object from input JSON', function() {
            const design = ArmsCollection.fromJSON(testJson.design);
            expect(design).to.be.instanceOf(ArmsCollection);
            expect(design.arms).to.have.length(2);
            for (const arm of design.arms) {
                expect(arm).to.be.instanceOf(Arm);
                expect(arm.epochs).not.to.be.empty;
                for (const epoch of arm.epochs) {
                    expect(epoch).to.be.instanceOf(Epoch);
                    if (epoch.elements) {
                        for (const element of epoch.elements) {
                            expect(element).to.be.instanceOf(Element);
                        }
                    }
                    if (epoch.events) {
                        for (const event of epoch.events) {
                            expect(event).to.be.instanceOf(Event);
                        }
                    }
                }
            }
        });
    });

    describe('fromExpandedJSON()', function() {
        it('instantiate the StudyDesign object from input expanded JSON', function() {
            const design = ArmsCollection.fromExpandedJSON(testExpandedJson.design);
            expect(design).to.be.instanceOf(ArmsCollection);
            expect(design.arms).to.have.length(2);
            for (const arm of design.arms) {
                expect(arm).to.be.instanceOf(Arm);
                expect(arm.epochs).not.to.be.empty;
                for (const epoch of arm.epochs) {
                    expect(epoch).to.be.instanceOf(Epoch);
                    if (epoch.elements) {
                        for (const element of epoch.elements) {
                            expect(element).to.be.instanceOf(Element);
                        }
                    }
                    if (epoch.events) {
                        for (const event of epoch.events) {
                            expect(event).to.be.instanceOf(Event);
                        }
                    }
                }
            }
        });
    });

    describe('toJSON', function() {

        const design = ArmsCollection.fromJSON(testJson.design);

        it('serializes a StudyDesign object back into JSON', function() {
            expect(design.toJSON()).to.eql(testJson.design);
        });
    });

    describe('toExpandedJSON', function() {

        it('serializes a StudyDesign object back into JSON', function() {
            const design = ArmsCollection.fromJSON(testJson.design);
            expect(design.toExpandedJSON()).to.eql(testExpandedJson.design);
        });

        it('loads a stusy as an expanded JSON and dumps it as JSON', function() {
            const design = ArmsCollection.fromExpandedJSON(testExpandedJson.design);
            const json = design.toJSON();
            expect(json).to.eql(testJson.design);
        });
    });

});