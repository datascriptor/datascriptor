const { expect } = require('chai');
const Study = require('../../lib/research/Study');
const ArmCollection = require('../../lib/research/ArmCollection');
const testJson = require('../fixtures/research/study');

describe('ArmCollection', function() {

    describe('fromJSON()', function() {
        it('loads a Study from its JSON representation', function () {
            const study = Study.fromJSON(testJson);
            expect(study).to.be.instanceOf(Study);
            expect(study.id).not.to.be.empty;
            expect(study.name).to.equal(testJson.name);
            expect(study.design).to.be.instanceOf(ArmCollection);
        });    
    });

    // TODO
    describe('fromExpandedJSON()', function() {});

    // TODO
    describe('toJSON()', function() {});
    
    // TODO
    describe('toExpandedJSON()', function() {});

});
