const { expect } = require('chai');
// const sinon = require('sinon');
const Element = require('../../lib/research/Element');
const { OBSERVATIONAL_DESIGN_TYPE, FULL_FACTORIAL_DESIGN_TYPE, CROSSOVER_DESIGN_TYPE } = require('../../lib/utils/constants');
const StudyDesign = require('../../lib/research/ArmCollection');
const Arm = require('../../lib/research/Arm');
const armCollectionFactory = require('../../lib/research/armCollectionFactory');

const interventionTypes = require('../../lib/resources/intervention-types');
const TEST_INDEX = Math.floor(Math.random() * Math.floor(interventionTypes.length - 1));

const testSubjectType = {
    term: 'Homo Sapiens',
    iri: 'http://purl.obolibrary.org/obo/NCBITaxon_9606'
};

const testObservationalFactors = [
    {
        name: 'sex',
        values: ['M'],
        isQuantitative: false,
    },
    {
        name: 'age',
        values: ['infants', 'teenagers'],
        isQuantitative: false
    },
    {
        name: 'example',
        values: [1, 10],
        isQuantitative: true,
        unit: 'wtv'
    }
];

const testObservationalGroups = [
    {
        name: 'SubjectGroup_0',
        type: testSubjectType,
        characteristics: [
            {
                name: 'sex',
                value: 'M',
                isQuantitative: false,
                unit: undefined
            },
            {
                name: 'age',
                value: 'teenagers',
                unit: undefined,
                isQuantitative: false
            },
            {
                name: 'example',
                value: 1,
                unit: 'wtv',
                isQuantitative: true
            }
        ]
    },
    {
        name: 'SubjectGroup_1',
        type: testSubjectType,
        characteristics: [
            {
                name: 'sex',
                value: 'M',
                isQuantitative: false,
                unit: undefined
            },
            {
                name: 'age',
                value: 'teenagers',
                isQuantitative: false,
                unit: undefined
            },
            {
                name: 'example',
                value: 10,
                unit: 'wtv',
                isQuantitative: true
            }
        ]
    },
    {
        name: 'SubjectGroup_2',
        type: testSubjectType,
        characteristics: [
            {
                name: 'sex',
                value: 'M',
                isQuantitative: false,
                unit: undefined
            },
            {
                name: 'age',
                value: 'infants',
                isQuantitative: false,
                unit: undefined,
            },
            {
                name: 'example',
                value: 1,
                unit: 'wtv',
                isQuantitative: true
            }
        ]
    },
    {
        name: 'SubjectGroup_3',
        type: testSubjectType,
        characteristics: [
            {
                name: 'sex',
                value: 'M',
                isQuantitative: false,
                unit: undefined
            },
            {
                name: 'age',
                value: 'infants',
                isQuantitative: false,
                unit: undefined
            },
            {
                name: 'example',
                value: 10,
                unit: 'wtv',
                isQuantitative: true
            }
        ]
    }
];

const testTreatmentGroups = [
    {
        name: 'TreatmentGroup_0',
        epochs: []
    },
    {
        name: 'TreatmentGroup_1',
        epochs: []
    }, 
    {
        name: 'TreatmentGroup_3',
        epochs: []
    }
];

describe('armCollectionFactory', function () {

    describe('computeSubjectGroupsFromObservationalFactors()', function () {
        it('computes the observational factors combinations correctly', function () {
            const actualSubjectGroups = armCollectionFactory.computeSubjectGroupsFromObservationalFactors(
                testSubjectType,
                testObservationalFactors
            );
            expect(actualSubjectGroups.length).to.equal(4);
            expect(actualSubjectGroups).to.eql(testObservationalGroups);
        });
    });

    describe('computeFullFactorialElements()', function () {
        it(`computes the element for the full factorial
            design, given the parameters as strings`, function () {
            const params = {
                agents: ['honey', 'sugar', 'molasses'],
                intensities: [0.1, 1.0, 10.0],
                durations: [7, 30],
                intensityUnit: 'cl/Kg',
                durationUnit: 'day'
            };
            const actualElements = armCollectionFactory.computeFullFactorialElements(params);
            expect(actualElements.length).to.equal(
                params.agents.length * params.durations.length * params.intensities.length
            );
            for (const element of actualElements) {
                expect(element).to.be.an.instanceof(Element);
                expect(element.agent).to.be.oneOf(params.agents);
                expect(element.intensity).to.be.oneOf(params.intensities);
                expect(element.intensityUnit).to.equal(params.intensityUnit);
                expect(element.duration).to.be.oneOf(params.durations);
                expect(element.durationUnit).to.equal(params.durationUnit);
            }
        });
    });

    describe('computeArmsFromObservationalAndTreatmentGroups', function() {
        it('computes all the combinations between observational and treatment groups', function () {
            const arms = armCollectionFactory.computeArmsFromObservationalAndTreatmentGroups(
                testObservationalGroups, testTreatmentGroups
            );
            
            expect(arms).to.have.length(testObservationalGroups.length*testTreatmentGroups.length);
            for (const arm of arms) {
                expect(arm).to.be.instanceOf(Arm);
                expect(arm).to.have.property('observationalFactors');
                expect(arm.observationalFactors).to.have.length(testObservationalFactors.length);
            }
        });
    });

    describe('observationalDesign()', function() {
        const name = 'Test Study Design';
        const description = 'This is an observational design on populations with different characteristics.';
        const size = 17;
        const subject = 'Mouse';
        const observationalPeriodObj = { duration: 60, durationUnit: 'day' };
        const followUpObj = { duration: 30, durationUnit: 'day' };

        it('generates an observational design', function () {
            const actualDesign = armCollectionFactory.observationalDesign({
                name, description, size,
                subjectType: subject,
                observationalGroups: testObservationalGroups,
                observationPeriod: observationalPeriodObj,
                followUp: followUpObj
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(OBSERVATIONAL_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(testObservationalGroups.length);
            for (const arm of actualDesign.arms) {
                const element = actualDesign.elements.find(el => el.id === arm.epochs[0].elements[0].id);
                expect(arm).to.have.property('observationalFactors');
                expect(arm.observationalFactors.length).to.be.greaterThan(0);
                // expect(element).to.have.length(1);
                expect(element.isTreatment()).to.equal(false);
            }
        });
    });

    describe('fullFactorialDesign()', function() {

        const name = 'Test Study Design';
        const description = `This is meant to be a full factorial design comparing the effect of honey,
            sugar and molasses under three different dosages (0.1, 1, and 10 cl/Kg)`;
        const params = {
            interventionType: interventionTypes[TEST_INDEX],
            agents: ['honey', 'sugar', 'molasses'],
            intensities: [0.1, 1.0, 10.0],
            durations: [7],
            intensityUnit: 'cl/Kg',
            durationUnit: 'day'
        };
        const subject = 'Homo sapiens';
        const size = 10;
        const screenObj = { duration: 7, durationUnit: 'day' };
        const runInObj = { duration: 3, durationUnit: 'day' };
        const followUpObj = { duration: 30, durationUnit: 'day' };

        it('generates a full factorial design', function () {

            const actualDesign = armCollectionFactory.fullFactorialDesign({
                name, description, size,
                subjectType: subject, elementParams: params,
                observationalGroups: testObservationalGroups 
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(FULL_FACTORIAL_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(
                params.agents.length * params.durations.length * params.intensities.length * testObservationalGroups.length
            );
            for (const arm of actualDesign.arms) {
                const element = actualDesign.elements.find(el => el.id === arm.epochs[0].elements[0].id);
                // expect(element).to.have.length(1);
                expect(element.isTreatment()).to.equal(true);
                expect(element.interventionType.term).to.equal(interventionTypes[TEST_INDEX].label);
                expect(element.interventionType.iri).to.equal(interventionTypes[TEST_INDEX].iri);
                expect(element.agent).to.be.oneOf(params.agents);
                expect(element.intensity).to.be.oneOf(params.intensities);
                expect(element.duration).to.be.oneOf(params.durations);
                expect(arm.observationalFactors).to.be.oneOf(
                    testObservationalGroups.map(og => og.characteristics)
                );
            }
        });

        it('generates a full factorial design with a screen phase before treatment for each arm', function () {
            const actualDesign = armCollectionFactory.fullFactorialDesign({
                name, description, size,
                subjectType: subject, elementParams: params, screen: screenObj,
                observationalGroups: testObservationalGroups
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(FULL_FACTORIAL_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(
                params.agents.length * params.durations.length * params.intensities.length * testObservationalGroups.length
            );
            for (const arm of actualDesign.arms) {
                // there is a screen phase, our treatment element is in the second epoch
                const element = actualDesign.elements.find(el => el.id === arm.epochs[1].elements[0].id);
                expect(element.agent).to.be.oneOf(params.agents);
                expect(element.intensity).to.be.oneOf(params.intensities);
                expect(element.duration).to.be.oneOf(params.durations);
                // screen is in the first epoch
                const screen = arm.epochs[0].elements[0];
                expect(screen.duration).to.equal(screenObj.duration);
                expect(screen.durationUnit).to.equal(screenObj.durationUnit);
            }
        });

        it('generates a full factorial design with a run-in phase before treatment for each arm', function () {
            const actualDesign = armCollectionFactory.fullFactorialDesign({
                name, description, size,
                subjectType: subject, observationalGroups: testObservationalGroups,
                elementParams: params, screen: screenObj, runIn: runInObj
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(FULL_FACTORIAL_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(
                params.agents.length * params.durations.length * params.intensities.length * testObservationalGroups.length
            );
            for (const arm of actualDesign.arms) {
                // there is a screen and a run-in phase, our treatment element is in the third epoch
                const element = actualDesign.elements.find(el => el.id === arm.epochs[2].elements[0].id);
                expect(element.agent).to.be.oneOf(params.agents);
                expect(element.intensity).to.be.oneOf(params.intensities);
                expect(element.duration).to.be.oneOf(params.durations);
                // screen is in the first epoch
                const screen = arm.epochs[0].elements[0];
                expect(screen.duration).to.equal(screenObj.duration);
                expect(screen.durationUnit).to.equal(screenObj.durationUnit);
                // run-in is in the second epoch
                const runIn = arm.epochs[1].elements[0];
                expect(runIn.duration).to.equal(runInObj.duration);
                expect(runIn.durationUnit).to.equal(runInObj.durationUnit);
            }
        });

        it('generates a full factorial design with a follow-up phase after treatment for each arm', function () {
            const actualDesign = armCollectionFactory.fullFactorialDesign({
                name, description, size,
                subjectType: subject, observationalGroups: testObservationalGroups,
                elementParams: params, followUp: followUpObj
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(FULL_FACTORIAL_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(
                params.agents.length * params.durations.length * params.intensities.length * testObservationalGroups.length
            );
            for (const arm of actualDesign.arms) {
                // there is only follow-up, our treatment element is in the first epoch
                const element = actualDesign.elements.find(el => el.id === arm.epochs[0].elements[0].id);
                expect(element.agent).to.be.oneOf(params.agents);
                expect(element.intensity).to.be.oneOf(params.intensities);
                expect(element.duration).to.be.oneOf(params.durations);
                // follow-up is in epoch 1
                const followUp = arm.epochs[1].elements[0];
                expect(followUp.duration).to.equal(followUpObj.duration);
                expect(followUp.durationUnit).to.equal(followUpObj.durationUnit);
            }
        });

    });

    describe('crossoverDesign()', function () {

        const name = 'Test Study Design';
        const description = `This is meant to be a full factorial design comparing the effect of honey,
            sugar and molasses under three different dosages (0.1, 1, and 10 cl/Kg)`;
        const testInterventionType = 'dietary intervention';
        const firstTreatment = {
            interventionType: testInterventionType,
            agent: 'honey',
            intensity: 15.0,
            intensityUnit: 'cl/Kg',
            duration: 30,
            durationUnit: 'day'
        };
        const secondTreatment = {
            interventionType: testInterventionType,
            agent: 'sugar',
            intensity: 15.0,
            intensityUnit: 'cl/Kg',
            duration: 30,
            durationUnit: 'day'
        };
        const thirdTreatment = {
            agent: 'maple syrup',
            intensity: 15.0,
            intensityUnit: 'cl/Kg',
            duration: 30,
            durationUnit: 'day'
        };
        const fourthTreatment = {
            agent: 'maple syrup',
            intensity: 15.0,
            intensityUnit: 'cl/Kg',
            duration: 30,
            durationUnit: 'day'
        };
        const subject = 'Homo sapiens';
        const size = 12;
        const screenObj = { id: '#screen', duration: 14, durationUnit: 'day' };
        const runInObj = { id: '#run-in', duration: 7, durationUnit: 'day' };
        const washoutObj = { id: '#washout', duration: 14, durationUnit: 'day' };
        const followUpObj = { id: '#follow-up', duration: 30, durationUnit: 'day' };

        it('generates a crossover design', function () {
            const treatments = [firstTreatment, secondTreatment, thirdTreatment];
            const actualDesign = armCollectionFactory.crossoverDesign({
                name, description, subjectType: subject,
                size,
                observationalGroups: testObservationalGroups,
                treatments
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(CROSSOVER_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(6 * testObservationalGroups.length); // 3! = 6
            for (const arm of actualDesign.arms) {
                expect(arm.epochs).to.have.length(treatments.length);
            }
        });

        it('generates a crossover design with interspersed washouts', function () {
            const treatments = [firstTreatment, secondTreatment, fourthTreatment];
            const actualDesign = armCollectionFactory.crossoverDesign({
                name, description, subjectType: subject,
                size, observationalGroups: testObservationalGroups,
                treatments, washout: washoutObj 
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(CROSSOVER_DESIGN_TYPE);
            expect(actualDesign.name).to.equal(name);
            expect(actualDesign.description).to.equal(description);
            expect(actualDesign.arms).to.have.length(6 * testObservationalGroups.length); // 3! = 6
            for (const arm of actualDesign.arms) {
                expect(arm.epochs).to.have.length(treatments.length + 2);
                const treatment0 = actualDesign.elements.find(el => el.id === arm.epochs[0].elements[0].id);
                expect(treatment0.interventionType).to.be.oneOf([testInterventionType, null]);
                const firstWashout = actualDesign.elements.find(el => el.id === arm.epochs[1].elements[0].id);
                const secondWashout = actualDesign.elements.find(el => el.id === arm.epochs[3].elements[0].id);
                expect(firstWashout).to.equal(secondWashout);
                expect(firstWashout.duration).to.equal(washoutObj.duration);
                expect(firstWashout.durationUnit).to.equal(washoutObj.durationUnit);
            }
        });

        it('generates a crossover design with screen, run-in, and follow-up', function () {
            const treatments = [secondTreatment, thirdTreatment, fourthTreatment];
            const actualDesign = armCollectionFactory.crossoverDesign({
                name, description, subjectType: subject,
                size,
                observationalGroups: testObservationalGroups,
                treatments,
                screen: screenObj, runIn: runInObj, followUp: followUpObj
            });
            expect(actualDesign).to.be.instanceOf(StudyDesign);
            expect(actualDesign.type).to.eql(CROSSOVER_DESIGN_TYPE);
            for (const arm of actualDesign.arms) {
                expect(arm.size).to.equal(size);
                expect(arm.epochs).to.have.length(treatments.length + 3);
                // screen is in the first epoch
                const screen = actualDesign.elements.find(el => el.id === arm.epochs[0].elements[0].id);
                console.log(`screen for arm ${arm.id}: duration: ${screen.duration}`);
                expect(screen.duration).to.equal(screenObj.duration);
                expect(screen.durationUnit).to.equal(screenObj.durationUnit);
                // run-in is in the second epoc
                const runIn = actualDesign.elements.find(el => el.id === arm.epochs[1].elements[0].id);
                console.log(`run-in for arm ${arm.id}: duration: ${runIn.duration}`);
                expect(runIn.duration).to.equal(runInObj.duration);
                expect(runIn.durationUnit).to.equal(runInObj.durationUnit);
                // follow-up is in the final epoch
                const followUp = actualDesign.elements.find(el => el.id === arm.epochs[5].elements[0].id);
                expect(followUp.duration).to.equal(followUpObj.duration);
                expect(followUp.durationUnit).to.equal(followUpObj.durationUnit);
            }
        });
    });

    describe('computeConcomirantTreatmentDesign()', function() {});

    describe('armCollectionBuilder', function() {
        /*
        let factStub, crossStub;

        beforeEach(function() {
            factStub = sinon.stub(studyDesignFactory, 'fullFactorialDesign').withArgs({}).returns('factorial');
            crossStub = sinon.stub(studyDesignFactory, 'crossoverDesign').withArgs({}).returns('crossover');
        });

        afterEach(function() {
            studyDesignFactory.fullFactorialDesign.restore();
            studyDesignFactory.crossoverDesign.restore();
        }); */

        it('fullFactorial', function() {
            // sinon.stub(studyDesignFactory, 'fullFactorialDesign').returns('factorial');
            const payload = {};
            const res = armCollectionFactory.armCollectionBuilder['fullFactorial'](payload);
            expect(res).to.be.not.empty;
        });

        it('crossover', function() {
            // sinon.stub(studyDesignFactory, 'crossoverDesign').returns('crossover');
            const payload = {};
            const res = armCollectionFactory.armCollectionBuilder['crossover'](payload);
            expect(res).to.be.not.empty;
        });

        it('observational', function() {
            // sinon.stub(studyDesignFactory, 'crossoverDesign').returns('crossover');
            const payload = {
                observationPeriod: {
                    duration: 3,
                    durationUnit: 'days'
                }
            };
            const res = armCollectionFactory.armCollectionBuilder['observational'](payload);
            expect(res).to.be.not.empty;
        });
    });

});
