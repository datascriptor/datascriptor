const chai = require('chai');
const Dataset = require('../../lib/research/Dataset');

// chai configuration
const expect = chai.expect;
chai.config.includeStack = true;

// test data
const d = {
    data: [],
    schema: {}
};

describe('Dataset', function() {

    describe('constructor', function() {

        it('creates a new Dataset object', function() {
            const dataset = new Dataset(d);
            expect(dataset).to.be.instanceOf(Dataset);
        });
        
    });

    describe('#data', function() {

        it('returns the data of the dataset', function() {
            const dataset = new Dataset(d);
            expect(dataset.data).to.eql(d.data);
        });

    });

    describe('#schema', function() {

        it('returns null if an empty schema is provided to the dataset', function() {
            const dataset = new Dataset({ data: d.data });
            expect(dataset.schema).to.eql(null);
        });
        
    });

    describe('hasSchema()', function() {

        it('returns false if the dataset has an empty schema', function() {
            const dataset = new Dataset(d);
            expect(dataset.hasSchema()).to.equal(false);
        });
        
    });

    describe('isValid()', function() {

        // FIXME is this correct?
        it('returns true if the dataset has an empty schema', function() {
            const dataset = new Dataset(d);
            expect(dataset.isValid()).to.equal(true);
        });
        
    });

});