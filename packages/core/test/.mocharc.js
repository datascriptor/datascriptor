'use strict';

module.exports = {  
  exit: true,
  bail: true,
  slow: 1000,
  recursive: true,
  timeout: 5000,
  checkLeaks: true,
  ui: 'bdd'
  // file: ['./test/pretest.js']
};