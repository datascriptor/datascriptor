const { expect } = require('chai');
const utils = require('../../lib/utils/funcs');

describe('zip()', function() {
    it('zips two arrays', function() {
        expect(utils.zip(['a', 'b', 'c'], [1, 2, 3])).to.eql([
            ['a', 1], ['b', 2], ['c', 3]
        ]);
    });
});

describe('perm()', function() {
    it('computes all the permutations of the given array', function() {
        expect(utils.perm([1, 2, 3])).to.eql([
            [1, 2, 3],
            [1, 3, 2],
            [2, 1, 3],
            [2, 3,  1], 
            [3, 1,  2],
            [3, 2,  1]
        ]);
    });

    it('computes all the permutations of the given array', function() {
        expect(utils.perm([{a: 1}, {a: 2}, {a: 3}])).to.eql([
            [{a: 1}, {a: 2}, {a: 3}],
            [{a: 1}, {a: 3}, {a: 2}],
            [{a: 2}, {a: 1}, {a: 3}],
            [{a: 2}, {a: 3},  {a: 1}], 
            [{a: 3}, {a: 1},  {a: 2}],
            [{a: 3}, {a: 2},  {a: 1}]
        ]);
    });
});

describe('intersperse', function() {
    it('intersperses \'a\' into the given array [1, 2, 3, 5]', function() {
        expect(utils.intersperse([1, 2, 3, {b: 4}, {c: 5}], 'a')).to.eql(
            [1, 'a', 2, 'a', 3, 'a', {b: 4}, 'a', {c: 5}]
        );
    });
    it('returns an empty array', function() {
        expect(utils.intersperse([], 'a')).to.eql([]);
    });
    it('returns a copy of the 1-item array', function() {
        expect(utils.intersperse([1], 'a')).to.eql([1]);
    });
});

describe('cartProd()', function() {

    it('computes the cartesian product of two arrays', function() {
        expect(utils.cartProd([['a', 'b', 'c'], [1, 2, 4]])).to.eql([
            ['a', 4], ['a', 2], ['a', 1],
            ['b', 4], ['b', 2], ['b', 1],
            ['c', 4], ['c', 2], ['c', 1]
        ]);
    });

    it('computes the cartesian product of three arrays', function() {
        expect(utils.cartProd([
            ['a', 'b', 'c'], 
            [1, 2, 4],
            ['x', 'y']
        ])).to.eql([
            ['a', 4, 'x'], ['a', 4, 'y'], ['a', 2, 'x'], ['a', 2, 'y'], ['a', 1, 'x'], ['a', 1, 'y'],
            ['b', 4, 'x'], ['b', 4, 'y'], ['b', 2, 'x'], ['b', 2, 'y'], ['b', 1, 'x'], ['b', 1, 'y'],
            ['c', 4, 'x'], ['c', 4, 'y'], ['c', 2, 'x'], ['c', 2, 'y'], ['c', 1, 'x'], ['c', 1, 'y']
        ]);
    });

});

describe('uuidv4()', function() {

    const REGEX_UUID = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i

    it('generates a UUID', function() {
        const uuid = utils.uuidv4();
        expect(REGEX_UUID.test(uuid)).to.be.true;
    });

});

