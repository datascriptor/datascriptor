const { expect } = require('chai');
const Study = require('../../lib/research/Study');
const StudyTextFactory = require('../../lib/text/StudyTextFactory');
const studyJson = require('../fixtures/research/study.json');

const testMinimalStudyJson = {
    name: 'test study',
    design: {
        type: 'just no type',
        arms: [
            {
                name: 'treatment',
                subjectType: 'Knock-out Mouse',
                size: 20
            },
            {
                name: 'control',
                subjectType: 'Wild Type Mouse',
                size: 10
            }
        ]
    }
};

describe('StudyTextFactory', function() {

    let study, textFactory;

    before(function() {
        study = Study.fromJSON(studyJson);
        textFactory = new StudyTextFactory(study);
        console.log(`Text Factory: ${textFactory}`);
    });

    describe('generateArmsReport', function() {
        it('generates the arms report', function() {
            const armsReport = textFactory.generateArmsReport();
            for (const [armName, text] of Object.entries(armsReport)) {
                console.log(`Arm report for arm ${armName}: ${text}`);
            }
        });

        it('generates an arms report with minimal input data', function() {
            const factory = new StudyTextFactory(Study.fromJSON(testMinimalStudyJson));
            const armsReport = factory.generateArmsReport();
            for (const arm of testMinimalStudyJson.design.arms) {
                expect(armsReport).to.have.property(arm.name);
            }
        });
    });

    describe('generateEventsReport', function() {
        it('generates the events report', function() {
            const eventsReport = textFactory.generateEventsReport();
            let ix = 0;
            for (const [eventId, eventReport] of Object.entries(eventsReport)) {
                console.log(`Events report for event ${eventId}: ${eventReport}`);
                expect(eventId).to.equal(study.design.events[ix].id);
                expect(eventReport).to.be.a('string');
                expect(eventReport).not.to.be.empty;
                ix++;
            }
            // console.log(`Events report: ${eventsReport}`);
        });

        it('returns an empty object if no events are provided', function() {
            const factory = new StudyTextFactory(Study.fromJSON(testMinimalStudyJson));
            const eventsReport = factory.generateEventsReport();
            expect(eventsReport).to.eql({});
        });
    });

    describe('generateElementsReport', function() {
        it('generates the elements report for each arm', function() {
            const elementsReport = textFactory.generateElementsReport();
            let ix = 0;
            for (const [elementId, elementReport] of Object.entries(elementsReport)) {
                console.log(`Events report for event ${elementId}: ${elementReport}`);
                expect(elementId).to.equal(study.design.elements[ix].id);
                expect(elementReport).to.be.a('string');
                expect(elementReport).not.to.be.empty;
                ix++;
            }
        });

        it('returns an empty object if no elements are provided', function() {
            const factory = new StudyTextFactory(Study.fromJSON(testMinimalStudyJson));
            const elementsReport = factory.generateElementsReport();
            expect(elementsReport).to.eql({});
        });
    });

});
