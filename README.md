# Datascriptor

![Logo](https://datascriptor.org/images/datascriptor-logo.png)

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/) 
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/1c26146d81334caab67103b6c42bbf29)](https://www.codacy.com/gl/datascriptor/datascriptor/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=datascriptor/datascriptor&amp;utm_campaign=Badge_Grade)
[![Netlify Status](https://api.netlify.com/api/v1/badges/724c0f2c-3c9f-40bc-81cc-e606e4deae6c/deploy-status)](https://app.netlify.com/sites/datascriptor/deploys)

This project contains the whole codebase for the Datascriptor application (https://datascriptor.org/).

## From structured dataset to data article

Leveraging our experience and links with the communities, we are now designing an open-source web-based tool – part of an ecosystem of 
existing annotation and authoring systems – to help researchers to use community standards to describe their (meta)data at the source, 
and capitalize on their effort to accelerate the creation of a data article.

In other words, Datascriptor is an application that aims at:
* capturing shallow (JSON-LD) generic structured metadata to speed up the paper publication processing, indexing and SEO
* collecting Study Design information to improve reproducibility and FAIRness of research papers. The user inputs information about the study design, treatment and sampling plans, together with the assays to be executed in the study. All this information will be stored in JSON format and can be used by the [ISA-API library](https://github.com/ISA-tools/isa-api) to generate a pre-propulated Investigation-Study-Assay (ISA) ducument to be serialised either as ISA-TAB or  ISA-JSON.
* generating text, images, and summary tables to be used in the Methods and Results section of a research paper draft. Text and figures are generated leveraging the study design information collected at the previous step and/or frictionless data tables with results of the assays/experiments run during the study.

The project has a monorepo structure and contains three packages at the moment:

[@datascriptor/core](packages/core): A minimal module, containing classes to describe research Studies, Study Designs and Datasets, plus utilities to:
* generate the textual description of the Studies and Studies Designs
* generate Study Design configurations from a minimal set of input parameters

[@datascriptor/api](packages/api): The REST API for the Datascriptor backend. This is mainly used to:
* save studies to the backend document store (MongoDB)
* use the conversion functionalities offered by the "core" module

[@datascriptor/ui](packages/ui): The User Interface for the Datascriptor Application. At the moment this works as a Single Page Application. 
We plan to support also Electron applications and mobile in the next future, leveraging the QUasar framework functionalitues.

## Requirements to work with this codebase:

* yarn ^1.2 (recommended, but works well with npm as well)
* @quasar/cli ^1.0: `yarn global install @quasar/cli`
* lerna ^3: `yarn global install lerna`

## Setup

* clone this project: `git clone https://gitlab.com/datascriptor/datascriptor`
* `cd datascriptor`
* install dependencies: `yarn install`
* wire the packages with lerna: `npx lerna bootstrap`
* start the API: `cd packages/api && yarn start`
* start the UI (development mode): `cd packages/ui && quasar dev`

## Testing

* run tests on all the packages: `npx lerna run test`
